#!/bin/sh
set -ex

. ${BITBUCKET_CLONE_DIR}/ops/base.sh
. ${BITBUCKET_CLONE_DIR}/ops/tools/get-info-from-packagejson.sh

echo "--------------------- ENVIRONMENT VARIABLES ---------------------------"
printenv
echo "--------------------- ENVIRONMENT VARIABLES ---------------------------"

echo "--------------------- Current Folder ---------------------------"
ls -l
cd ${BITBUCKET_CLONE_DIR}
ls -l
echo "--------------------- Current Folder ---------------------------"

export ENVIRONMENT=$BITBUCKET_DEPLOYMENT_ENVIRONMENT
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID_QIP
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY_QIP
export ECR_UNIQUE_IDENTIFIER=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

. ${BITBUCKET_CLONE_DIR}/ops/base_container.envs

export TF_ECR=$TF_VAR_DOCKER_REGISTRY

echo "--------------------- Docker build ---------------------------"

docker build --build-arg NODE_ENV=$ENVIRONMENT -t $TF_ECR:$ECR_UNIQUE_IDENTIFIER -t $TF_ECR:$PACKAGE_VERSION -f Dockerfile .

$(aws ecr get-login --region $AWS_DEFAULT_REGION | sed -e 's/-e none//g')

docker push $TF_ECR:$ECR_UNIQUE_IDENTIFIER
docker push $TF_ECR:$PACKAGE_VERSION
echo $TF_ECR:$ECR_UNIQUE_IDENTIFIER

echo "--------------------- Image build ---------------------------"
echo "export TF_VAR_ECR=$TF_ECR" >> ${BITBUCKET_CLONE_DIR}/ops/release_container.envs
echo "export ECR_UNIQUE_IDENTIFIER=$ECR_UNIQUE_IDENTIFIER" >> ${BITBUCKET_CLONE_DIR}/ops/release_container.envs
echo "export PACKAGE_VERSION=$PACKAGE_VERSION" >> ${BITBUCKET_CLONE_DIR}/ops/release_container.envs
echo "export SERVICE=$SERVICE" >> ${BITBUCKET_CLONE_DIR}/ops/release_container.envs
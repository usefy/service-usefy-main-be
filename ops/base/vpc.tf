data "aws_vpc" "main" {
  id = "${var.VPC_ID}"
}

output "VPC_ARN" {
  value = "${data.aws_vpc.main.arn}"
}

output "VPC_ID" {
  value = "${data.aws_vpc.main.id}"
}

output "VPC_CIDR_BLOCK" {
  value = "${data.aws_vpc.main.cidr_block}"
}
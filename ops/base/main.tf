/**
 * main.tf
 * The main entry point for Terraform run
 * See variables.tf for common variables
 * See ecr.tf for creation of Elastic Container Registry for all environments
 * See state.tf for creation of S3 bucket for remote state
 */
# Backend for store stage file
terraform {
  backend "s3" {
    encrypt = false
  }
}

# Using the AWS Provider
# https://www.terraform.io/docs/providers/
provider "aws" {
  region  = "${var.region}"
  version = "2.17"
}

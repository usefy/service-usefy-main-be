#!/bin/sh
set -ex

echo "--------------------- ENVIRONMENT VARIABLES ---------------------------"
printenv
echo "--------------------- ENVIRONMENT VARIABLES ---------------------------"

echo "--------------------- Current Folder ---------------------------"
ls -l
cd ${BITBUCKET_CLONE_DIR}/ops/base
ls -l
echo "--------------------- Current Folder ---------------------------"


export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID_QIP
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY_QIP

. ${BITBUCKET_CLONE_DIR}/ops/tools/get-info-from-packagejson.sh
export TF_VAR_SERVICE=$SERVICE
export TF_VAR_app=$SERVICE

export S3_TERRAFORM_STATE_BUCKET=qip-terraform
export S3_TERRAFORM_STATE_REGION=$AWS_DEFAULT_REGION
export TERRAFORM_STATE_NAME="$SERVICE-$AWS_DEFAULT_REGION/terraform_deploy_${SERVICE}_${TF_VAR_ENVIRONMENT}.tfstate"

echo "Starting base infra build"

echo "Starting terraform init"
/usr/local/bin/terraform init \
 -backend-config="bucket=${S3_TERRAFORM_STATE_BUCKET}" \
 -backend-config="region=${S3_TERRAFORM_STATE_REGION}" \
 -backend-config="key=${TERRAFORM_STATE_NAME}"

echo "Starting terraform apply" 
# /usr/local/bin/terraform plan 
/usr/local/bin/terraform apply -auto-approve
# /usr/local/bin/terraform destroy -auto-approve

rm ${BITBUCKET_CLONE_DIR}/ops/base_container.envs || true

/usr/local/bin/terraform output | while read line
do
  echo export `echo TF_VAR_$line |tr '[:lower:]' '[:upper:]'| awk '{print $1}'`=`echo $line | awk '{print $3}'` >> ${BITBUCKET_CLONE_DIR}/ops/base_container.envs
done

chmod +x ${BITBUCKET_CLONE_DIR}/ops/base_container.envs

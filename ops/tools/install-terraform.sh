#!/bin/sh
set -ex


mkdir -p ~/bin

cd ~/bin

export TERRAFORM_VERSION=0.12.3
export PATH="$PATH:/root/bin:/usr/local/bin"

apt-get update && \
    apt-get install -y python-pip \
    python-dev \
    build-essential \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
    unzip && \
    pip install --upgrade awscli

wget -O terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform.zip -d /usr/local/bin && \
    rm -f terraform.zip

terraform version
#!/bin/sh
set -ex

export SERVICE=${SERVICE_NAME:-$(cat ${BITBUCKET_CLONE_DIR}/package.json  | grep name | head -1 | sed 's/"name"://' | sed 's/[",]//g'| tr -d '[[:space:]]')}
export PACKAGE_VERSION=$(cat ${BITBUCKET_CLONE_DIR}/package.json | grep version | head -1 | awk -F= "{ print $2 }" | sed 's/[version:,\",]//g' | tr -d '[[:space:]]')
echo "Service is $SERVICE:$PACKAGE_VERSION"

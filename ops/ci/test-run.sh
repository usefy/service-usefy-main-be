#!/bin/sh
# set -ex

# # create a custom network (silently fail if exists)
# INET_NAME=isolated_ci_network
# docker network create --driver bridge $INET_NAME || true

# SERVICE='usefy-main-be'

# # DynamoDB docker
# DYNAMO_CONTAINER_NAME=$SERVICE-dynamo
# DYNAMO_CONTAINER_PORT=8000
# docker run \
#     --network $INET_NAME \
#     --name $DYNAMO_CONTAINER_NAME \
#     -p "$DYNAMO_CONTAINER_PORT:$DYNAMO_CONTAINER_PORT" \
#     -d amazon/dynamodb-local \

# docker run \
#     --network $INET_NAME \
#     --env "CONFIG_ENV=testing" \
#     --env "MOCK_DYNAMODB_ENDPOINT=http://$DYNAMO_CONTAINER_NAME:$DYNAMO_CONTAINER_PORT" \
#     --entrypoint 'yarn' \
#     $SERVICE-dev \
#     -c 'run' \
#     -c 'test'

# container_id=$(docker ps -l --format "{{.ID}}")
# docker cp $container_id:/usr/src/app/junit.xml ./test-reports
# docker cp $container_id:/usr/src/app/coverage ./test-reports

# cat ./test-reports/junit.xml

curl -L https://github.com/docker/compose/releases/download/1.23.2/docker-compose-`uname -s`-`uname -m` > ~/docker-compose
chmod +x ~/docker-compose
sudo mv ~/docker-compose /usr/local/bin/docker-compose

make api-test-integration
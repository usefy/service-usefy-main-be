SERVICE='usefy-main-be'

DYNAMO_CONTAINER_NAME=$SERVICE-dynamo
INET_NAME=isolated_ci_network

# Terminate local database containers
docker stop $DYNAMO_CONTAINER_NAME || true && docker rm $DYNAMO_CONTAINER_NAME || true
docker network rm $INET_NAME
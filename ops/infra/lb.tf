# note that this creates the alb, target group, and access logs
# the listeners are defined in lb-http.tf and lb-https.tf
# delete either of these if your app doesn't need them
# but you need at least one

data "aws_subnet_ids" "public" {
  vpc_id = "${var.VPC_ID}"
}


resource "aws_lb" "main" {
  name            = "${var.app}-${var.environment}"
  subnets         = data.aws_subnet_ids.public.ids
  security_groups = [aws_security_group.load_balancer.id]
  tags            = var.tags
}

resource "aws_lb_target_group" "main" {
  name        = "${var.app}-${var.environment}"
  vpc_id      = data.aws_vpc.main.id
  port        = "${var.PORT}"
  protocol    = "${var.lb_protocol}"
  target_type = "ip"

  health_check {
    path                = "${var.health_check}"
    matcher             = "${var.health_check_matcher}"
    interval            = "${var.health_check_interval}"
    timeout             = "${var.health_check_timeout}"
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }

  tags = var.tags
}

resource "aws_lb_listener" "redirect" {
  load_balancer_arn = aws_lb.main.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = 443
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "main" {
  load_balancer_arn = aws_lb.main.id
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = aws_acm_certificate.https.arn

  depends_on = [aws_acm_certificate.https, aws_route53_record.validation, aws_acm_certificate_validation.this]

  default_action {
    target_group_arn = aws_lb_target_group.main.id
    type             = "forward"
  }
}

resource "aws_route53_record" "load_balancer" {
  zone_id = data.aws_route53_zone.domain-zone.zone_id
  name    = "${var.app}-${var.environment}.${var.DOMAIN}"
  type    = "CNAME"
  ttl     = 5
  records = [
    aws_lb.main.dns_name,
  ]

  # set_identifier = substr(sha1(var.REGION), 0, 7)
  # latency_routing_policy {
  # region = var.REGION
  # }
}

# The load balancer DNS name
output "lb_dns" {
  value = "${aws_lb.main.dns_name}"
}

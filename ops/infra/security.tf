resource "aws_security_group" "load_balancer" {
  name        = "${var.app}-lb-public-${var.environment}"
  vpc_id      = data.aws_vpc.main.id
  description = "Allow inbound access from the Internet"

  ingress {
    description = "HTTP"
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

resource "aws_security_group" "ecs_service" {
  name        = "${var.app}-ecs-service-${var.environment}"
  vpc_id      = data.aws_vpc.main.id
  description = "Allow inbound access from the ALB only"

  ingress {
    protocol        = "tcp"
    from_port       = var.PORT
    to_port         = var.PORT
    security_groups = [aws_security_group.load_balancer.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [aws_security_group.load_balancer]

  tags = var.tags
}
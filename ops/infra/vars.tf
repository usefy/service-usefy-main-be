
variable "REGION" {
  description = "Aws region"
  default     = "us-east-1"
}

variable "DOMAIN" {
  description = "Domain for deployment"
  default     = "meuqip.com.br"
}

/*
 * variables.tf
 * Common variables to use in various Terraform files (*.tf)
 */

# The AWS region to use for the dev environment's infrastructure
# Currently, Fargate is only available in `us-east-1`.

# Tags for the infrastructure
variable "tags" {
  type = "map"
  default = {
    application = "usefy"
  }
}


# The application's name
variable "app" {}

variable "cluster_name" {
  default = "uefy"
}

# The environment that is being built
variable "environment" {}

# The port the container will listen on, used for load balancer health check
# Best practice is that this value is higher than 1024 so the container processes
# isn't running at root.
variable "PORT" {
  default = 3000
}

# The port the load balancer will listen on
variable "lb_port" {
  default = "80"
}

# The load balancer protocol
variable "lb_protocol" {
  default = "HTTP"
}

# How many containers to run
variable "replicas" {
  default = "1"
}

# The name of the container to run
variable "container_name" {
  default = "app"
}

variable "ECR_UNIQUE_IDENTIFIER" {}


# The minimum number of containers that should be running.
# Must be at least 1.
# used by both autoscale-perf.tf and autoscale.time.tf
# For production, consider using at least "2".
variable "ecs_autoscale_min_instances" {
  default = "1"
}

# The maximum number of containers that should be running.
# used by both autoscale-perf.tf and autoscale.time.tf
variable "ecs_autoscale_max_instances" {
  default = "1"
}

# The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused
variable "deregistration_delay" {
  default = "30"
}

# The path to the health check for the load balancer to know if the container(s) are ready
variable "health_check" {
  default = "/health"
}

# How often to check the liveliness of the container
variable "health_check_interval" {
  default = "30"
}

# How long to wait for the response on the health check path
variable "health_check_timeout" {
  default = "10"
}

# What HTTP response code to listen for
variable "health_check_matcher" {
  default = "200"
}

variable "lb_access_logs_expiration_days" {
  default = "3"
}

# Whether the application is available on the public internet,
# also will determine which subnets will be used (public or private)

variable "VPC_ID" {
  description = "VPC ID to deploy the container"
}
variable "VPC_CIDR_BLOCK" {}

variable "ENV_VAR" {
  description = "Environemt variables of the container "
}
data "aws_route53_zone" "domain-zone" {
  name = "${var.DOMAIN}"
}

# Find a certificate that is issued
resource "aws_acm_certificate" "https" {
  provider = "aws.global"
  domain_name   = "${var.app}-${var.environment}.${var.DOMAIN}"
  validation_method = "DNS"
}

resource "aws_route53_record" "validation" {
  name    = "${aws_acm_certificate.https.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.https.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.domain-zone.zone_id}"
  records = ["${aws_acm_certificate.https.domain_validation_options.0.resource_record_value}"]
  ttl     = "60"
}

resource "aws_acm_certificate_validation" "this" {
  certificate_arn = "${aws_acm_certificate.https.arn}"
  validation_record_fqdns = [
    "${aws_route53_record.validation.fqdn}",
  ]
}

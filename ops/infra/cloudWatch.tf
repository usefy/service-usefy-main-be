resource "aws_cloudwatch_log_group" "app" {
  name              = "/fargate/service/${var.app}-${var.environment}"
  retention_in_days = "14"
  tags              = "${var.tags}"
}

resource "aws_dynamodb_table" "sports" {
  hash_key     = "id"
  name         = "usefy-${var.environment}-sports"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "parentId"
    type = "S"
  }

  global_secondary_index {
    name            = "parentId_id_index"
    hash_key        = "parentId"
    range_key        = "id"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "clubs" {
  hash_key     = "id"
  range_key    = "clubName"
  name         = "usefy-${var.environment}-clubs"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "clubName"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "coupons" {
  hash_key     = "id"
  range_key    = "listingItem"
  name         = "usefy-${var.environment}-coupons"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "listingItem"
    type = "S"
  }

  attribute {
    name = "key"
    type = "S"
  }

  global_secondary_index {
    name            = "key_index"
    hash_key        = "key"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "listingItem_key_index"
    hash_key        = "listingItem"
    range_key       = "key"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "disputes" {
  hash_key     = "uid"
  range_key    = "orderId"
  name         = "usefy-${var.environment}-disputes"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "orderId"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "listings" {
  hash_key     = "firebaseUid"
  range_key    = "id"
  name         = "usefy-${var.environment}-listings"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "firebaseUid" // TODO: change to uid
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  global_secondary_index {
    name            = "id_index"
    hash_key        = "id"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "listings-keywords" {
  hash_key     = "id"
  range_key    = "keyword"
  name         = "usefy-${var.environment}-listings-keywords"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "keyword"
    type = "S"
  }

  attribute {
    name = "publishedAt"
    type = "S"
  }

  attribute {
    name = "price"
    type = "N"
  }

  local_secondary_index {
    name            = "id_publishedAt_index"
    range_key       = "publishedAt"
    projection_type = "ALL"
  }

  local_secondary_index {
    name            = "id_price_index"
    range_key       = "price"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "listings-favorites" {
  hash_key     = "uid"
  range_key    = "id"
  name         = "usefy-${var.environment}-listings-favorites"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "uid"
    type = "S"
  }

  global_secondary_index {
    name            = "uid_index"
    hash_key        = "uid"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "rooms" {
  hash_key     = "listingId"
  range_key    = "id"
  name         = "usefy-${var.environment}-rooms"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "listingId"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "firebaseUid1"
    type = "S"
  }

  attribute {
    name = "firebaseUid2"
    type = "S"
  }

  global_secondary_index {
    name            = "id_index"
    hash_key        = "id"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "firebaseUid1_id_index"
    hash_key        = "firebaseUid1"
    range_key       = "id"
    projection_type = "ALL"
  }
  global_secondary_index {
    name            = "firebaseUid2_id_index"
    hash_key        = "firebaseUid2"
    range_key       = "id"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "messages" {
  hash_key     = "roomId"
  range_key    = "id"
  name         = "usefy-${var.environment}-messages"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "roomId"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "firebaseUid"
    type = "S"
  }

  attribute {
    name = "createdAt"
    type = "S"
  }

   attribute {
    name = "readAt"
    type = "S"
  }

  global_secondary_index {
    name            = "id_index"
    hash_key        = "id"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "firebaseUid_id_index"
    hash_key        = "firebaseUid"
    range_key       = "id"
    projection_type = "ALL"
  }

  local_secondary_index {
    name            = "roomId_createdAt_index"
    range_key       = "createdAt"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "read_at_index"
    hash_key        = "readAt"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "moipAccounts" {
  hash_key     = "uid"
  range_key    = "id"
  name         = "usefy-${var.environment}-moipAccounts"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "moipBankAccounts" {
  hash_key     = "uid"
  range_key    = "id"
  name         = "usefy-${var.environment}-moipBankAccounts"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "moipCustomers" {
  hash_key     = "uid"
  range_key    = "id"
  name         = "usefy-${var.environment}-moipCustomers"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "moipCustomersFundingInstrument" {
  hash_key     = "uid"
  range_key    = "id"
  name         = "usefy-${var.environment}-moipCustomersFundingInstrument"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "moipOrders" {
  hash_key     = "uid"
  range_key    = "id"
  name         = "usefy-${var.environment}-moipOrders"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "moipPayments" {
  hash_key     = "uid"
  range_key    = "id"
  name         = "usefy-${var.environment}-moipPayments"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "orderId"
    type = "S"
  }

  global_secondary_index {
    name            = "orderId_uid_index"
    hash_key        = "orderId"
    range_key       = "uid"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "notifications" {
  hash_key     = "uid"
  range_key    = "id"
  name         = "usefy-${var.environment}-notifications"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "orders" {
  hash_key     = "id"
  name         = "usefy-${var.environment}-orders"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "listingId"
    type = "S"
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  global_secondary_index {
    name            = "uid_id_index"
    hash_key        = "uid"
    range_key       = "id"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "listingId_id_index"
    hash_key        = "listingId"
    range_key       = "id"
    projection_type = "ALL"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "requestLoggers" {
  hash_key     = "id"
  name         = "usefy-${var.environment}-requestLoggers"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "id"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "users-sports-apps" {
  hash_key     = "uid"
  range_key    = "app"
  name         = "usefy-${var.environment}-users-sports-apps"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "uid"
    type = "S"
  }

  attribute {
    name = "app"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

resource "aws_dynamodb_table" "users" {
  hash_key     = "firebaseUid"
  name         = "usefy-${var.environment}-users"
  billing_mode = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "firebaseUid"
    type = "S"
  }

  tags = {
    Name        = "${var.app}"
    Environment = "${var.environment}"
  }
}

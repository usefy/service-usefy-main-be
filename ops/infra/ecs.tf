
resource "aws_ecs_cluster" "app" {
  name = "${var.cluster_name}"
  tags = "${var.tags}"
}

data "aws_ecr_repository" "app" {
  name = "${var.app}"
}

resource "aws_ecs_service" "app" {
  name                    = "${var.app}-${var.environment}"
  cluster                 = "${aws_ecs_cluster.app.id}"
  task_definition         = "${aws_ecs_task_definition.app.arn}"
  desired_count           = "${var.replicas}"
  launch_type             = "FARGATE"
  tags                    = var.tags
  enable_ecs_managed_tags = true
  propagate_tags          = "SERVICE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_service.id]
    subnets          = data.aws_subnet_ids.public.ids
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.main.id
    container_name   = var.container_name
    container_port   = var.PORT
  }

  lifecycle {
    ignore_changes = ["desired_count"]
  }

  depends_on = [aws_lb_listener.main]
}

resource "aws_ecs_task_definition" "app" {
  family                   = "${var.app}-${var.environment}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "256"
  memory                   = "512"
  execution_role_arn       = "${data.aws_iam_role.task_execution.arn}"

  # defined in role.tf
  # task_role_arn = "${aws_iam_role.app_role.arn}"

  container_definitions = <<DEFINITION
[
  {
    "name": "${var.container_name}",
    "image": "${data.aws_ecr_repository.app.repository_url}:${var.ECR_UNIQUE_IDENTIFIER}",
    "memory": 512,
    "cpu": 256,
    "essential": true,
    "networkMode": "awsvpc",
    "portMappings": [{
      "hostPort": ${var.PORT},
      "containerPort": ${var.PORT},
      "protocol": "tcp"
    }],
    "environment": ${var.ENV_VAR},
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "/fargate/service/${var.app}-${var.environment}",
        "awslogs-region": "us-east-1",
        "awslogs-stream-prefix": "ecs"
      }
    }
  }
]
DEFINITION

  tags = "${var.tags}"
}

# service-usefy-main-be


## Executando

Para executar o container localmente, para subir o docker, use esse comando:

```bash
docker-compose up
```

## DynamoDB IDE

Como sugestão para visualização das informações do DynamoDB:

```bash
npm install -g dynamodb-admin
```
import { logger } from '../../src/logger';
import { State } from '../../src/models';

export const stateFactory = (
  correlationId: string = 'test-correlation-id',
  sessionId: string = 'test-session-id',
): State => ({
  correlationId,
  sessionId,
  logger: logger.child({
    correlationId,
    sessionId,
  }),
  out: null,
  user: null,
});

describe('Text Example', () => {
  it('Stay True', () => {
    const stayTrue = true;

    expect(stayTrue).toEqual(true);
  });

  it('I\'m false', () => {
    const imFalse = false;

    expect(imFalse).toEqual(false);
  });

  it('I\'m promise you.', async () => {
    const promise = new Promise((resolve) => {
      resolve('I\'m done');
    });

    expect(await promise).toEqual('I\'m done');
  });
});

import { notificationReceived } from '../../src/modules/payments/services/notificationService';
import { stateFactory } from '../utils';
import supertest from 'supertest';
import { app } from '../../src/app';
import { config } from '../../src/config';

const state = stateFactory();

describe('Notifications Module `/notification`', () => {
  describe('Notification endpoint', () => {
    it('Should fails if no token', async () => {
      const data = {
        date: new Date(),
        env: '',
        event: 'UNKNOWN',
        resource: {
          title: 'unknown resource',
        },
      };

      const { body } = await supertest(app)
        .post('/v1/payments/notifications')
        .send(data)
        .expect(401);
    });

    it('Should fails if invalid token', async () => {
      const data = {
        date: new Date(),
        env: '',
        event: 'UNKNOWN',
        resource: {
          title: 'unknown resource',
        },
      };

      const { body } = await supertest(app)
        .post('/v1/payments/notifications')
        .set('token', 'invalid')
        .send(data)
        .expect(401);
    });

    it('Should get success when UNKNOWN.* event is received', async () => {
      const data = {
        date: new Date(),
        env: '',
        event: 'UNKNOWN',
        resource: {
          title: 'unknown resource',
        },
      };

      const { body } = await supertest(app)
        .post('/v1/payments/notifications')
        .set('token', config.moip.webhookToken)
        .send(data)
        .expect(200);

      expect(body).toBeTruthy();
    });

    xit('Should fail if order not found for a payment', async () => {
      const data = {
        date: new Date(),
        env: '',
        event: 'PAYMENT.IN_ANALYSIS',
        resource: {
          payment: {
            id: 'PAY-ICZGLZS503NV',
            _links: {
              order: {
                href: 'https://sandbox.moip.com.br/v2/orders/ORD-QKBOXT978YCX',
                title: 'ORD-QKBOXT978YCX',
              },
              self: {
                href: 'https://sandbox.moip.com.br/v2/payments/PAY-0OBNUIQZAGGP',
              },
            },
          },
        },
      };

      const { body } = await supertest(app)
        .post('/v1/payments/notifications')
        .set('token', config.moip.webhookToken)
        .send(data)
        .expect(418);
    });

    xit('Should fail if payment not found', async () => {
      const data = {
        date: new Date(),
        env: '',
        event: 'PAYMENT.IN_ANALYSIS',
        resource: {
          payment: {
            id: 'PAY-ICZGLZS503NV',
            _links: {
              order: {
                href: 'https://sandbox.moip.com.br/v2/orders/ORD-QKBOXT978YCX',
                title: 'ORD-QKBOXT978YCX',
              },
              self: {
                href: 'https://sandbox.moip.com.br/v2/payments/PAY-0OBNUIQZAGGP',
              },
            },
          },
        },
      };

      const { body } = await supertest(app)
        .post('/v1/payments/notifications')
        .set('token', config.moip.webhookToken)
        .send(data)
        .expect(418);
    });

  });
});

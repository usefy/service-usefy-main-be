import supertest from 'supertest';
import { app } from '../../src/app';
import { user as firebaseUser, auth } from '../../src/components/firebase';

const usersToRemove: string[] = [];

async function generateToken(uid) {
  return auth.createToken(uid);
}

describe('User Module `/user`', () => {
  xit('Should create a new user', async () => {
    const { body } = await supertest(app)
      .post('/v1/user')
      .send({
        displayName: 'Test User',
        firstName: 'Test',
        lastName: 'User',
        username: 'testuser',
        email: 'test.user@email.com',
        password: '123456',
        passwordConfirmation: '123456',
      })
      .expect(200);

    usersToRemove.push(body.uid);

    expect(body.displayName).toBe('Test User');
  });

  xit('Should fail create a new user when body is malformed', async () => {
    const response = await supertest(app)
      .post('/v1/user')
      .send({
        displayName: 'Test User',
        // email: 'test@email.com',
        password: '123456',
        passwordConfirmation: '123456',
      })
      .expect(400);

    const response2 = await supertest(app)
      .post('/v1/user')
      .send({
        displayName: 'Test User',
        email: 'test@email.com',
        // password: '123123',
        // passwordConfirmation: '123123',
      })
      .expect(400);

    const response3 = await supertest(app)
      .post('/v1/user')
      .send({
        // displayName: 'Test User',
        email: 'test@email.com',
        password: '123123',
        passwordConfirmation: '123123',
      })
      .expect(400);
  });
  // it('Should authenticate a user', async () => {
  //   const uid = usersToRemove[usersToRemove.length - 1];
  //   const token = await generateToken(uid);

  //   const response = await supertest(app)
  //     .post('/v1/login')
  //     .set('Authorization', `Bearer ${token}`)
  //     .expect(200);
  // });
  // it('Should fail authenticate a user when request is malformed', async () => { });
  // it('Should update a user', async () => { });
  // it('Should logout a user', async () => { });
  // it('Should get if username is available', async () => { });
  // it('Should get username is unavailable', async () => { });

  afterAll(async () => {
    // remove all created users

    for (const u of usersToRemove) {
      await firebaseUser.remove(u);
    }
  });
});

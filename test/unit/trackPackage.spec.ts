import { trackPackage, defaultConfig } from '../../src/modules/shipment/components/correios/remote/correiosSRO';
import { stateFactory } from '../utils';

const state = stateFactory();

describe('Track Packages ar Correios', () => {
  it('Should get response from Correios', async () => {

    const response = await trackPackage('LX588019498US', defaultConfig, state);

    expect(response).toBeDefined();
  });
});

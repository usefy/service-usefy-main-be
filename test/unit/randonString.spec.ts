import { randomString } from '../../src/helpers/randomString';

function isOnly(input: string, { letters = true, numbers = false, specials = false }) {
  let response = true;
  const lettersList = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const numberList = '0123456789';
  const specialsList = '?!@#$%&*-=+';

  input.split('').forEach((value) => {
    if (letters && numbers && specials) {
      response = response &&
        (lettersList.includes(value) ||
          numberList.includes(value) ||
          specialsList.includes(value));
    }

    if (letters && numbers && !specials) {
      response = response &&
        (lettersList.includes(value) ||
          numberList.includes(value)) &&
        !specialsList.includes(value);
    }

    if (letters && !numbers && !specials) {
      response = response &&
        (lettersList.includes(value) &&
          !numberList.includes(value) &&
          !specialsList.includes(value));
    }

    if (!letters && numbers && !specials) {
      response = response &&
        (!lettersList.includes(value) &&
          numberList.includes(value) &&
          !specialsList.includes(value));
    }

    if (!letters && numbers && specials) {
      response = response &&
        !lettersList.includes(value) &&
        (numberList.includes(value) ||
          specialsList.includes(value));
    }

    if (!letters && !numbers && specials) {
      response = response &&
        (!lettersList.includes(value) &&
          !numberList.includes(value) &&
          specialsList.includes(value));
    }

    if (letters && !numbers && specials) {
      response = response &&
        !numberList.includes(value) &&
        (lettersList.includes(value) ||
          specialsList.includes(value));
    }

  });

  return response;
}
describe('Random String Helper', () => {
  it('Should get error is all char options are disabled.', () => {
    try {
      randomString(5, { letters: false, numbers: false, specials: false });
    } catch (error) {
      expect(error).toBeDefined();
    }
  });

  it('Should return a random string.', () => {
    const l = 9;
    const r = randomString(l);
    expect(r).toHaveLength(l);
    expect(isOnly(r, { letters: true, numbers: true, specials: false })).toBeTruthy();
  });

  it('Should return a random string with specials.', () => {
    const l = 9;
    const o = { letters: true, numbers: true, specials: true };
    const r = randomString(l, o);
    expect(r).toHaveLength(l);
    expect(isOnly(r, o)).toBeTruthy();
  });

  it('Should generate a random string only with numbers.', () => {
    const l = 9;
    const o = { letters: false, numbers: true, specials: false };
    const r = randomString(l, o);
    expect(r).toHaveLength(l);
    expect(isOnly(r, o)).toBeTruthy();
  });

  it('Should generate a random string only with letters.', () => {
    const l = 9;
    const o = { letters: true, numbers: false, specials: false };
    const r = randomString(l, o);
    expect(r).toHaveLength(l);
    expect(isOnly(r, o)).toBeTruthy();
  });

  it('Should generate a random string only with specials.', () => {
    const l = 9;
    const o = { letters: false, numbers: false, specials: true };
    const r = randomString(l, o);
    expect(r).toHaveLength(l);
    expect(isOnly(r, o)).toBeTruthy();
  });
});

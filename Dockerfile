FROM node:10-slim as BUILDER 
LABEL maintainer="Bruno Germano"

WORKDIR /usr/src/app

# RUN mkdir data
# RUN touch ./data/shared-local-instance.db

# Install app dependencies
COPY package.json yarn.lock ./
RUN yarn install

# Bundle app source
COPY tsconfig.json tslint.json jest.config.js ./

#COPY data ./data
COPY src ./src
COPY test ./test

RUN yarn build

FROM node:10-alpine

RUN apk add --no-cache curl

ARG NODE_ENV

WORKDIR /usr/src/app

COPY --from=BUILDER /usr/src/app/package.json ./
COPY --from=BUILDER /usr/src/app/build ./build
COPY --from=BUILDER /usr/src/app/node_modules ./node_modules

EXPOSE 3000

CMD [ "yarn", "start" ]
import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import cors from 'cors';
import { router } from './routes';
import { logger } from './logger';
import { setCorrelationAndSessionId } from './helpers/setTrackers';

export const app: express.Application = express();

// Compression middleware (should be placed before express.static)
app.use(compression({
  threshold: 512,
}));

// bodyParser should be above methodOverride
app.use(bodyParser.urlencoded({
  extended: true,
}));

// bodyParser allows to parse JSON object into javascript object in req.body
app.use(bodyParser.json());

app.use(setCorrelationAndSessionId());
app.use(createChildLogger);
// app.use((req, res, next) => {
//   logger.info({ req, res }, 'Initial Info');
//   return next();
// });

app.use(cors());

app.use(router);

export function createChildLogger(req, res, next) {
  req.state.logger = logger.child({
    correlationId: req.state.correlationId,
    sessionId: req.state.sessionId,
  });
  return next();
}

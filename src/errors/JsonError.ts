/**
 * Standard json error with possibility to add response code
 * @property {string} name Name of it
 * @extends Error
 */
export default class JsonError extends Error {
  name: string;
  message: string;
  status: number;
  info: any;
  statusCode: number; // important for Restify

  /**
   *
   * @param {string} message Error message
   * @param {number} status Status code for http response
   * @param {number} errorCode Custom error code for i.e.
   *  mobile app error handling (like email is already taken)
   */
  constructor(message: string, status: number, info: any = {}) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.status = status;
    this.info = info;
    this.statusCode = status; // important for Restify
  }
}

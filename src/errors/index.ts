import { NotFoundError } from './NotFoundError';
import { ServerError } from './ServerError';
import { BadRequestError } from './BadRequestError';
import { UnauthorizedError } from './UnauthorizedError';
import { BadGatewayError } from './BadGatewayError';
import { ValidationError } from './ValidationError';
import { ForbiddenError } from './ForbiddenError';
import { TeapotError } from './TeapotError';

export const errors = {
  notFound: (message: string, info: any) => {
    return new NotFoundError(message, info);
  },
  serverError: (message: string, info: any) => {
    return new ServerError(message, info);
  },
  badGateway: (message: string, info: any) => {
    return new BadGatewayError(message, info);
  },
  badRequest: (message: string, info: any) => {
    return new BadRequestError(message, info);
  },
  unauthorized: (message: string, info: any) => {
    return new UnauthorizedError(message, info);
  },
  validation: (message: string, info: any) => {
    return new ValidationError(message, info);
  },
  forbidden: (message: string, info: any) => {
    return new ForbiddenError(message, info);
  },
  teapot: (message: string, info: any = null) => new TeapotError(message, info),
};

const paymentErrors = [
  {
    code: 'PAY-014',
    message: 'Não foi possível descriptografar o hash do cartão.',
  },
  {
    code: 'PAY-033',
    message: 'A data de nascimento do portador do cartão é inválido',
  },
  { code: 'PAY-036', message: 'O CPF informado deve ter 11 números' },
  {
    code: 'PAY-037',
    message:
      'O valor do pagamento é inválido ou maior do que o valor restante do pedido',
  },
  { code: 'PAY-100', message: 'O IP possui mais de 15 caracteres.' },
  { code: 'PAY-101', message: 'O fingerprint possui mais de 256 caracteres.' },
  { code: 'PAY-102', message: 'O userAgent possui mais de 256 caracteres.' },
  {
    code: 'PAY-103',
    message: 'A latitude possui mais de 3 inteiros ou 7 decimais.',
  },
  {
    code: 'PAY-104',
    message: 'A longitude possui mais de 3 inteiros ou 7 decimais',
  },
  { code: 'PAY-531', message: 'Valor da taxa é maior que o valor da comissão' },
  { code: 'PAY-600', message: 'Quantidade de parcelas deve ser numérico' },
  {
    code: 'PAY-601',
    message: 'A estrutura do instrumento de pagamento não foi informada',
  },
  {
    code: 'PAY-602',
    message: 'O statementDescriptor deve ter no máximo 13 caracteres.',
  },
  {
    code: 'PAY-603',
    message:
      'A estrutura de informações não está correta para o método informado',
  },
  { code: 'PAY-604', message: 'Pagamento não encontrado' },
  { code: 'PAY-605', message: 'O nó de parcelas não deve ser negativo' },
  {
    code: 'PAY-606',
    message: 'Já existe um pagamento realizado com cartão de crédito',
  },
  {
    code: 'PAY-607',
    message: 'Já existe um pagamento realizado com cartão de débito',
  },
  { code: 'PAY-608', message: 'Ocorreu um erro na criação cartão de crédito' },
  {
    code: 'PAY-609',
    message: 'Forma de pagamento não autorizada para a conta',
  },
  {
    code: 'PAY-610',
    message:
      'A estrutura de informações de cartão de crédito não foi informada',
  },
  {
    code: 'PAY-611',
    message: 'Já existe um pagamento realizado com boleto bancário',
  },
  {
    code: 'PAY-612',
    message: 'Já existe um pagamento realizado com débito em conta',
  },
  {
    code: 'PAY-613',
    message: 'Somente é possível capturar pagamentos com status PRE_AUTHORIZED',
  },
  {
    code: 'PAY-614',
    message: 'Somente é possível desfazer pagamentos com status PRE_AUTHORIZED',
  },
  { code: 'PAY-615', message: 'Conta Wirecard não autorizada' },
  { code: 'PAY-616', message: 'Conta Wirecard não encontrada' },
  {
    code: 'PAY-617',
    message: 'Já existe um pagamento realizado com wallet para esse pedido',
  },
  {
    code: 'PAY-618',
    message: 'Forma de pagamento não pode ser utilizada nesse pedido',
  },
  {
    code: 'PAY-619',
    message: 'O mês de expiração do cartão de crédito não foi informado',
  },
  {
    code: 'PAY-620',
    message: 'O mês de expiração do cartão de crédito deve ser numérico',
  },
  {
    code: 'PAY-621',
    message: 'O mês de expiração do cartão informado é inválido',
  },
  {
    code: 'PAY-622',
    message: 'O ano de expiração do cartão não foi informado',
  },
  {
    code: 'PAY-623',
    message: 'O ano de expiração do cartão de crédito deve ser numérico',
  },
  {
    code: 'PAY-624',
    message:
      'A data de expiração do cartão de crédito deve ser maior ou igual a data',
  },
  {
    code: 'PAY-625',
    message: 'O ano de expiração do cartão informado é inválido',
  },
  {
    code: 'PAY-626',
    message: 'Formato informado para o cartão de crédito é invalido',
  },
  {
    code: 'PAY-627',
    message:
      'O código de segurança do cartão deve conter 4 caracteres para Amex e 3',
  },
  { code: 'PAY-628', message: 'O número do cartão não foi informado' },
  { code: 'PAY-629', message: 'O número do cartão deve ser numérico' },
  {
    code: 'PAY-630',
    message:
      'O número do cartão informado possui quantidade de caracteres inválido',
  },
  {
    code: 'PAY-631',
    message: 'O código de segurança do cartão não foi informado',
  },
  {
    code: 'PAY-632',
    message: 'O código de segurança do cartão deve ser numérico',
  },
  {
    code: 'PAY-633',
    message:
      'A estrutura de informaççes do portador do cartão não foi informado',
  },
  {
    code: 'PAY-634',
    message: 'O nome do portador do cartão não foi informado',
  },
  {
    code: 'PAY-635',
    message:
      'O nome do portador do cartão Deve ter no mínimo 3 caracteres e no máxim',
  },
  {
    code: 'PAY-636',
    message: 'O nome do portador do cartão informado é inválido',
  },
  {
    code: 'PAY-637',
    message: 'A data de nascimento do portador do cartão não foi informado',
  },
  {
    code: 'PAY-638',
    message: 'A estrutura de documento do portador do cartão não foi informado',
  },
  {
    code: 'PAY-639',
    message: 'A estrutura de telefone do portador não foi informado',
  },
  {
    code: 'PAY-640',
    message:
      'A estrutura de endereço de cobrança do portador não foi informado',
  },
  {
    code: 'PAY-641',
    message: 'O número informado não é um número de cartão válido',
  },
  { code: 'PAY-642', message: 'Cartão de crédito não foi encontrado' },
  {
    code: 'PAY-643',
    message: 'A data de expiração do boleto bancário não foi informada',
  },
  {
    code: 'PAY-644',
    message: 'A data de expiração do boleto bancário é anterior a atual',
  },
  {
    code: 'PAY-645',
    message: 'A data de expiração do boleto bancário informado é inválida',
  },
  {
    code: 'PAY-646',
    message:
      'A primeira linha de instruções do boleto possui quantidade de caractere',
  },
  {
    code: 'PAY-647',
    message:
      'A segunda linha de instruções do boleto possui quantidade de caracteres',
  },
  {
    code: 'PAY-648',
    message:
      'A terceira linha de instruções do boleto possui quantidade de caractere',
  },
  {
    code: 'PAY-649',
    message: 'A uri de logo marca a ser aplicada no boleto é inválido',
  },
  {
    code: 'PAY-650',
    message: 'O número de identificação bancária não foi informado',
  },
  { code: 'PAY-651', message: 'O número de identificação bancária é inválido' },
  {
    code: 'PAY-652',
    message:
      'O número de identificação bancária informado é inválido para esta opera',
  },
  { code: 'PAY-653', message: 'A data de expiração não foi informada' },
  { code: 'PAY-654', message: 'A data de expiração informado é inválida' },
  {
    code: 'PAY-655',
    message: 'A uri de redirecionamento informada é inválido',
  },
  {
    code: 'PAY-656',
    message:
      'Não foi possível desfazer o pagamento, pois ele já está capturado na adquirente',
  },
  { code: 'PAY-657', message: 'Dados de pagamento MPOS inválidos' },
];

const transferError = [
  {
    code:'TRA-001',
    message:'Deve existir',
  },
  {
    code:'TRA-002',
    message:'Deve ser numérico',
  },
  {
    code:'TRA-003',
    message:'Deve ser uma string não vazia',
  },
  {
    code:'TRA-004',
    message:'Deve ser uma data-hora válida',
  },
  {
    code:'TRA-005',
    message:'Deve ser uma data válida',
  },
  {
    code:'TRA-006',
    message:'Deve ser um dos valores possíveis: {values}',
  },
  {
    code:'TRA-007',
    message:'Excedeu o tamanho máximo de {max} caracteres',
  },
  {
    code:'TRA-100',
    message:'Conta bancaria indicada não foi encontrada',
  },
  {
    code:'TRA-101',
    message:'Saldo disponível insuficiente para essa operação',
  },
  {
    code:'TRA-102',
    message:'Identidade da conta bancaria não encontrado',
  },
  {
    code:'TRA-103',
    message:'CPF/CNPJ com formato invalido',
  },
  {
    code:'TRA-104',
    message:'Divergência entre dados de CPF/CNPJ',
  },
  {
    code:'TRA-105',
    message:'Limite para saque alcançado',
  },
  {
    code:'TRA-106',
    message:'Erro de comunicação com o Financial',
  },
  {
    code:'TRA-107',
    message:'Sem integração com o Financial',
  },
  {
    code:'TRA-108',
    message:'Aconteceu um erro interno ao processar transferência',
  },
  {
    code:'TRA-109',
    message:'Valor requisitado inferior ao permitido',
  },
  {
    code:'TRA-110',
    message:'Conta Wirecard origem da transferência não existe',
  },
  {
    code:'TRA-111',
    message:'Conta Wirecard destino da transferência não existe',
  },
  {
    code:'TRA-144',
    message:'Ja foi feita uma transferencia neste valor, aguarde um momento',
  },
];

export const find = code =>
  [...paymentErrors, ...transferError].find(error => error.code === code);

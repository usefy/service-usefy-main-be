import { default as JsonError } from './JsonError';

export class BadRequestError extends JsonError {
  /**
   * @param {String} message Error message
   * @param {Number} errorCode Custom error code
   */
  constructor(message: string, info: any) {
    super(message, 400, info);
  }
}

import { default as JsonError } from './JsonError';

export class UnauthorizedError extends JsonError {
  /**
   * @param {String} message Error message
   * @param {Number} errorCode Custom error code
   */
  constructor(message: string, info: any) {
    super(message, 401, info);
  }
}

import { default as JsonError } from './JsonError';

export class TeapotError extends JsonError {
  /**
   * @param {String} message Error message
   * @param {Number} errorCode Custom error code
   */
  constructor(message: string, info: any) {
    // I'm a teapot
    // The server refuses the attempt to brew coffee with a teapot.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/418
    super(message, 418, info);
  }
}

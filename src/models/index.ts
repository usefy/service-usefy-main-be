import { auth } from 'firebase-admin';
import { Logger } from 'pino';

export type State = {
  correlationId?: string;
  sessionId?: string;
  logger: Logger;
  out?: any;
  user?: auth.DecodedIdToken;
};

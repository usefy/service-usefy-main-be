
/**
 * User's roles
 *
 * @export
 * @enum {number}
 */
export enum Roles {
  superAdmin = 'SUPER_ADMIN',
}

const CIO = require('customerio-node');
import { config } from '../../config';
import { State } from '../../models';

const cio = new CIO(config.customerio.siteID, config.customerio.trackingApiKey);
export interface EventParams {
  id: string;
  name: string;
  data?: object;
}

export interface IdentifyParams {
  id: string;
  email:string;
  name: string;
}

export const identify = async (params: IdentifyParams, state: State) => {
  try {
    if (config.customerio.enabled) {
      return cio.identify(params.id, {
        email: params.email,
        name: params.name,
        createdAt: new Date().valueOf(),
      });
    }
  } catch (error) {
    state.logger.error({ error }, 'Failed to send event message track');
  }
};

export const send = async (params: EventParams, state: State) => {
  try {
    if (config.customerio.enabled) {
      return cio.track(params.id, {
        name: params.name,
        data: params.data,
      });
    }
  } catch (error) {
    state.logger.error({ error }, 'Failed to send event message track');
  }
};

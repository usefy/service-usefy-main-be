import * as user from '../user';

const usersToRemove: string[] = [];
let userUid = '';

describe('Firebase User Component', () => {
  xit('Should create a new user', async () => {
    const newUser = await user.create({
      email: 'test@email.com',
      displayName: 'Tester',
      password: '123123',
      username: 'test',
      phoneNumber: '27999999999',
    });

    userUid = <string>newUser.uid;

    usersToRemove.push(<string>newUser.uid);

    expect(newUser).toBeDefined();
    expect(newUser.displayName).toEqual('Tester');
  });

  xit('Should get user data', async () => {
    const userData = await user.get(userUid);

    expect(userData).toBeDefined();
    expect(userData.displayName).toEqual('Tester');
  });

  xit('Should fail when create a new user without email', async () => {
    const param = {
      email: '',
      displayName: 'Tester',
      password: '123123',
      username: 'test',
      phoneNumber: '27999999999',
    };

    try {
      const newUser = await user.create(param);
      usersToRemove.push(<string>newUser.uid);

      // force fails
      expect(false).toBeTruthy();
    } catch (error) {
      expect(error).toBeDefined();
      expect(error.name).toEqual('BadGatewayError');
    }
  });

  xit('Should fail when create a new user without password', async () => {
    const param = {
      email: 'test@email.com',
      displayName: 'Tester',
    };

    try {
      const newUser = await user.create(param);
      usersToRemove.push(<string>newUser.uid);

      // force fails
      expect(false).toBeTruthy();
    } catch (error) {
      expect(error).toBeDefined();
      expect(error.name).toEqual('BadGatewayError');
    }
  });

  xit('Should fail when create a new user with same email', async () => {
    const param = {
      email: 'test@email.com',
      displayName: 'Tester 1',
      password: '123123',
    };

    try {
      const newUser = await user.create(param);
      usersToRemove.push(<string>newUser.uid);

      // force fails
      expect(false).toBeTruthy();
    } catch (error) {
      expect(error).toBeDefined();
      expect(error.name).toEqual('BadGatewayError');
    }
  });

  xit('Should create a new user with different email', async () => {
    const newUser = await user.create({
      email: 'test1@email.com',
      displayName: 'Tester',
      password: '123123',
      username: 'test',
      phoneNumber: '27999999999',
    });

    usersToRemove.push(<string>newUser.uid);

    expect(newUser).toBeDefined();
    expect(newUser.displayName).toEqual('Tester');
  });

  xit('Should update a user', async () => {
    const newUser = await user.update(userUid, {
      displayName: 'Tester Updated',
    });

    usersToRemove.push(<string>newUser.uid);

    expect(newUser).toBeDefined();
    expect(newUser.displayName).toEqual('Tester Updated');
  });

  afterAll(async () => {
    // remove all created users

    for (const u of usersToRemove) {
      await user.remove(u);
    }
  });
});

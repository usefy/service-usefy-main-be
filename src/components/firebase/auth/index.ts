import { logger } from '../../../logger';
import { errors } from '../../../errors';
import { getApp } from '../init';
import { State } from '../../../models';

const firebaseApp = getApp();

export async function createToken(uid: string) {
  // TODO: define additional claims on our token
  const additionalClaims = {
    custom: true,
  };

  try {
    const token = await firebaseApp.auth().createCustomToken(uid);

    logger.info({ uid }, 'Custom token generated for a user on firebase.');
    return token;
  } catch (err) {
    logger.error({ uid }, 'Error on generated a custom token for a user on Firebase');
    throw errors.badGateway('Error on generated a custom token for a user on Firebase', err);
  }
}

export async function revokeToken(uid: string) {

  try {
    await firebaseApp.auth().revokeRefreshTokens(uid);
  } catch (err) {
    logger.error({ uid }, 'Error on generated a custom token for a user on Firebase');
    throw errors.badGateway('Error on generated a custom token for a user on Firebase', err);
  }
}

export async function createOrUpdateUserInFirebase(uid: string, displayName: string, state: State) {
  let newUser;

  try {
    // update user if already exists
    newUser = firebaseApp.auth().updateUser(uid, {
      displayName,
    });

  } catch (err) {
    // if user not found create a new one
    if (err.code === 'auth/user-not-found') {
      state.logger.info({ uid, err }, 'User not found in firebase with custom auth, creating a new one');
      newUser = firebaseApp.auth().createUser({
        uid,
        displayName,
      });
    } else {
      state.logger.info({ uid, err }, 'Try to update or create firebase user with custom auth error.');
      throw errors.badGateway('Error creating or updating a new user using custom auth', { uid });
    }
  }

  return newUser;
}

export async function createUserInFirebase(uid: string, displayName: string, photoURL: string, state: State) {
  try {
    state.logger.info({ uid }, 'Creating new user using custom auth');
    await  firebaseApp.auth().createUser({
      uid,
      displayName,
      photoURL,
    });
  } catch (err) {
    state.logger.info({ uid, err }, 'Try to create firebase user with custom auth error');
    throw errors.badGateway('Error creating a new user using custom auth', { uid });
  }
}

export async function getUserInFirebase(uid: string, state: State) {
  try {
    const user = await firebaseApp.auth().getUser(uid);
    return user;
  } catch (err) {
    state.logger.info({ uid, err }, 'Error on getting user in firebase');
    throw errors.badGateway('Error on getting user in firebase', { uid });
  }
}

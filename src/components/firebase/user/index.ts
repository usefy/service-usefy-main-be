import * as admin from 'firebase-admin';
import { logger } from '../../../logger';
import { errors } from '../../../errors';
import { getApp } from '../init';

const firebaseApp = getApp();

export interface FirebaseUser {
  uid?: string;
  email?: string;
  password?: string;
  displayName?: string;
  username?: string;
  phoneNumber?: string;
  disabled?: boolean;
  emailVerified?: boolean;
  photoURL?: string;
  address?: any;
  metadata?: any;
  providerData?: any;
  passwordHash?: string;
  tokensValidAfterTime?: string | Date;
}

export async function create(user: FirebaseUser): Promise<FirebaseUser | admin.auth.UserRecord> {
  const newUser = Object.assign({}, user, {
    emailVerified: false,
    disabled: false,
  });

  try {
    const userRecord = await firebaseApp.auth().createUser(newUser);

    logger.info({ uid: userRecord.uid }, 'Successfully created new user on firebase.');

    return userRecord;
  } catch (err) {
    if (err.code === 'auth/email-already-exists') {
      throw errors.validation('Already exists email user on Firebase', err);
    }

    logger.error({ user, err }, 'Error on create a new user on Firebase');
    throw errors.badGateway('Error on create a new user on Firebase', err);
  }

}

export async function update(uid: string, user: FirebaseUser): Promise<FirebaseUser | admin.auth.UserRecord> {
  try {
    const userRecord = await firebaseApp.auth().updateUser(uid, user);

    logger.info({ uid: userRecord.uid }, 'Successfully updated a user on firebase.');

    return userRecord;
  } catch (err) {
    logger.error({ uid, user, err }, 'Error on update a user on Firebase');
    throw errors.badGateway('Error on update a user on Firebase', err);
  }
}

export async function get(uid: string): Promise<FirebaseUser | admin.auth.UserRecord> {
  try {
    const userRecord = await firebaseApp.auth().getUser(uid);
    logger.info({ uid: userRecord.uid }, 'Successfully fetched user data on Firebase.');

    return userRecord;
  } catch (err) {
    logger.error({ uid, err }, 'Error fetching user data on Firebase');
    throw errors.badGateway('Error fetching user data on Firebase', err);
  }
}

export async function remove(uid: string): Promise<void> {
  try {
    const userRecord = await firebaseApp.auth().deleteUser(uid);
    logger.info({ uid }, 'Successfully removed user data on Firebase.');
  } catch (err) {
    logger.error({ uid, err }, 'Error removed user data on Firebase');
    throw errors.badGateway('Error removed user data on Firebase', err);
  }
}

export async function setClaim(uid: string, claim: any) {
  try {
    const userRecord = await firebaseApp.auth().setCustomUserClaims(uid, claim);
    logger.info({ uid }, 'Successfully updated user claim on Firebase.');
  } catch (err) {
    logger.error({ uid, err }, 'Error updating user claim on Firebase');
    throw errors.badGateway('Error updating user claim on Firebase', err);
  }
}

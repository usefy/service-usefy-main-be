import * as admin from 'firebase-admin';
import { logger } from '../../logger';
import { config } from '../../config';

export let firebaseApp: admin.app.App;

export function getApp(): admin.app.App {
  if (process.env.CONFIG_ENV === 'test-unit') {
    // TODO: use initializeTestApp in authenticated routes
    return;
  }

  if (!firebaseApp) {
    logger.info('Initializing Firebase app');
    firebaseApp = admin.initializeApp(config.firebase);
    logger.info('Initializing success Firebase app');
  }

  return firebaseApp;
}

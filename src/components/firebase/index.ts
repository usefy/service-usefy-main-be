import { getApp } from './init';
import * as _user from './user';
import * as _auth from './auth';
import { errors } from '../../errors';

export const user = _user;
export const auth = _auth;

const firebaseApp = getApp();

export async function verifyIdToken(idToken, state) {
  try {
    const decodedIdToken = await firebaseApp.auth().verifyIdToken(idToken);

    state.logger.info('ID Token correctly decoded', decodedIdToken);

    return decodedIdToken;
  } catch (err) {
    state.logger.error('Error while verifying Firebase ID token:', err);
    throw errors.badGateway('Error while verifying Firebase ID token', err);
  }
}

export * from './init';

import { APIClient, SendEmailRequest } from 'customerio-node/api';
import { RegionUS } from 'customerio-node/regions';

import { State } from '../../models';
import { config } from '../../config';
export interface Notification {
  to: string;
  messageData: object;
  transactionMessageId: string;
  identifiers: object;
}

export const send = async (notification: Notification, state: State) => {
  try {
    if (config.customerio.enabled) {
      const customer =  new APIClient(config.customerio.appApiKey, { region: RegionUS });

      const message = new SendEmailRequest({
        to: notification.to,
        transactional_message_id: notification.transactionMessageId,
        message_data: notification.messageData,
        identifiers: notification.identifiers,
      });

      return customer.sendEmail(message);
    }
  } catch (err) {
    state.logger.error({ err }, 'Failed send message notification');
  }
};

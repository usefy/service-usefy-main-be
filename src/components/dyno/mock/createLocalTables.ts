import * as AWS from 'aws-sdk';

import { config } from '../../../config';
import { logger } from '../../../logger';
import { moipAccountTable } from './tables/moipAccount';
import { moipBankAccountTable } from './tables/moipBankAccount';
import { moipCustomerTable } from './tables/moipCustomer';
import { moipOrderTable } from './tables/moipOrder';
import { moipPaymentTable } from './tables/moipPayment';
import { requestLoggerTable } from './tables/requestLogger';
import { sportsAppsTable } from './tables/sportsApps';
import { usersTable } from './tables/user';
import { listingsTable, listingKeywordsTable, favoritesTable } from './tables/listings';
import { sports as sportsTable, populateSports } from './tables/sports';
import { roomsTable, messagesTable } from './tables/messages';
import { couponsTable } from './tables/coupons';
import { ordersTable } from './tables/orders';
import { clubsTable, clubsPartnerTable } from './tables/clubs';
import { moipCustomerFundingInstrumentTable } from './tables/moipCustomerFundingInstrument';
import { disputesTable } from './tables/disputes';
import { notificationsTable } from './tables/notifications';

// localhost/tests config
const opt = {
  region: 'us-east-1',
  accessKeyId: 'xxxx',
  secretAccessKey: 'xxxx',
  endpoint: config.dyno.endpoint,
};

logger.info(opt);

AWS.config.update(opt);
const dynamodb = new AWS.DynamoDB();

/* eslint-disable */
async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index += 1) {
    await callback(array[index]);
  }
}

export const createTables = async () => {
  const tablesParams = [
    usersTable,
    moipAccountTable,
    moipBankAccountTable,
    moipCustomerTable,
    moipCustomerFundingInstrumentTable,
    moipOrderTable,
    moipPaymentTable,
    requestLoggerTable,
    sportsAppsTable,
    sportsTable,
    listingsTable,
    listingKeywordsTable,
    roomsTable,
    messagesTable,
    couponsTable,
    ordersTable,
    clubsTable,
    clubsPartnerTable,
    disputesTable,
    favoritesTable,
    notificationsTable,
  ];

  logger.info('Start created tables');

  await asyncForEach(tablesParams, async (params) => {
    logger.info('table:', params.TableName);
    await dynamodb
      .describeTable({
        TableName: params.TableName,
      })
      .promise()
      .catch(async (err) => {
        logger.error(err);
        try {
          await dynamodb.createTable(params).promise();
          logger.info(`table ${params.TableName} created`);
        } catch (err) {
          logger.info(err);
        }
      });
  });

  await populateSports();

  logger.info('Created all sports');
};

import { config } from '../../../../config';

export const usersTable = {
  TableName: `usefy-${config.server.configEnv}-users`,
  KeySchema: [
    { AttributeName: 'firebaseUid', KeyType: 'HASH' }, // Partition key
  ],
  AttributeDefinitions: [
    { AttributeName: 'firebaseUid', AttributeType: 'S' },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

import { config } from '../../../../config';

export const listingsTable = {
  TableName: `usefy-${config.server.configEnv}-listings`,
  KeySchema: [
    { AttributeName: 'firebaseUid', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'id', KeyType: 'RANGE' }, // Sort key
  ],
  AttributeDefinitions: [
    { AttributeName: 'firebaseUid', AttributeType: 'S' },
    { AttributeName: 'id', AttributeType: 'S' },
  ],
  GlobalSecondaryIndexes: [
    {
      IndexName: 'id_index',
      KeySchema: [
        { AttributeName: 'id', KeyType: 'HASH' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

export const listingKeywordsTable = {
  TableName: `usefy-${config.server.configEnv}-listings-keywords`,
  KeySchema: [
    { AttributeName: 'id', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'keyword', KeyType: 'RANGE' }, // Sort key
  ],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'keyword', AttributeType: 'S' },
    { AttributeName: 'publishedAt', AttributeType: 'S' },
    { AttributeName: 'price', AttributeType: 'N' },
  ],
  LocalSecondaryIndexes: [
    {
      IndexName: 'id_publishedAt_index',
      KeySchema: [
        { AttributeName: 'id', KeyType: 'HASH' },
        { AttributeName: 'publishedAt', KeyType: 'RANGE' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
    },
    {
      IndexName: 'id_price_index',
      KeySchema: [
        { AttributeName: 'id', KeyType: 'HASH' },
        { AttributeName: 'price', KeyType: 'RANGE' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

export const favoritesTable = {
  TableName: `usefy-${config.server.configEnv}-listings-favorites`,
  KeySchema: [
    { AttributeName: 'uid', KeyType: 'HASH' }, // Range key
    { AttributeName: 'id', KeyType: 'RANGE' }, // Partition key
  ],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'uid', AttributeType: 'S' },
  ],
  GlobalSecondaryIndexes: [
    {
      IndexName: 'uid_index',
      KeySchema: [
        { AttributeName: 'uid', KeyType: 'HASH' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

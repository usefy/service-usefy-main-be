import { config } from '../../../../config';

export const roomsTable = {
  TableName: `usefy-${config.server.configEnv}-rooms`,
  KeySchema: [
    { AttributeName: 'listingId', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'id', KeyType: 'RANGE' }, // Sort key
  ],
  AttributeDefinitions: [
    { AttributeName: 'firebaseUid1', AttributeType: 'S' },
    { AttributeName: 'firebaseUid2', AttributeType: 'S' },
    { AttributeName: 'listingId', AttributeType: 'S' },
    { AttributeName: 'id', AttributeType: 'S' },
  ],
  GlobalSecondaryIndexes: [
    {
      IndexName: 'id_index',
      KeySchema: [
        { AttributeName: 'id', KeyType: 'HASH' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
    {
      IndexName: 'firebaseUid1_id_index',
      KeySchema: [
        { AttributeName: 'firebaseUid1', KeyType: 'HASH' },
        { AttributeName: 'id', KeyType: 'RANGE' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
    {
      IndexName: 'firebaseUid2_id_index',
      KeySchema: [
        { AttributeName: 'firebaseUid2', KeyType: 'HASH' },
        { AttributeName: 'id', KeyType: 'RANGE' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

export const messagesTable  = {
  TableName: `usefy-${config.server.configEnv}-messages`,
  KeySchema: [
    { AttributeName: 'roomId', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'id', KeyType: 'RANGE' }, // Sort key
  ],
  AttributeDefinitions: [
    { AttributeName: 'roomId', AttributeType: 'S' },
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'firebaseUid', AttributeType: 'S' },
    { AttributeName: 'createdAt', AttributeType: 'S' },
  ],
  GlobalSecondaryIndexes: [
    {
      IndexName: 'id_index',
      KeySchema: [
        { AttributeName: 'id', KeyType: 'HASH' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
    {
      IndexName: 'firebaseUid_id_index',
      KeySchema: [
        { AttributeName: 'firebaseUid', KeyType: 'HASH' },
        { AttributeName: 'id', KeyType: 'RANGE' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
  ],
  LocalSecondaryIndexes: [
    {
      IndexName: 'roomId_createdAt_index',
      KeySchema: [
        { AttributeName: 'roomId', KeyType: 'HASH' },
        { AttributeName: 'createdAt', KeyType: 'RANGE' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

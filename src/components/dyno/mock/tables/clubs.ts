import { config } from '../../../../config';

export const clubsTable = {
  TableName: `usefy-${config.server.configEnv}-clubs`,
  KeySchema: [
    { AttributeName: 'id', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'clubName', KeyType: 'RANGE' }, // Range key
  ],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'clubName', AttributeType: 'S' },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

export const clubsPartnerTable = {
  TableName: `usefy-${config.server.configEnv}-clubs-partner`,
  KeySchema: [
    { AttributeName: 'id', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'clubName', KeyType: 'RANGE' }, // Range key
  ],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'clubName', AttributeType: 'S' },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

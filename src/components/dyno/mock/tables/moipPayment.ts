import { config } from '../../../../config';

export const moipPaymentTable = {
  TableName: `usefy-${config.server.configEnv}-moipPayments`,
  KeySchema: [
    { AttributeName: 'id', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'uid', KeyType: 'RANGE' }, // Sort key
  ],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'uid', AttributeType: 'S' },
    { AttributeName: 'orderId', AttributeType: 'S' },
  ],
  GlobalSecondaryIndexes: [
    {
      IndexName: 'orderId_uid_index',
      KeySchema: [
        { AttributeName: 'orderId', KeyType: 'HASH' },
        { AttributeName: 'uid', KeyType: 'RANGE' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

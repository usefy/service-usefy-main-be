import { config } from '../../../../config';

export const notificationsTable = {
  TableName: `usefy-${config.server.configEnv}-notifications`,
  KeySchema: [
    { AttributeName: 'uid', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'id', KeyType: 'RANGE' }, // Range key
  ],
  AttributeDefinitions: [
    { AttributeName: 'uid', AttributeType: 'S' },
    { AttributeName: 'id', AttributeType: 'S' },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

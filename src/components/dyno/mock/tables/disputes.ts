import { config } from '../../../../config';

export const disputesTable = {
  TableName: `usefy-${config.server.configEnv}-disputes`,
  KeySchema: [
    { AttributeName: 'uid', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'orderId', KeyType: 'RANGE' }, // Range key
  ],
  AttributeDefinitions: [
    { AttributeName: 'uid', AttributeType: 'S' },
    { AttributeName: 'orderId', AttributeType: 'S' },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

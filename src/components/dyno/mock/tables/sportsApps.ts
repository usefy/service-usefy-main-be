import { config } from '../../../../config';

export const sportsAppsTable = {
  TableName: `usefy-${config.server.configEnv}-users-sports-apps`,
  KeySchema: [
    { AttributeName: 'uid', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'app', KeyType: 'RANGE' }, // Range key
  ],
  AttributeDefinitions: [
    { AttributeName: 'uid', AttributeType: 'S' },
    { AttributeName: 'app', AttributeType: 'S' },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

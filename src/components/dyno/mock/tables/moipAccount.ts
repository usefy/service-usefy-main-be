import { config } from '../../../../config';

export const moipAccountTable = {
  TableName: `usefy-${config.server.configEnv}-moipAccounts`,
  KeySchema: [
    { AttributeName: 'uid', KeyType: 'HASH' }, // Partition key
    { AttributeName: 'id', KeyType: 'RANGE' }, // Partition key
  ],
  AttributeDefinitions: [
    { AttributeName: 'uid', AttributeType: 'S' },
    { AttributeName: 'id', AttributeType: 'S' },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

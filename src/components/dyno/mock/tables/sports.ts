import * as _ from 'lodash';
import v4 from 'uuid/v4';
import { config } from '../../../../config';
import { logger } from '../../../../logger';
import moment = require('moment');
import { State } from '../../../../models';
import { sportsTable } from '../../../../modules/sports/tables/sportsTable';

export const sports = {
  TableName: `usefy-${config.server.configEnv}-sports`,
  KeySchema: [
    { AttributeName: 'id', KeyType: 'HASH' }, // Partition key
  ],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'parentId', AttributeType: 'S' },
  ],
  GlobalSecondaryIndexes: [
    {
      IndexName: 'parentId_id_index',
      KeySchema: [
        { AttributeName: 'parentId', KeyType: 'HASH' },
        { AttributeName: 'id', KeyType: 'RANGE' }],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
};

export async function populateSports() {
  const state = {
    logger,
  };

  const count = await sportsTable.countAll(<State>state);

  if (count && count.Count < 1) {
    const sports = [
      'Triatlo',
      'Ciclismo',
      'MTB',
      'Corrida',
      'Trail Run',
      'Dog Run',
      'Corrida de Aventura',
      'Natação',
      'Trekking',
      'Escalada',
      'Downhill',
      'Skate',
      'Wakeboarding',
      'Surf',
      'SUP',
      'Kitesurfing',
      'KiteFoil',
      'Ski',
      'Sports Fashion',
      'Fitness/Fortalecimento',
      'Camping',
      'Canoagem',
      'Canyoning',
      'Paraquedismo',
      'Para-pente',
      'Asa Delta',
      'Slack-line',
      'Wing-suit',
      'eSports',
    ];

    await sportsTable.deleteAll(['id'], state);

    for (const sport of sports!) {
      const document = {
        id: v4(),
        title: sport,
      };

      await sportsTable.create(
        _.merge({}, document, {
          createdAt: moment().toISOString(),
        }),
        state,
      );
    }
  }
}

import * as AWS from 'aws-sdk';
import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../config';
import { errors } from '../../errors';
import { State } from '../../models';

if (config.params.mockDynamodb) {
  const opt = {
    region: 'us-east-1',
    accessKeyId: 'xxxx',
    secretAccessKey: 'xxxx',
    endpoint: config.dyno.endpoint,
  };

  AWS.config.update(opt);
}

const dynamodb = new AWS.DynamoDB.DocumentClient({
  region: config.server.region,
});

export class DefaultModel {
  tableName: string;
  validationScheme: joi.SchemaLike;
  client: AWS.DynamoDB.DocumentClient;

  constructor(tableName: string, validationScheme: joi.SchemaLike) {
    this.tableName = tableName;
    this.validationScheme = validationScheme;
    this.client = dynamodb;
    // this.getOne = this.getOne.bind(this);
  }

  async get(key, state) {
    const params: AWS.DynamoDB.DocumentClient.GetItemInput = {
      TableName: this.tableName,
      Key: key,
      ConsistentRead: true,
    };
    // state.logger.info({ dynamodbCall: params }, `${this.tableName}.get`);
    const { Item } = await this.client.get(params).promise();
    // state.logger.info({ dynamodbResult: Item }, `${this.tableName}.get Result`);
    return Item;
  }

  async create(document, state) {
    const validation = joi.validate(document, this.validationScheme);
    if (validation.error) throw errors.validation(_.get(validation, 'error.message'), validation.error);

    const params = {
      TableName: this.tableName,
      Item: document,
      ConsistentRead: true,
    };

    // state.logger.info({ dynamodbCall: params }, `${this.tableName}.create`);
    const result = await this.client.put(params).promise();
    // state.logger.info({ dynamodbResult: result }, `${this.tableName}.create Result`);
    return document;
  }

  validate(document) {
    const validation = joi.validate(document, this.validationScheme);
    if (validation.error) throw errors.validation(_.get(validation, 'error.message'), validation.error);
    return document;
  }

  async delete(key, state) {
    const params = {
      Key: key,
      TableName: this.tableName,
    };

    // state.logger.info({ dynamodbCall: params }, `${this.tableName}.delete`);
    return this.client.delete(params).promise();
  }

  async deleteAll(keyList, state) {
    if (config.server.configEnv.toLowerCase() === 'prod') {
      // state.logger.error(
      //   {
      //     dynamodbCall: {
      //       TableName: this.tableName,
      //     },
      //   },
      //   'Atempted to delete all data in prod!!!!');

      throw new Error('Atempted to delete all data in prod!!!!');
    }

    // we are looping until the table is empty
    while (true) {
      const { Items } = await this.client.scan({
        TableName: this.tableName,
        ConsistentRead: true,
        Limit: 20,
      }).promise();

      if (!Items || _.isEmpty(Items)) {
        break;
      }

      const params = {
        RequestItems: {},
      };

      params.RequestItems[this.tableName] = Items.map((item) => {
        const key = {};
        keyList.forEach((k) => {
          key[k] = item[k];
        });
        return { DeleteRequest: { Key: key } };
      });

      // state.logger.info({ dynamodbCall: params }, 'batchWrite call to remove all items');
      // eslint-disable-next-line no-await-in-loop
      await this.client.batchWrite(params).promise();
    }
    return true;
  }

  async dynamoUpdate(params: AWS.DynamoDB.DocumentClient.UpdateItemInput, state: State) {
    // state.logger.info({ dynamodbCall: params }, `${this.tableName}.update`);
    const result = await this.client.update(params).promise();
    // state.logger.info({ dynamodbResult: result }, `${this.tableName}.update Result`);
    return result;
  }

  async dynamoQuery(params: AWS.DynamoDB.DocumentClient.QueryInput, state: State) {
    // state.logger.info({ dynamodbCall: params }, `${this.tableName}.query`);
    const result = await this.client.query(params).promise();
    // state.logger.info({ dynamodbResult: result }, `${this.tableName}.query Result`);
    return result;
  }

  async dynamoScan(params: AWS.DynamoDB.DocumentClient.ScanInput, state: State) {
    // state.logger.info({ dynamodbCall: params }, `${this.tableName}.scan`);
    const result = await this.client.scan(params).promise();
    // state.logger.info({ dynamodbResult: result }, `${this.tableName}.query Result`);
    return result;
  }

  async countAll(state: State) {
    const params = {
      TableName: this.tableName,
      ConsistentRead: true,
      Select: 'COUNT',
    };
    // state.logger.info({ dynamodbCall: params }, `${this.tableName}.countAll`);
    const result = await this.client.scan(params).promise();
    // state.logger.info({ dynamodbResult: result }, `${this.tableName}.query Result`);
    return result;
  }
}

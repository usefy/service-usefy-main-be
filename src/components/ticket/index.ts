import nodeZendesk from 'node-zendesk';

import { config } from '../../config';

const zendesk = nodeZendesk.createClient({
  username:  config.zendesk.username,
  token:     config.zendesk.token,
  remoteUri: config.zendesk.remoteUri,
});

export const ticket = {
  create: async (
    {
      message,
      priority = 'normal',
    },
    state,
  ) => {
    try {
      await zendesk.tickets.create({
        ticket: {
          priority,
          comment: {
            body: message,
          },
          subject: '[Curadoria] Novo Eqipo',
        },
      });
    } catch (err) {
      state.logger.error({ err }, 'Failed to create zendesk ticket');
    }
  },
};

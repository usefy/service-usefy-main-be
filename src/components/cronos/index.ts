import * as cron from 'node-cron';

// # ┌────────────── second (optional)
// # │ ┌──────────── minute
// # │ │ ┌────────── hour
// # │ │ │ ┌──────── day of month
// # │ │ │ │ ┌────── month
// # │ │ │ │ │ ┌──── day of week
// # │ │ │ │ │ │
// # │ │ │ │ │ │
// # * * * * * *

export enum ScheduleSetting {
  everyMinute = '* * * * *',
  everyTwoMinutes = '*/2 * * * *',
  everyThirtyMinutes = '*/30 * * * *',
  everyHour = '0 * * * *',
  everyDay5am = '0 5 * * *',
}

export interface Schedule {
  second?: string;
  minute: string;
  hour: string;
  dayOfMonth: string;
  month: string;
  dayOfWeek: string;
}

export const scheduleParser = (s: Schedule): string => {
  const {
    second,
    minute,
    hour,
    dayOfMonth,
    month,
    dayOfWeek,
  } = s;
  return `${second ? second : ''} ${minute} ${hour} ${dayOfMonth} ${month} ${dayOfWeek}`;
};

export const defaultOptions: cron.ScheduleOptions = {
  scheduled: true,
  timezone: 'America/Sao_Paulo',
};

export const cronTask = (schedule: Schedule | string, task: () => void, options: cron.ScheduleOptions = defaultOptions) => {
  const s = typeof schedule === 'string' ? schedule : scheduleParser(schedule);
  return cron.schedule(s, task, options);
};

import citiesByStateJson from '../tables/cities-by-state.json';

export const citiesByStateRepo = {
  all: () => citiesByStateJson,
};

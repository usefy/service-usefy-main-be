import supertest from 'supertest';
import { app } from '../../../app';

describe('Cities Module \/cities', () => {
  describe('Cities endpoint', () => {
    it('should return brazil cities and states information', async () => {
      await supertest(app)
        .get('/v1/cities-by-state')
        .expect(200);
    });
  });
});

import { Request, Response } from 'express';
import { citiesByStateService } from '../services/citiesByStateService';

export async function get(req: Request, res: Response) {
  return citiesByStateService(req.state);
}

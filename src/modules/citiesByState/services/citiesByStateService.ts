import { citiesByStateRepo } from '../repositories/citiesByStateRepo';
import { State } from '../../../models';

interface States {
  [stateId: string]: string;
}

interface City {
  id: number;
  state_id: number;
  name: string;
}

interface CitiesAndStates {
  states: States;
  cities: City[];
}

interface CitiesByState {
  [stateName: string]: String[];
}

export function adaptCitiesByState(citiesAndStates: CitiesAndStates): CitiesByState {
  const citiesByState = {};

  Object.entries(citiesAndStates.states).forEach(([stateId, stateName]) => {
    citiesByState[stateName] = citiesAndStates.cities
      .filter(city => city.state_id === parseInt(stateId, 10))
      .map(city => city.name);
  });

  return citiesByState;
}

export function citiesByStateService(state: State) {
  state.logger.info('CitiesByState service');

  const allCitiesAndStates = citiesByStateRepo.all();

  return adaptCitiesByState(allCitiesAndStates);
}

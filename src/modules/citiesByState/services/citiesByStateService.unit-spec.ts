import { adaptCitiesByState, citiesByStateService } from './citiesByStateService';
import { stateFactory } from '../../../../test/utils';

describe('CitiesByState Service', () => {
  describe('adaptCitiesByState', () => {
    describe('when receiving an object with states and cities', () => {
      it('should adadpt to object with cities by state', async () => {
        const statesAndCities = {
          states: { 1: 'Acre', 2: 'Roraima' },
          cities: [
            { state_id: 1, id: 91, name: 'Cidade do Acre 1' },
            { state_id: 1, id: 92, name: 'Cidade do Acre 2' },
            { state_id: 2, id: 93, name: 'Cidade de Roraima 1' },
            { state_id: 2, id: 94, name: 'Cidade de Roraima 2' },
          ],
        };

        const citiesByState = adaptCitiesByState(statesAndCities);
        const expectedCitiesByState = {
          Acre: ['Cidade do Acre 1', 'Cidade do Acre 2'],
          Roraima: ['Cidade de Roraima 1', 'Cidade de Roraima 2'],
        };

        expect(citiesByState).toEqual(expectedCitiesByState);
      });
    });
  });

  describe('citiesByStateService', () => {
    xit('should return citiesByState adapted from json file', async () => {

      const citiesByState = citiesByStateService(stateFactory());

      const stateNames = Object.keys(citiesByState);
      expect(stateNames).toHaveLength(27);

      stateNames.forEach((state) => {
        expect(citiesByState[state].length).toBeGreaterThan(0);
      });
    });
  });
});

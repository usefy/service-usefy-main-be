import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import * as citiesByStateController from '../controllers/citiesByStateController';

export const router = Router();

router
  .route('/cities-by-state')
  .get(
    routerDefaultHandler(citiesByStateController.get),
  );

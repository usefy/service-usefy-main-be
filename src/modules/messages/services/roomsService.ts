import get from 'lodash/get';
import v4 from 'uuid/v4';
import { State } from '../../../models';
import { errors } from '../../../errors';
import { roomsRepo } from '../repositories/rooms';
import { profileService } from '../../users/services/profileService';
import { getSingleListing } from '../../listings/services/listingService';
import { sendMessage } from './messagesService';
import * as orderService from '../../checkout/services/ordersService';

export async function getRoomService(roomId, state: State) {
  state.logger.info('Rooms service, get a Room');

  const room = await roomsRepo.getById(roomId, state);
  const listingItem = await getSingleListing({ id: room.listingId }, state);
  const users = await Promise.all([
    profileService({ username: undefined, uid: room.firebaseUid1 }, state),
    profileService({ username: undefined, uid: room.firebaseUid2 }, state),
  ]);

  const firstNames = users.map(user => user.displayName!.split(' ')[0]);

  return {
    listingItem,
    users,
    id: room.id,
    title: `${room.title} ${firstNames.join(' e ')}`,
    createdAt: room.createdAt,
  };
}

export async function getRoomByIdService(roomId, state: State) {
  try {
    const room = await roomsRepo.getById(roomId, state);
    const order = await orderService.getByListingId(room.listingId, state);
    const listingItem = await getSingleListing({ id: room.listingId }, state);

    const users = await Promise.all([
      profileService({ username: undefined, uid: room.firebaseUid1 }, state),
      profileService({ username: undefined, uid: room.firebaseUid2 }, state),
    ]);

    const firstNames = users.map(user => user.displayName!.split(' ')[0]);

    return {
      listingItem,
      users,
      order: order ? {
        id: order.id,
        orderStatus: order.orderStatus,
        createdAt: order.createdAt,
        buyer: {
          uid: order.uid,
        },
      } : null,
      id: room.id,
      title: `${room.title} ${firstNames.join(' e ')}`,
      createdAt: room.createdAt,
    };
  } catch (error) {
    throw error;
  }
}

export async function getUsersRoomsService(query, state: State) {
  state.logger.info('Rooms service, get users Rooms');

  const offset = get(query, 'offset');
  const limit = get(query, 'limit');
  const archived = get(query, 'archived');

  try {
    const rooms = await roomsRepo.getByUid(state.user.uid, state);

    const response = await Promise.all(rooms.map(async (room) => {
      const listingItem = await getSingleListing({ id: room.listingId }, state);
      const order = await orderService.getByListingId(room.listingId, state);
      const users = await Promise.all([
        profileService({ username: undefined, uid: room.firebaseUid1 }, state),
        profileService({ username: undefined, uid: room.firebaseUid2 }, state),
      ]);

      const firstNames = users.map(user => user.displayName!.split(' ')[0]);

      return {
        listingItem,
        users,
        order: order ? {
          id: order.id,
          orderStatus: order.orderStatus,
          createdAt: order.createdAt,
          user: {
            uid: order.uid,
          },
        } : null,
        id: room.id,
        title: `${room.title} ${firstNames.join(' e ')}`,
        createdAt: room.createdAt,
      };
    }));
    return response;
  } catch (err) {
    state.logger.error({ offset, limit, archived }, 'Error requesting users messages rooms');
    throw err;
  }
}

export async function newRoomService(body, state: State) {
  state.logger.info('Rooms service, create a new room');

  const {
    listingId,
    message,
  } = body;

  const listing = await getSingleListing({ id: listingId }, state);

  try {
    const users = await Promise.all([
      profileService({ username: undefined, uid: listing.firebaseUid }, state),
      profileService({ username: undefined, uid: state.user.uid }, state),
    ]);

    const firstNames = users.map(user => user.displayName!.split(' ')[0]);

    const [alreadyCreatedRoom] = await roomsRepo.getByUserUidAndEquipoId(
      state.user.uid,
      listingId,
      state,
    );

    if (alreadyCreatedRoom) {
      state.logger.info({ listingId, uid: state.user.uid }, 'Room already created');

      return {
        users,
        id: alreadyCreatedRoom.id,
        title: `${alreadyCreatedRoom.title} ${firstNames.join(' e ')}`,
        createdAt: alreadyCreatedRoom.createdAt,
        listingItem: listing,
      };
    }

    state.logger.info({ listingId, uid: state.user.uid }, 'Creating a new message room');

    const room = await roomsRepo.create(
      {
        id: v4(),
        title: listing.title,
        listingId: listing.id,
        firebaseUid1: listing.firebaseUid,
        firebaseUid2: state.user.uid,
      },
      state,
    );

    state.logger.info({ room }, 'Room created with success');

    return {
      users,
      id: room.id,
      title: `${room.title} ${firstNames.join(' e ')}`,
      createdAt: room.createdAt,
      listingItem: listing,
    };
  } catch (err) {
    state.logger.error({ listingId, message, err }, 'Error on creating a new room or sending the first message.');
    throw errors.serverError('Error creating a new message room', { listingId, message });
  }
}

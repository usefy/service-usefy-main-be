import merge from 'lodash/merge';
import v4 from 'uuid/v4';
import moment = require('moment');

import { State } from '../../../models';
import { messagesRepo } from '../repositories/messages';
import * as tracking from '../../../components/tracking';
import { config } from '../../../config';
import { roomsGet } from '../controllers/roomsController';
import { roomsRepo } from '../repositories/rooms';

export async function sendMessage({ roomId, text, photoUrl = '' }, state: State) {
  try {
    const message = await messagesRepo.create(
      {
        text,
        roomId,
        photoUrl,
        id: v4(),
        firebaseUid: state.user.uid,
      },
      state,
    );
    state.logger.info({ message }, 'Message sent with success.');

    await tracking.send(
      {
        id: state.user.uid,
        name: config.customerio.track.message.received,
        data: {
          message: text,
        },
      },
      state,
    );

    return {
      uid: message.firebaseUid,
      ...message,
    };
  } catch (err) {
    state.logger.error({ roomId, text, uid: state.user.uid }, 'Error sending a message.');
    throw err;
  }
}

function buildNext(params) {
  const tempParams = Object.keys(params).map((item) => {
    return item === 'offset' ? `${item}=${encodeURIComponent(JSON.stringify(params[item]))}` : `${item}=${params[item]}`;
  });
  return `?${tempParams.join('&')}`;
}

function messageBuilder(itens) {
  const messages = itens.map((message) => {
    return {
      uid: message.firebaseUid,
      text: message.text,
      id: message.id,
      createdAt: message.createdAt,
      readAt: message.readAt,
      photoUrl: message.photoUrl,
    };
  });

  return { messages, count: messages!.length || 0 };
}

export async function getMessagesPaginate(params, state: State) {
  const { roomId, offset = undefined, limit = 10, reverse = false } = params;
  const options = {
    limit,
    reverse: reverse === 'true',
  };

  if (offset) {
    const { id, createdAt } = await messagesRepo.getById({ roomId, id: offset }, state);

    const { data } = await messagesRepo.getByRoomIdPaginate(
      roomId,
      {
        ...options,
        exclusiveStartKey: {
          id,
          roomId,
          createdAt,
        },
      },
      state);

    return messageBuilder(data);
  }

  const { data } = await messagesRepo.getByRoomId(roomId, options, state);

  return messageBuilder(data);
}

export async function getHasMessagesUnread(state: State) {
  const rooms = await roomsRepo.getByUid(state.user.uid, state);
  const messages = [];

  await Promise.all(rooms.map(async (room) => {
    messages.push(...await messagesRepo.getByMessagesUnread(room.id, state));
  }));

  return { count: messages.length };
}

export async function getMessages(params, state: State) {
  const { roomId, offset = undefined, limit = 24, reverse = false, prefix = '/v1/messages' } = params;
  let messages: any[] = [];
  let parsedOffset = offset ? JSON.parse(decodeURIComponent(offset)) : undefined;

  do {
    const options = {
      limit,
      reverse: reverse === 'true',
      offset: parsedOffset,
    };

    const { data, lastKey } = await messagesRepo.getByRoomId(roomId, options, state);

    parsedOffset = lastKey;
    messages = messages.concat(data!.map((message) => {
      return {
        uid: message.firebaseUid,
        text: message.text,
        id: message.id,
        createdAt: message.createdAt,
        readAt: message.readAt,
        photoUrl: message.photoUrl,
      };
    }));

  } while (messages.length < limit && parsedOffset);

  const response: {
    messages: any[],
    count: number,
    next?: string,
  } = { messages, count: messages!.length || 0 };

  if (parsedOffset) {
    const toBuild = merge({}, params, { offset: parsedOffset }, { limit });
    response.next = `${prefix}/${buildNext(toBuild)}`;
  }

  return response;
}

export const updateMessageRoomById = async (params, state: State) => {
  const { roomId, id } = params;

  const message = await messagesRepo.update({ id, roomId }, { readAt: moment().toISOString() }, state);

  return message;
};

import * as _ from 'lodash';
import moment from 'moment';
import { roomsTable } from '../tables/roomsTable';
import { State } from '../../../models';

export const roomsRepo = {

  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return roomsTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  getById: (id: string, state: State) => roomsTable.getById(id, state),

  getByUid: (uid: string, state: State) => roomsTable.getByUid(uid, state),

  getByUserUidAndEquipoId: (uid: string, equipoId: string, state: State) => roomsTable.getByUserUidAndEquipoId(uid, equipoId, state),

  validate: document => roomsTable.validate(document),

  countAll: (state: State) => roomsTable.countAll(state),
};

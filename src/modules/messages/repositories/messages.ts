import * as _ from 'lodash';
import moment from 'moment';
import { messagesTable } from '../tables/messagesTable';
import { State } from '../../../models';
import { MessageDocument } from '../models';

export const messagesRepo = {

  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return messagesTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  getById: ({ id, roomId }, state: State) => messagesTable.getById({ id, roomId }, state),

  getByRoomId: (roomId: string, { limit, reverse }, state: State) =>
    messagesTable.getByRoomId({ roomId, limit, reverse }, state),

  getByMessagesUnread: (roomId, state: State) => messagesTable.getMessagesUnread(roomId, state),

  getByRoomIdPaginate: (roomId: string, { exclusiveStartKey, limit, reverse }, state: State) =>
    messagesTable.getByRoomIdPaginate({ roomId, exclusiveStartKey, limit, reverse }, state),

  validate: document => messagesTable.validate(document),

  countAll: (state: State) => messagesTable.countAll(state),

  update: async (
    { id, roomId }: { id: string, roomId: string },
    document: Partial<MessageDocument>,
    state: State,
  ): Promise<MessageDocument> => messagesTable.update({ id, roomId }, document, state),

};

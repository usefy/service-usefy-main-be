import get from 'lodash/get';
import { Request } from 'express';
import { newRoomService, getUsersRoomsService, getRoomByIdService } from '../services/roomsService';
import { sendMessage, getMessagesPaginate } from '../services/messagesService';

export async function roomsGet(req: Request) {
  return getUsersRoomsService(req.query, req.state);
}

export async function roomGetById(req: Request) {
  return getRoomByIdService(req.params.roomId, req.state);
}

export async function roomsPost(req: Request) {
  return newRoomService(req.body, req.state);
}

export async function roomMessagePost(req: Request) {
  return sendMessage(
    {
      roomId: req.params.roomId,
      text: req.body.text,
      photoUrl: req.body.photoUrl,
    },
    req.state,
  );
}

export async function roomMessageGet(req: Request) {
  const params = {
    roomId: req.params.roomId,
    offset: get(req.query, 'offset', undefined),
    limit: get(req.query, 'limit', undefined),
    reverse: get(req.query, 'reverse', undefined),
    prefix: `/v1/rooms/${req.params.roomId}/messages`,
  };

  return getMessagesPaginate(params, req.state);
}

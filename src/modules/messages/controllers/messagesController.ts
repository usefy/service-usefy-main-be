import { Request, Response } from 'express';
import {
  sendMessage,
  getMessages,
  updateMessageRoomById,
  getHasMessagesUnread,
} from '../services/messagesService';

export async function messagePost(req: Request, res: Response) {
  return sendMessage(req.body, req.state);
}

export async function messagesGet(req: Request) {
  return getMessages(req.query, req.state);
}

export async function messageHasUnreadGet(req: Request) {
  return getHasMessagesUnread(req.state);
}

export async function putMessageRoomById(req: Request) {
  return updateMessageRoomById(req.params, req.state);
}

export interface MessageDocument {
  id: string;
  uid: string;
  roomId: string;
  text: string;
  createdAt?: Date | string;
  readAt?: Date | string;
  photoUrl?: string;
}

import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';

const tableName = `usefy-${config.server.configEnv}-rooms`;

// NOTE: firebaseUid1 will be aways the listing publisher

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  title: joi.string().required(),
  listingId: joi.string().required(),
  firebaseUid1: joi.string().required(),
  firebaseUid2: joi.string().required(),
  createdAt: joi.date(),
  archivedAt: joi.date(),
  updatedAt: joi.date(),
});

export class RoomModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUid(uid: string, state: State) {
    const params1 = {
      TableName: this.tableName,
      IndexName: 'firebaseUid1_id_index',
      KeyConditionExpression: '#firebaseUid1 = :firebaseUid1',
      // ignore archived rooms
      FilterExpression: 'attribute_not_exists(archivedAt)',
      ExpressionAttributeNames: {
        '#firebaseUid1': 'firebaseUid1',
      },
      ExpressionAttributeValues: {
        ':firebaseUid1': uid,
      },
      ConsistentRead: false,
    };
    const query1 = this.dynamoQuery(params1, state);

    const params2 = {
      TableName: this.tableName,
      IndexName: 'firebaseUid2_id_index',
      KeyConditionExpression: '#firebaseUid2 = :firebaseUid2',
      // ignore archived rooms
      FilterExpression: 'attribute_not_exists(archivedAt)',
      ExpressionAttributeNames: {
        '#firebaseUid2': 'firebaseUid2',
      },
      ExpressionAttributeValues: {
        ':firebaseUid2': uid,
      },
      ConsistentRead: false,
    };
    const query2 = this.dynamoQuery(params2, state);

    // wait for both queries
    const results = await Promise.all([query1, query2]);

    return _.concat(_.get(results[0], 'Items', []), _.get(results[1], 'Items', []));
  }

  async getById(id: string, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'id_index',
      KeyConditionExpression: '#id = :id',
      // ignore archived rooms
      FilterExpression: 'attribute_not_exists(archivedAt)',
      ExpressionAttributeNames: {
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
      },
      ConsistentRead: false,
    };
    const { Items: data } = await this.dynamoQuery(params, state);
    return data![0];
  }

  async getByUserUidAndEquipoId(uid: string, equipoId: string, state: State) {
    const params1 = {
      TableName: this.tableName,
      FilterExpression: 'attribute_not_exists(archivedAt) AND #firebaseUid1 = :firebaseUid1 AND #listingId = :listingId',
      ExpressionAttributeNames: {
        '#firebaseUid1': 'firebaseUid1',
        '#listingId':  'listingId',
      },
      ExpressionAttributeValues: {
        ':firebaseUid1': uid,
        ':listingId': equipoId,
      },
      ConsistentRead: false,
    };
    const query1 = this.dynamoScan(params1, state);

    const params2 = {
      TableName: this.tableName,
      FilterExpression: 'attribute_not_exists(archivedAt) AND #firebaseUid2 = :firebaseUid2 AND #listingId = :listingId',
      ExpressionAttributeNames: {
        '#firebaseUid2': 'firebaseUid2',
        '#listingId':  'listingId',
      },
      ExpressionAttributeValues: {
        ':firebaseUid2': uid,
        ':listingId': equipoId,
      },
      ConsistentRead: false,
    };
    const query2 = this.dynamoScan(params2, state);

    // wait for both queries
    const results = await Promise.all([query1, query2]);

    return _.concat(_.get(results[0], 'Items', []), _.get(results[1], 'Items', []));
  }
}

export const roomsTable = new RoomModel(tableName, validationScheme);

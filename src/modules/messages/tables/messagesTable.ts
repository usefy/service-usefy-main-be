import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';
import { MessageDocument }  from '../models';

const tableName = `usefy-${config.server.configEnv}-messages`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  text: joi.string().required(),
  roomId: joi.string().required(),
  firebaseUid: joi.string().required(),
  createdAt: joi.date(),
  deletedAt: joi.date(),
  readAt: joi.date(),
  photoUrl: joi.string().allow(''),
});

export class MessageModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getById({ id, roomId }, state: State) {
    const params = {
      TableName: this.tableName,
      FilterExpression: 'attribute_not_exists(deletedAt)',
      KeyConditionExpression: '#id = :id and #roomId = :roomId ',
      ExpressionAttributeNames: {
        '#id': 'id',
        '#roomId': 'roomId',
      },
      ExpressionAttributeValues: {
        ':id': id,
        ':roomId': roomId,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items![0];
  }

  async getMessagesUnread (roomId, state: State) {
    const params = {
      TableName: this.tableName,
      FilterExpression: 'attribute_not_exists(readAt)',
      KeyConditionExpression: '#roomId = :roomId',
      ExpressionAttributeNames: {
        '#roomId': 'roomId',
      },
      ExpressionAttributeValues: {
        ':roomId': roomId,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items;
  }

  async getByRoomId({ roomId, limit, reverse = false }, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'roomId_createdAt_index',
      KeyConditionExpression: '#roomId = :roomId',
      // ignore removed messages
      FilterExpression: 'attribute_not_exists(deletedAt)',
      ExpressionAttributeNames: {
        '#roomId': 'roomId',
      },
      ExpressionAttributeValues: {
        ':roomId': roomId,
      },
      ConsistentRead: false,
      ScanIndexForward: !reverse,
      Limit: limit,
    };
    const result = await this.dynamoQuery(params, state);
    return { data: result.Items, lastKey: result.LastEvaluatedKey };
  }

  async getByRoomIdPaginate({ roomId, exclusiveStartKey, limit, reverse = false }, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'roomId_createdAt_index',
      KeyConditionExpression: '#roomId = :roomId',
      // ignore removed messages
      FilterExpression: 'attribute_not_exists(deletedAt)',
      ExpressionAttributeNames: {
        '#roomId': 'roomId',
      },
      ExpressionAttributeValues: {
        ':roomId': roomId,
      },
      ExclusiveStartKey: exclusiveStartKey,
      ConsistentRead: false,
      ScanIndexForward: !reverse,
      Limit: limit,
    };
    const result = await this.dynamoQuery(params, state);
    return { data: result.Items, lastKey: result.LastEvaluatedKey };
  }

  async update(
    { id, roomId }: { id: string, roomId: string },
    document: Partial<MessageDocument>,
    state: State,
  ): Promise<MessageDocument> {
    // tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    for (const key in document) {
      if (document.hasOwnProperty(key) && document[key]) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(document, key);
      }
    }

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        roomId,
        id,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);

    return <MessageDocument>data;
  }
}

export const messagesTable = new MessageModel(tableName, validationScheme);

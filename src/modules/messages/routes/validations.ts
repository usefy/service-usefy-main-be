import * as joi from '@hapi/joi';

export const roomsGetById: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      roomId: joi.string().required(),
    })
    .required(),
});

export const roomsGet: joi.SchemaLike = joi.object().keys({
  query: joi
    .object()
    .keys({
      offset: joi.string(),
      limit: joi.number(),
      archived: joi.boolean(),
    })
    .required(),
});

export const roomsPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      listingId: joi.string().required(),
    })
    .required(),
});

export const roomMessagePost: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      roomId: joi.string().required(),
    }),
  body: joi
    .object()
    .keys({
      text: joi.string().required(),
    })
    .required(),
});

export const messagePost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      roomId: joi.string().required(),
      text: joi.string().required(),
    })
    .required(),
});

export const messagesGet: joi.SchemaLike = joi.object().keys({
  query: joi
    .object()
    .keys({
      roomId: joi.string().required(),
      offset: joi.string(),
      limit: joi.number(),
      reverse: joi.boolean(),
    })
    .required(),
});

export const messagesPut: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      roomId: joi.string().required(),
      id: joi.string().required(),
    })
    .required(),
});

export const roomMessageGet: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      roomId: joi.string().required(),
    }),
  query: joi
    .object()
    .keys({
      offset: joi.string(),
      limit: joi.number(),
      reverse: joi.boolean(),
    }),
});

import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';
import { roomsPost, roomsGet, roomsGetById, messagePost, roomMessagePost, messagesGet, roomMessageGet, messagesPut } from './validations';
import * as roomsController from '../controllers/roomsController';
import * as messagesController from '../controllers/messagesController';

export const router = Router();

router
  .route('/rooms')
  .get(
    validateFirebaseIdToken,
    validateReqParams(roomsGet),
    routerDefaultHandler(roomsController.roomsGet),
  )
  .post(
    validateFirebaseIdToken,
    validateReqParams(roomsPost),
    routerDefaultHandler(roomsController.roomsPost),
  );

router
  .route('/rooms/:roomId')
  .get(
    validateFirebaseIdToken,
    validateReqParams(roomsGetById),
    routerDefaultHandler(roomsController.roomGetById),
  );

router
  .route('/rooms/:roomId/messages')
  .get(
    validateFirebaseIdToken,
    validateReqParams(roomMessageGet),
    routerDefaultHandler(roomsController.roomMessageGet),
  )
  .post(
    validateFirebaseIdToken,
    validateReqParams(roomMessagePost),
    routerDefaultHandler(roomsController.roomMessagePost),
  );

router
  .route('/rooms/:roomId/messages/:id')
  .put(
    validateFirebaseIdToken,
    validateReqParams(messagesPut),
    routerDefaultHandler(messagesController.putMessageRoomById),
  );

router
  .route('/message')
  .post(
    validateFirebaseIdToken,
    validateReqParams(messagePost),
    routerDefaultHandler(messagesController.messagePost),
  );

router
  .route('/messages')
  .get(
    validateFirebaseIdToken,
    validateReqParams(messagesGet),
    routerDefaultHandler(messagesController.messagesGet),
  );

router
  .route('/messages/unread')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(messagesController.messageHasUnreadGet),
  );

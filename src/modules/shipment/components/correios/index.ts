import { State } from '../../../../models';
import { errors } from '../../../../errors';
export * from './remote/correiosSRO';
export * from './validation';

// tslint:disable-next-line: variable-name
const Correios = require('node-correios');

export const services = {
  // 4014: 'SEDEX à vista',
  // 4065: 'SEDEX à vista pagamento na entrega',
  4510: 'PAC à vista',
  // 4707: 'PAC à vista pagamento na entrega',
  // 40169: 'SEDEX12 ( à vista e a faturar)',
  // 40215: 'SEDEX 10 (à vista e a faturar)',
  // 40290: 'SEDEX Hoje Varejo',
};

// export const formats = {
//   '1': 'Formato caixa/pacote',
//   '2': 'Formato rolo/prisma',
//   '3': 'Envelope',
// };

export const defaultFormat = 1;

export interface AddressResponse {
  bairro: string;
  cep: string;
  localidade: string;
  logradouro: string;
  uf: string;
}

export interface AddressResult {
  district: string;
  cep: string;
  city: string;
  street: string;
  state: string;
}

export interface QuoteQuery {
  cepFrom: string;
  cepTo: string;
  price: number;
  weight: number;
  depth: number;
  height?: number | undefined;
  width?: number | undefined;
  diameter?: number | undefined;
}

export interface Quote {
  title: string;
  serviceCode: string;
  price: number;
  priceOwnHand: number;
  priceReceiptNotice: number;
  priceDeclaredValue: number;
  error?: string;
  message?: string;
  priceNoExtras: number;
  deliveryTime: string;
}

export interface QuoteResponse {
  services: Quote[];
  destinationAddress: AddressResult;
}

export interface CorreiosResponse {
  Codigo: string;
  Valor: number;
  ValorMaoPropria: number;
  ValorAvisoRecebimento: number;
  ValorValorDeclarado: number;
  Erro: string | undefined;
  MsgErro: string | undefined;
  ValorSemAdicionais: number;
  PrazoEntrega: string;
}

export interface AddressQuery {
  cep: string;
}

export const getQuote = async (
  {
    cepFrom,
    cepTo,
    price,
    weight,
    depth,
    height = undefined,
    width = undefined,
    diameter = undefined,
  }: QuoteQuery,
  state: State,
): Promise<QuoteResponse> => {
  const correios = new Correios();

  const options = {
    nCdServico: Object.keys(services).join(','),
    sCepOrigem: cepFrom,
    sCepDestino: cepTo,
    nVlValorDeclarado: price,
    nVlPeso: weight,
    nCdFormato: defaultFormat,
    nVlComprimento: depth,
    nVlAltura: 0,
    nVlLargura: 0,
    nVlDiametro: 0,
  };

  if (height) {
    options['nVlAltura'] = height;
  }
  if (width) {
    options['nVlLargura'] = width;
  }
  if (diameter) {
    options['nVlDiametro'] = diameter;
  }

  try {
    const response: CorreiosResponse[] = await correios.calcPrecoPrazo(options);
    state.logger.info({ correiosResponse: response }, 'Correios quote result');

    const address: AddressResponse = await correios.consultaCEP({ cep: cepTo });
    state.logger.info({ correiosResponse: address }, 'Correios address result');

    const quotes = response.map((quote: CorreiosResponse): Quote => {
      const newQuote: Quote = {
        title: services[quote.Codigo],
        serviceCode: quote.Codigo,
        price: quote.Valor,
        priceOwnHand: quote.ValorMaoPropria,
        priceReceiptNotice: quote.ValorAvisoRecebimento,
        priceDeclaredValue: quote.ValorValorDeclarado,
        error: quote.Erro,
        message: quote.MsgErro,
        priceNoExtras: quote.ValorSemAdicionais,
        deliveryTime: quote.PrazoEntrega,
      };

      return newQuote;
    });

    const result = {
      services: quotes,
      destinationAddress: {
        district: address.bairro,
        cep: address.cep,
        city: address.localidade,
        street: address.logradouro,
        state: address.uf,
      },
    };

    return result;
  } catch (err) {
    state.logger.error({ err }, 'Correios get quote error.');
    throw errors.badGateway('Correios get quote error', {});
  }
};

import axios from 'axios';
import xml2js from 'xml2js';
import { has, get } from 'lodash';

import { config } from '../../../../../config';
import { State } from '../../../../../models';
import { errors } from '../../../../../errors';

export interface SROConfig {
  username: string;
  password: string;
  type: string;
  result: string;
  language: string;
  limit: number;
  filter: boolean;
}

export const defaultConfig: SROConfig = {
  username: config.correios.SRO.username,
  password: config.correios.SRO.password,
  type: config.correios.SRO.type,
  result: config.correios.SRO.result,
  language: config.correios.SRO.language,
  limit: config.correios.SRO.limit,
  filter: config.correios.SRO.filter,
};

const remote = axios.create({
  baseURL: config.correios.SRO.baseUrl,
  headers: {
    Accept: 'text/xml',
    'Content-Type': 'text/xml; charset=utf-8',
    'cache-control': 'no-cache',
    'User-Agent': `Usefy/${config.version.apiVersion}`,
  },
});

const parseXML = (text, state: State): Promise<any> => new Promise((resolve, reject) => {
  xml2js.parseString(text, (err, object) => {
    if (!err) {
      state.logger.info({ object }, 'Success parsing XML');
      resolve(object);
    } else {
      state.logger.info({ text, err, object }, 'Error parsing XML');
      reject(errors.validation('Not able to parse XML response', {}));
    }
  });
});

const createSOAPEnvelope = (objects: string[], config?: SROConfig) => {
  let envelope = `<?xml version="1.0"?>
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:res="http://resource.webservice.correios.com.br/">
     <soapenv:Header/>
     <soapenv:Body>
        <res:buscaEventosLista>
  `;

  if (config.username && config.password) {
    envelope += `         <usuario>${config.username}</usuario>
             <senha>${config.password}</senha>
    `;
  }

  envelope += `         <tipo>${config.type}</tipo>
           <resultado>${config.result}</resultado>
           <lingua>${config.language}</lingua>
  `;

  envelope += objects.map((object: string) => `         <objetos>${object}</objetos>
  `).join('');

  envelope += `      </res:buscaEventosLista>
     </soapenv:Body>
  </soapenv:Envelope>`;
  return envelope;
};

const expand = (item) => {
  if (Array.isArray(item)) {
    if (item.length === 1) {
      return expand(item[0]);
    }
    return item.map(expand);
  }

  if (item === Object(item)) {
    for (const key in item) {
      item[key] = expand(item[key]);
    }
  } else if (typeof item === 'string' || item instanceof String) {
    return item.trim();
  }

  return item;
};

export const trackPackage = async (objects: string, sroConfig: SROConfig = defaultConfig, state: State) => {

  try {
    const data = createSOAPEnvelope([objects], sroConfig);
    const response = await remote.post('/service/rastro', data);

    state.logger.info({ axiosResponse: response.data }, 'Success on tracking object on Correios');

    const parsed = await parseXML(response.data, state);

    const trackedObjects = get(
      parsed,
      'soapenv:Envelope.soapenv:Body[0].ns2:buscaEventosListaResponse[0].return[0].objeto',
    ).map(expand);

    state.logger.info({ trackedObjects }, 'Success response parsed from Correios');

    return trackedObjects[0];
  } catch (err) {
    if (has(err, 'config')) {
      // means HTTP axios error
      const response = await parseXML(err.data, state);
      const error = expand(get(response, 'soapenv:Envelope.soapenv:Body[0].soapenv:Fault[0].faultstring[0]'));
      state.logger.info(
        {
          axiosError: err,
          axiosResponse: get(err, 'response.data'),
        },
        'Error calling Correios tracker service',
      );
      throw errors.badGateway('Error tracking packages on Correios', { error });
    }

    state.logger.warn({ err }, 'Unknown error on tracking packages on Correios');
    throw err;
  }
};

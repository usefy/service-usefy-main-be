import axios from 'axios';

import { State } from '../../../../models';
import { errors } from '../../../../errors';

const remote = axios.create({
  baseURL: 'https://brasilapi.com.br/api/',
});

export interface CEPQuery {
  cep: string;
  state: string;
  city: number;
  neighborhood: number;
  street: number;
}

export const getCEP = async ({ cep }, state: State): Promise<CEPQuery> => {
  try {
    const { data } = await remote.get(`/cep/v1/${cep}`);

    return data;
  } catch (err) {
    state.logger.warn({ err }, 'Cep: Error get cep detail');
    throw errors.badGateway('Error get cep detail', {});
  }
};

import { Request } from 'express';
import { getPendingOrders } from '../../checkout/services/ordersService';

export async function getCuration(req: Request) {
  return getPendingOrders(req.state);
}

import { Request } from 'express';
import { getQuote, getCEP } from '../services/correiosService';

export async function correiosGet(req: Request) {
  return getQuote(req.query, req.state);
}

export async function cepGet(req: Request) {
  return getCEP(req.query, req.state);
}

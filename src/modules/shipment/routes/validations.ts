import * as joi from '@hapi/joi';

export const correiosGet: joi.SchemaLike = joi.object().keys({
  query: joi.object().keys({
    cepTo: joi.string().required(),
    listingItem: joi.string().required(),
  }).required(),
});

export const cepGet: joi.SchemaLike = joi.object().keys({
  query: joi.object().keys({
    cepTo: joi.string().required(),
  }).required(),
});

import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';
import { correiosGet, cepGet } from './validations';
import * as correiosController from '../controllers/correiosController';
import * as shipmentController from '../controllers/shipmentController';

export const router = Router();

router
  .route('/shipment/correios')
  .get(
    validateReqParams(correiosGet),
    routerDefaultHandler(correiosController.correiosGet),
  );

router
  .route('/shipment/cep')
  .get(
    validateReqParams(cepGet),
    routerDefaultHandler(correiosController.cepGet),
  );

router
  .route('/shipment/curation')
  .get(
    routerDefaultHandler(shipmentController.getCuration),
  );

import { State } from '../../../models';
import * as listingService from '../../listings/services/listingService';
import * as correios from '../components/correios';
import * as cep from '../components/cep';
import { errors } from '../../../errors';
import get = require('lodash/get');
import { fullProfileService } from '../../users/services/profileService';

export const getQuote = async ({ cepFrom, cepTo, listingItem }, state: State) => {
  const item = await listingService.getSingleListing({ id: listingItem }, state);

  if (!item) {
    throw errors.notFound('Listing item note found.', {});
  }

  try {
    const seller = await fullProfileService(item.firebaseUid, state);

    const response = await correios.getQuote(
      {
        cepTo,
        cepFrom: seller.address.zipcode,
        price: item.price,
        weight: item.shipmentWeight,
        depth: item.shipmentDepth,
        height: get(item, 'shipmentHeight', undefined),
        width: get(item, 'shipmentWidth', undefined),
        diameter: get(item, 'shipmentDiameter', undefined),
      },
      state,
    );

    return response;
  } catch (err) {
    state.logger.error({ err }, 'Error getting quote from Correios');
    throw err;
  }
};

export const getCEP = async ({ cepTo }, state: State) => {
  try {
    const response = await cep.getCEP({ cep: cepTo }, state);

    return response;
  } catch (err) {
    state.logger.error({ err }, 'Error getting cep from BrasilAPI');
    throw err;
  }
};

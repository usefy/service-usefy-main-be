import { State } from '../../../models';
import { getAccessUrl, getToken } from '../components/garmin';
import { errors } from '../../../errors';
import { UserSportsApp } from '../models/User';
import { userSportsAppsRepo } from '../repositories/usersSportsApps';
import * as firebase from '../../../components/firebase';

export async function accessUrl(state: State) {
  try {
    const redirectUrl = await getAccessUrl(state);
    return { redirectUrl };
  } catch (err) {
    state.logger.error({ err }, 'Error on generate authorization URL on Garmin');
    throw errors.badGateway('Error on generate authorization URL on Garmin', { err });
  }
}

export async function deauthorizeUser(state: State) {
  const uid = state.user.uid;

  try {
    await userSportsAppsRepo.delete({ uid, app: 'garmin' }, state);

    return true;
  } catch (err) {
    state.logger.error({ err }, 'Error on deauthorize user on Garmin');
    throw errors.badGateway('Error on deauthorize user on Garmin', { err });
  }
}

async function parseGarminToken(oauthToken: string, oauthVerifier: string, state: State) {
  try {
    const result = await getToken(oauthToken, oauthVerifier, state);

    state.logger.info({ garminTokenResult: result }, 'Strava code  to token generated with success');

    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error on get user token on Garmin');
    throw errors.badGateway('Error on get user token on Garmin', { err });
  }
}

export async function getOauthToken(query, state: State) {
  const { oauth_token: oauthToken, oauth_verifier: oauthVerifier } = query;
  const result = await parseGarminToken(oauthToken, oauthVerifier, state);

  // FIXME: fix this uid and displayName for Garmin Users
  // The UID we'll assign to the user.
  const uid = `garmin:${result.results}`;
  const displayName = `Garmin User ${result.results}`;

  await firebase.auth.createOrUpdateUserInFirebase(uid, displayName, state);

  const document: UserSportsApp = {
    uid,
    app: 'garmin',
    accessToken: result.oAuthAccessToken,
    tokenSecret: result.oAuthAccessTokenSecret,
    appData: JSON.stringify(result),
  };

  await userSportsAppsRepo.updateOrCreate(uid, document, state);

  const token = firebase.auth.createToken(uid);

  state.logger.info({ uid }, 'Custom firebase token created to a user using strava.');

  return token;
}

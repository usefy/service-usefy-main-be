import { userRepo } from '../repositories/users';
import { errors } from '../../../errors';
import { User } from '../models/User';
import * as firebaseUser from '../../../components/firebase/user';
import * as tracking from '../../../components/tracking';
import { State } from '../../../models';

export async function fullProfileService(uid: string, state): Promise<User> {
  const firebaseData = await firebaseUser.get(uid);
  const dbResponse = await userRepo.getByUserUid(uid, state);
  return <User>Object.assign({}, firebaseData, <User>dbResponse || {});
}

export async function profileService(
  params: { username?: string | null; uid?: string | null },
  state: State,
) {
  state.logger.info('User Profile Service');

  if (params.username) {
    const dbResponse = await userRepo.getByUsername(params.username, state);
    const {
      firebaseUid,
      email,
      firstName,
      lastName,
      displayName,
      username,
      birthDate,
      sportPracticeTime,
      sports,
      clubs,
      createdAt,
      address,
      photoURL,
    }: Partial<User> = <User>dbResponse![0];

    const {
      providerData,
      passwordHash,
      tokensValidAfterTime,
      metadata,
      disabled,
      ...firebaseData
    } = await firebaseUser.get(firebaseUid);

    if (disabled) {
      throw errors.notFound('User not found', {});
    }

    return {
      email,
      firstName,
      lastName,
      displayName,
      birthDate,
      sportPracticeTime,
      sports,
      clubs,
      createdAt,
      username,
      address,
      photoURL,
      uid: firebaseUid,
      ...firebaseData,
    };
  }

  if (params.uid) {
    const dbResponse = await userRepo.getByUserUid(params.uid, state);
    const {
      providerData,
      passwordHash,
      tokensValidAfterTime,
      metadata,
      disabled,
      ...firebaseData
    } = await firebaseUser.get(params.uid);

    const {
      uid,
      email,
      firstName,
      lastName,
      displayName,
      username,
      birthDate,
      sportPracticeTime,
      sports,
      clubs,
      createdAt,
      address,
      photoURL,
    }: Partial<User> = <User>dbResponse || {};

    if (disabled) {
      throw errors.notFound('User not found', {});
    }

    return {
      uid,
      email,
      firstName,
      lastName,
      displayName,
      birthDate,
      sportPracticeTime,
      sports,
      clubs,
      createdAt,
      username,
      address,
      photoURL,
      ...firebaseData,
    };
  }

  throw errors.badRequest('Insufficient parameters to request profile', {});
}

export async function meService(state: State) {
  const [dbResponse, firebaseResponse] = await Promise.all([
    userRepo.getByUserUid(state.user.uid, state),
    firebaseUser.get(state.user.uid),
  ]);

  if (!dbResponse && firebaseResponse) {
    return firebaseResponse;
  }

  if (dbResponse && !firebaseResponse) {
    return dbResponse;
  }

  return Object.assign(dbResponse, firebaseResponse);
}

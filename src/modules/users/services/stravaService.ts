import { getAccessUrl, deauthorize, getToken, StravaTokenResponse } from '../components/strava';
import { State } from '../../../models';
import { errors } from '../../../errors';
import { userSportsAppsRepo } from '../repositories/usersSportsApps';
import { UserSportsApp } from '../models/User';
import * as firebase from '../../../components/firebase';

export function accessUrl(state: State) {
  return getAccessUrl(state);
}

export async function deauthorizeUser(state: State) {
  const uid = state.user.uid;
  const sportApp = await userSportsAppsRepo.getByUserUidAndApp(uid, 'strava', state);
  const accessToken = sportApp.accessToken;

  try {
    const result = await deauthorize(accessToken, state);

    await userSportsAppsRepo.delete({ uid, app: 'strava' }, state);

    state.logger.info({ result }, 'Strava user deauthorized with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error on deauthorize user on Strava');
    throw errors.badGateway('Error on deauthorize user on Strava', { err });
  }
}

async function parsingStravaToken(code: string, state: State) {
  try {
    const result: StravaTokenResponse = await getToken(code, state);
    state.logger.info({ stravaTokenResult: result }, 'Strava code to token generated with success');

    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error on parse code to token for a user on Strava');
    throw errors.badGateway('Error on parse code to token for a user on Strava', { err });
  }
}

export async function code2Token(query, state: State) {
  const { code } = query;

  const result = await parsingStravaToken(code, state);
  const uid = `strava:${result.athlete.id}`;
  const displayName = `${result.athlete.firstname} ${result.athlete.lastname}`;
  const photoURL = result.athlete.profile;

  try {
    const user = await firebase.auth.getUserInFirebase(uid, state);
    state.logger.info({ uid: user.uid }, 'User found following to signing');
  } catch (error) {
    state.logger.info('User not found, try to create.');
    await firebase.auth.createUserInFirebase(uid, displayName, photoURL, state);

    const document: UserSportsApp = {
      uid,
      app: 'strava',
      appData: JSON.stringify(result),
    };

    await userSportsAppsRepo.updateOrCreate(uid, document, state);
  }

  const token = await firebase.auth.createToken(uid);

  state.logger.info({ uid }, 'Custom firebase token created to a user using strava.');

  return { token };
}

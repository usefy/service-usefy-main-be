import * as _ from 'lodash';
import { userRepo } from '../repositories/users';
import { State } from '../../../models';

const blackListOfUsernames = [
  'about',
  'sobre',
  'contato',
  'contact',
  'login',
  'logout',
  'checkout',
  'pagamentos',
];

export async function usernameService(params, state: State) {
  const { username } = params;

  if (blackListOfUsernames.findIndex(b => b === username) > 0) {
    return {
      username,
      valid: false,
    };
  }

  const usernameCount = await userRepo.usernameCount(username, state);

  return {
    username,
    valid: usernameCount === 0,
  };
}

export async function emailService(params, state: State) {
  const { email } = params;

  const emailCount = await userRepo.emailCount(email, state);

  return {
    alreadyExists: emailCount > 0,
  };
}

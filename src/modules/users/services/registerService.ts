import * as firebase from '../../../components/firebase';
import { FirebaseUser } from '../../../components/firebase/user';
import * as tracking from '../../../components/tracking';
import { config } from '../../../config';

export async function registerService(param, state) {
  state.logger.info({ param }, 'register service');

  const { email, password, displayName, phoneNumber, username }: FirebaseUser = param;

  const firebaseUser = await firebase.user.create({
    email,
    password,
    displayName,
    phoneNumber,
    username,
  });

  await tracking.identify(
    {
      email,
      id: firebaseUser.uid,
      name: displayName,
    },
    state,
  );

  await tracking.send(
    {
      id: firebaseUser.uid,
      name: config.customerio.track.welcome,
      data: {
        firstName: displayName,
      },
    },
    state,
  );

  return {
    uid: firebaseUser.uid,
    displayName: firebaseUser.displayName,
    email: firebaseUser.email,
    phoneNumber: firebaseUser.phoneNumber,
    photoUrl: firebaseUser.photoURL,
    emailVerified: firebaseUser.emailVerified,
    disabled: firebaseUser.disabled,
  };
}

import * as GGLPhone from 'google-libphonenumber';
import { State } from '../../../models';
import * as firebase from '../../../components/firebase';
import { userRepo } from '../repositories/users';
import { usernameService } from './usernameService';
import { errors } from '../../../errors';
import { meService } from './profileService';

export async function userUpdateService(param, state: State) {
  state.logger.info({ param }, 'user update service');

  const {
    email,
    password,
    passwordConfirmation, // TODO: check password
    firstName,
    lastName,
    displayName,
    phoneNumber,
    username,
    photoURL,
    taxNumber,
    idNumber,
    idIssuer,
    idIssuerDate,
    birthDate,
    address: addressBody,
    sportPracticeTime,
    sports,
    clubs,
    fcmToken,
  } = param;

  const defaultAddress = {
    address: '',
    address2: '',
    addressNumber: '',
    district: '',
    zipcode: '',
    currentCity: '',
    currentState: '',
    country: '',
  };

  const {
    address,
    address2,
    addressNumber,
    district,
    zipcode,
    currentCity,
    currentState,
    country,
  } = addressBody || defaultAddress;

  const currentUser = await meService(state);

  if (username && currentUser.username !== username) {
    // validate username
    const usernameResponse = await usernameService({ username }, state);

    if (!usernameResponse.valid) {
      state.logger.warn({ username }, 'Invalid username submitted.');
      throw errors.badRequest('Invalid Username', { username });
    }
  }

  // parse phone
  const phoneUtil = GGLPhone.PhoneNumberUtil.getInstance();
  let phone: any = null;
  let phoneParsed: any = null;

  if (phoneNumber) {
    phoneParsed = phoneUtil.parseAndKeepRawInput(phoneNumber, 'BR'); // TODO: usar corretamente o país
    const stringPhone: string = phoneParsed.getNationalNumber();

    phone = {
      countryCode: phoneParsed.getCountryCode(),
      areaCode: stringPhone.toString().substring(0, 2),
      phone: stringPhone.toString().substring(2),
    };
  }

  // parse address

  const add = {
    address,
    address2,
    zipcode,
    district,
    currentCity,
    currentState,
    country,
    number: addressNumber,
  };

  const firebaseUser = await firebase.user.update(state.user.uid, {
    email,
    password,
    displayName,
    phoneNumber: phoneParsed ? phoneUtil.format(phoneParsed, 1) : phoneNumber,
  });

  const userDb = await userRepo.updateOrCreate(
    state.user.uid,
    {
      email,
      firstName,
      lastName,
      displayName,
      username,
      photoURL,
      taxNumber,
      idNumber,
      idIssuer,
      idIssuerDate,
      birthDate,
      sportPracticeTime,
      sports,
      clubs,
      phone,
      address: add,
      notifications: JSON.stringify({
        fcmToken, // firebase cloud message token
      }),
    },
    state,
  );

  return meService(state);
}

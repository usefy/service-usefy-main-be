import moment from 'moment';

import { userRepo } from '../repositories/users';
import { State } from '../../../models';
import { listingsRepo } from '../../listings/repositories/listings';
import { ListingItem } from '../../listings/models';
import * as tracking from '../../../components/tracking';
import { config } from '../../../config';

const processingUsersNotCreatedEquipoByCreatedAt = async (params, state: State) => {
  const users = await userRepo.getUsersByCreatedAt(
      params.startDate,
      params.endDate,
      state,
    );

  // const users = await userRepo.getAll(state);

  return Promise.all(users.map(async (user) => {
    const equipos: ListingItem[] = await listingsRepo.getByUserUid(user.firebaseUid, state);

    if (equipos.length === 0) {
      return tracking.send(
        {
          id: user.firebaseUid,
          name: params.name,
          data: {},
        },
        state,
      );
    }
  }));
};

export const processingUsersNotCreatedEquipoAfter5Days = async (state: State) => {
  try {
    return processingUsersNotCreatedEquipoByCreatedAt(
      {
        startDate: moment().subtract(5, 'days').startOf().toISOString(),
        endDate: moment().subtract(5, 'days').endOf().toISOString(),
        name: config.customerio.track.order.seller.notCreatedEquipoAfter5Days,
      },
      state,
    );
  } catch (err) {
    state.logger.info({ err }, 'Error processing users not created equipo after 5 days');
  }
};

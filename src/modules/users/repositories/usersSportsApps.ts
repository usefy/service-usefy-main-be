import * as _ from 'lodash';
import moment from 'moment';
import { usersSportsAppsTable } from '../table/usersSportsAppsTable';
import { State } from '../../../models';

export const userSportsAppsRepo = {

  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return usersSportsAppsTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  updateOrCreate: async (uid, document, state: State) => {
    const existentDocument = usersSportsAppsTable.getByUserUidAndApp(uid, document.app, state);

    if (existentDocument) {
      return userSportsAppsRepo.update(uid, document, state);
    }

    return userSportsAppsRepo.create(document, state);
  },

  update: async (uid, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return usersSportsAppsTable.update(
      uid,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  getByUserUid: async (uid: string, state: State) => usersSportsAppsTable.getByUserUid(uid, state),

  getByUserUidAndApp: async (uid: string, app:string, state: State) => usersSportsAppsTable.getByUserUidAndApp(uid, app, state),

  getOne: async (key, state: State) => usersSportsAppsTable.get(key, state),

  deleteAll: state => usersSportsAppsTable.deleteAll(['uid', 'app'], state),

  delete: (key, state: State) => usersSportsAppsTable.delete(key, state),

  validate: document => usersSportsAppsTable.validate(document),

  countAll: (state: State) => usersSportsAppsTable.countAll(state),
};

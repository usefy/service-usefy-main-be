import * as _ from 'lodash';
import moment from 'moment';
import { userTable } from '../table/usersTable';
import { State } from '../../../models';

export const userRepo = {

  create: async (uid: string, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return userTable.create(
      _.merge({}, documentCopy, {
        firebaseUid: uid,
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  updateOrCreate: async (uid, document, state: State) => {
    const documentCopy = _.cloneDeep(document);

    const existentDocument = await userTable.getByUserUid(uid, state);

    if (existentDocument) {
      return userRepo.update(uid, documentCopy, state);
    }

    return userRepo.create(uid, documentCopy, state);
  },

  update: async (uid, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return userTable.update(
      uid,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  getOne: async (key, state: State) => userTable.get(key, state),

  getAll: async (state: State) => userTable.getAll(state),

  usernameCount: async (username: string, state: State) => userTable.usernameCount(username, state),

  emailCount: async (email: string, state: State) => userTable.emailCount(email, state),

  getByUserUid: async (uid: string, state: State) => userTable.getByUserUid(uid, state),

  getByUsername: async (username: string, state: State) => userTable.getByUsername(username, state),

  getByUserIdAndEmail: async (uid: string, email: string, state: State) => userTable.getByUserUidAndEmail(uid, email, state),

  getUsersByCreatedAt: async (createdAtStart: string, createdAtEnd: string, state: State) =>
    userTable.getUsersByCreatedAt(
      createdAtStart,
      createdAtEnd,
      state,
    ),

  deleteAll: state => userTable.deleteAll(['uid', 'email'], state),

  delete: (key, state: State) => userTable.delete(key, state),

  validate: document => userTable.validate(document),

  countAll: (state: State) => userTable.countAll(state),
};

import * as joi from '@hapi/joi';

export const userPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      email: joi.string().email().required(),
      password: joi.string().required(),
      passwordConfirmation: joi.string().required().valid(joi.ref('password')),
      displayName: joi.string().required(),
      phoneNumber: joi.string(),
    })
    .required(),
});

export const userPatch: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      email: joi.string().email(),
      password: joi.string(),
      passwordConfirmation: joi.string(),
      firstName: joi.string(),
      lastName: joi.string(),
      displayName: joi.string(),
      username: joi.string().min(3).max(12),
      phoneNumber: joi.string(),
      photoURL: joi.string(),
      sportPracticeTime: joi.string().allow(''),
      taxNumber: joi.string(),
      birthDate: joi.string(),
      address: joi.object().keys({
        address: joi.string(),
        address2: joi.string(),
        addressNumber: joi.string(),
        district: joi.string(),
        zipcode: joi.string(),
        currentCity: joi.string(),
        currentState: joi.string(),
        country: joi.string(),
      }),
      fcmToken: joi.string(),
    })
    .required(),
});

export const usernameGet: joi.SchemaLike = joi.object().keys({
  params: {
    username: joi.string().min(3).max(25),
  },
});

export const emailGet: joi.SchemaLike = joi.object().keys({
  params: {
    email: joi.string().email(),
  },
});

export const stravaPost: joi.SchemaLike = joi.object().keys({
  query: joi
    .object()
    .keys({
      code: joi.string().min(8).required(),
    })
    .required(),
});

export const garminPost: joi.SchemaLike = joi.object().keys({
  query: joi
    .object()
    .keys({
      oauth_token: joi.string().min(8).required(),
      oauth_verifier: joi.string().required(),
    })
    .required(),
});

import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import * as validations from './validations';
import * as userController from '../controllers/userController';
import * as usernameController from '../controllers/usernameController';
import * as profileController from '../controllers/profileController';
import * as stravaController from '../controllers/stravaController';
import * as garminController from '../controllers/garminController';

export const router = Router();

router
  .route('/user')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(userController.userGet),
  )
  .patch(
    validateFirebaseIdToken,
    validateReqParams(validations.userPatch),
    routerDefaultHandler(userController.userPatch),
  )
  .post(
    validateReqParams(validations.userPost),
    routerDefaultHandler(userController.userPost),
  );

router
  .route('/user/strava')
  .get(
    routerDefaultHandler(stravaController.stravaGet),
  )
  .post(
    validateReqParams(validations.stravaPost),
    routerDefaultHandler(stravaController.stravaPost),
  )
  .delete(
    validateFirebaseIdToken,
    routerDefaultHandler(stravaController.stravaDel),
  );

router
  .route('/user/garmin')
  .get(
    routerDefaultHandler(garminController.garminGet),
  )
  .post(
    validateReqParams(validations.garminPost),
    routerDefaultHandler(garminController.garminPost),
  )
  .delete(
    validateFirebaseIdToken,
    routerDefaultHandler(garminController.garminDel),
  );

router
  .route('/users/:username')
  .get(
    routerDefaultHandler(profileController.profileGet),
  );

router
  .route('/users/:username/validate')
  .get(
    validateReqParams(validations.usernameGet),
    routerDefaultHandler(usernameController.usernameGet),
  );

router
  .route('/users/email/:email')
  .get(
    validateReqParams(validations.emailGet),
    routerDefaultHandler(usernameController.emailGet),
  );

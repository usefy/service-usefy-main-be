import { Sport } from '../../sports/models/Sport';
import { Club } from '../../clubs/models/Club';
import { Moment } from 'moment';

export type User = {
  uid: string;
  firebaseUid?: string;
  email: string;
  firstName: string;
  lastName: string;
  displayName?: string;
  photoURL: string;
  username: string;
  taxNumber?: string;
  idNumber?: string;
  idIssuer?: string;
  idIssuerDate?: string | Date | Moment;
  birthDate?: string | Date | Moment;

  phone?: {
    countryCode: string,
    areaCode: string,
    phone: string,
  };

  address?: {
    address: string,
    number: string,
    address2?: string,
    district: string,
    zipcode: string,
    currentCity: string,
    currentState: string,
    country: string,
  };

  moip?: {
    id: string,
    login: string,
    accessToken: string,
    channelId: string,
    link?: string,
    createdAt: string | Date | Moment,
  };

  sportPracticeTime?: string;
  sports?: Sport[];
  clubs?: Club[];
  createdAt: string | Date | Moment;
  updatedAt?: string | Date | Moment;
};

export interface UserSportsApp {
  uid: string;
  app: string;
  accessToken?: string;
  tokenSecret?: string;
  refreshToken?: string;
  createdAt?: string;
  expiresAt?: number;
  expiresIn?: number;
  appData?: string;
}

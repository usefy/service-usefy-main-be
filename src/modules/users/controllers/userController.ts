import { Request, Response } from 'express';
import { registerService } from '../services/registerService';
import { userUpdateService } from '../services/userUpdateService';
import { meService, profileService } from '../services/profileService';

export async function userPost(req: Request, res: Response) {
  return registerService(req.body, req.state);
}

export async function userGet(req: Request, res: Response) {
  return meService(req.state);
}

export async function userPatch(req: Request, res: Response) {
  return userUpdateService(req.body, req.state);
}

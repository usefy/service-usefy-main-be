import { Request, Response } from 'express';
import { profileService } from '../services/profileService';

export async function profileGet(req: Request, res: Response) {
  return profileService(req.params, req.state);
}

import { Request, Response } from 'express';
import { accessUrl, deauthorizeUser, code2Token } from '../services/stravaService';

export async function stravaGet(req: Request, res: Response) {
  return { redirectUrl: accessUrl(req.state) };
}

export async function stravaPost(req: Request, res: Response) {
  return code2Token(req.query, req.state);
}

export async function stravaDel(req: Request, res: Response) {
  return deauthorizeUser(req.state);
}

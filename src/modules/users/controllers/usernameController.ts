import { Request, Response } from 'express';
import { usernameService, emailService } from '../services/usernameService';

export async function usernameGet(req: Request, res: Response) {
  return usernameService(req.params, req.state);
}

export async function emailGet(req: Request, res: Response) {
  return emailService(req.params, req.state);
}

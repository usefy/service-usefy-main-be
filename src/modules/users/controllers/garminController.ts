import { Request, Response } from 'express';
import { accessUrl, getOauthToken, deauthorizeUser } from '../services/garminService';

export async function garminGet(req: Request, res: Response) {
  return accessUrl(req.state);
}

export async function garminPost(req: Request, res: Response) {
  return getOauthToken(req.query, req.state);
}

export async function garminDel(req: Request, res: Response) {
  return deauthorizeUser(req.state);
}

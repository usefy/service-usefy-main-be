import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';
import { UserSportsApp } from '../models/User';

const tableName = `usefy-${config.server.configEnv}-users-sports-apps`;

const validationScheme = joi.object().keys({
  uid: joi.string().required(),
  app: joi.string().required(),
  accessToken: joi.string(),
  createdAt: joi.string(),
  refreshToken: joi.string(),
  tokenSecret: joi.string(),
  expiresAt: joi.number(),
  expiresIn: joi.number(),
  appData: joi.string(),
});

export class UserSportsAppModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUserUid(uid: string, state: State): Promise<UserSportsApp[]> {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#uid = :uid',
      ExpressionAttributeNames: {
        '#uid': 'uid',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
      },
      ConsistentRead: true,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return <UserSportsApp[]>Items;
  }

  async getByUserUidAndApp(uid: string, app: string, state: State): Promise<UserSportsApp> {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#uid = :uid AND #app = :app',
      ExpressionAttributeNames: {
        '#uid': 'uid',
        '#app': 'app',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
        ':app': app,
      },
      ConsistentRead: true,
    };
    const { Items } = await this.dynamoQuery(params, state);
    const data = Items ? Items.length > 0 ? Items[0] : {} : {};
    return <UserSportsApp>data;
  }

  async update(uid: string, document: UserSportsApp, state: State) {

    // tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    for (const key in document) {
      if (document.hasOwnProperty(key) && document[key]) {
        if (key === 'app' || key === 'uid') continue;

        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(document, key);
      }
    }

    const { app } = document;
    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        uid,
        app,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return data;
  }
}

export const usersSportsAppsTable = new UserSportsAppModel(tableName, validationScheme);

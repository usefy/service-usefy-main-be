import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';

const tableName = `usefy-${config.server.configEnv}-users`;

export interface UserProps {
  firebaseUid: string;
  email: string;
  firstName: string;
  lastName: string;
  displayName: string;
  username: string;
  photoURL: string;
  taxNumber: string;
  idNumber: string;
  idIssuer: string;
  idIssuerDate: string;
  birthDate: string;
  phone: {
    countryCode: string;
    areaCode: string;
    phone: string;
  };
  address: {
    address: string;
    number: string;
    address2: string;
    zipcode: string;
    district: string;
    currentCity: string;
    currentState: string;
    country: string;
  };
  sportPracticeTime: string;
  sports: string[];
  clubs: string[];
  createdAt: Date;
  updatedAt: Date;
}

const validationScheme = joi.object().keys({
  uid: joi.string(),
  firebaseUid: joi.string().required(),
  email: joi.string().required(),
  firstName: joi.string(),
  lastName: joi.string(),
  displayName: joi.string(),
  username: joi.string().min(3).max(12),
  photoURL: joi.string(),
  taxNumber: joi.string(),
  idNumber: joi.string(),
  idIssuer: joi.string(),
  idIssuerDate: joi.string(),
  birthDate: joi.string(),
  phone: joi.object().keys({
    countryCode: joi.number(),
    areaCode: joi.string(),
    phone: joi.string(),
  }),
  address: joi.object().keys({
    address: joi.string().allow(''),
    number: joi.string().allow(''),
    address2: joi.string().allow(''),
    zipcode: joi.string().allow(''),
    district: joi.string().allow(''),
    currentCity: joi.string().allow(''),
    currentState: joi.string().allow(''),
    country: joi.string().allow(''),
  }),
  notifications: joi.object().keys({}).allow({}),
  sportPracticeTime: joi.string(),
  sports: joi.array(),
  clubs: joi.array(),
  notification: joi.string(),
  createdAt: joi.date(),
  updatedAt: joi.date(),
});

export class UserModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUserUid(uid: string, state: State): Promise<any> {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#firebaseUid = :firebaseUid',
      ExpressionAttributeNames: {
        '#firebaseUid': 'firebaseUid',
      },
      ExpressionAttributeValues: {
        ':firebaseUid': uid,
      },
      ConsistentRead: true,
    };
    const { Items } = await this.dynamoQuery(params, state);
    const data = Items ? Items.length > 0 ? Items[0] : undefined : undefined;
    return data;
  }

  async getByUserUidAndEmail(uid: string, email: string, state: State) {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#firebaseUid = :firebaseUid AND #email = :email',
      ExpressionAttributeNames: {
        '#firebaseUid': 'firebaseUid',
        '#email': 'email',
      },
      ExpressionAttributeValues: {
        ':firebaseUid': uid,
        ':email': email,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoQuery(params, state);
    return data;
  }

  async getByEmail(email: string, state: State) {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#email = :email',
      ExpressionAttributeNames: {
        '#email': 'email',
      },
      ExpressionAttributeValues: {
        ':email': email,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoQuery(params, state);
    return data;
  }

  async getByUsername(username: string, state: State) {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#username = :username',
      ExpressionAttributeNames: {
        '#username': 'username',
      },
      ExpressionAttributeValues: {
        ':username': username,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return data;
  }

  async usernameCount(username: string, state: State) {
    const params = {
      TableName: this.tableName,
      Select: 'COUNT',
      FilterExpression: 'username = :username',
      ExpressionAttributeValues: {
        ':username': username,
      },
      ConsistentRead: true,
    };
    const { Count } = await this.dynamoScan(params, state);
    return Count;
  }

  async emailCount(email: string, state: State) {
    const params = {
      TableName: this.tableName,
      Select: 'COUNT',
      FilterExpression: 'email = :email',
      ExpressionAttributeValues: {
        ':email': email,
      },
      ConsistentRead: true,
    };
    const { Count } = await this.dynamoScan(params, state);
    return Count;
  }

  async update(uid: string, document, state: State) {

    // tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    for (const key in document) {
      if (document.hasOwnProperty(key) && document[key] !== undefined) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(document, key);
      }
    }

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        firebaseUid: uid,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return data;
  }

  async getUsersByCreatedAt(
    createdAtStart: string,
    createdAtEnd: string,
    state,
  ): Promise<UserProps[]> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#createdAt >= :createdAtStart and #createdAt <= :createdAtEnd',
      ExpressionAttributeNames: {
        '#createdAt': 'createdAt',
      },
      ExpressionAttributeValues: {
        ':createdAtStart': createdAtStart,
        ':createdAtEnd': createdAtEnd,
      },
      ConsistentRead: true,
    };

    const { Items: data } = await this.dynamoScan(params, state);
    return <UserProps[]>data || <UserProps[]>[];
  }

  async getAll(state): Promise<UserProps[]> {
    const params = {
      TableName: this.tableName,
      Select: 'ALL_ATTRIBUTES',
    };

    const { Items: data } = await this.dynamoScan(params, state);

    return <UserProps[]>data || <UserProps[]>[];
  }
}

export const userTable = new UserModel(tableName, validationScheme);

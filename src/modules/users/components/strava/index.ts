import { oauth } from 'strava-v3';
import { State } from '../../../../models';

export interface StravaTokenResponse {
  token_type: string;
  expires_at: number;
  expires_in: number;
  refresh_token: string;
  access_token: string;
  athlete: {
    id: number;
    username: string;
    resource_state: number | string;
    firstname: string;
    lastname: string;
    city: string;
    state: string;
    country: string;
    sex: string;
    premium: boolean;
    summit: boolean;
    created_at: string;
    updated_at: string
    badge_type_id: number;
    profile_medium: string;
    profile: string;
    friend: any;
    follower: any;
  };
}

export const getAccessUrl = (state: State) => {
  // documentação dos escopos http://developers.strava.com/docs/authentication/
  const scope = [
    'read',
    'profile:read_all',
  ];
  // TODO: investigar como usar esse parametro corretamente
  const stravaState = 'test';
  const approvalPrompt = 'auto';

  state.logger.info({ scope, stravaState, approvalPrompt }, 'Getting user oAuth access url for Strava.');

  return oauth.getRequestAccessURL({
    state: stravaState,
    scope: scope.join(','),
    approval_prompt: approvalPrompt,
  });
};

export const getToken = (code: string, state: State): Promise<StravaTokenResponse> => {
  return new Promise((resolve, reject) => {
    oauth.getToken(code, (err, payload) => {
      if (err || (payload.errors && payload.errors.length > 0)) {
        const error = err ? err : payload.errors;
        state.logger.warn({ error }, 'Error on get token for a strava user');
        return reject(error);
      }

      resolve(payload);
    });
  });
};

export const deauthorize = (accessToken: string, state: State): Promise<any> => {
  return new Promise((resolve, reject) => {
    oauth.deauthorize({ access_token: accessToken }, (err, payload) => {
      if (err || (payload.errors && payload.errors.length > 0)) {
        const error = err ? err : payload.errors;
        state.logger.warn({ error }, 'Error on deauthorize strava user');
        return reject(error);
      }

      resolve(payload);
    });
  });
};

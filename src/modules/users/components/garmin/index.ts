import { State } from '../../../../models';
import { config } from '../../../../config';
// tslint:disable-next-line:variable-name
const OAuth = require('oauth');
let tokens;

const oauth = new OAuth.OAuth(
  'https://connectapi.garmin.com/oauth-service/oauth/request_token',
  'https://connectapi.garmin.com/oauth-service/oauth/access_token',
  config.garmin.key,
  config.garmin.secret,
  '1.0',
  null,
  'HMAC-SHA1',
);

export interface GarminTokenResponse {
  oAuthAccessToken: string;
  oAuthAccessTokenSecret: string;
  results: any;
}

export const unauthorizedRequest = async (state: State): Promise<{ oAuthToken: string, oAuthTokenSecret: string }> => {

  if (tokens.oAuthTokenSecret && tokens.oAuthToken) {
    return tokens;
  }

  return new Promise((resolve, reject) => {
    oauth.getOAuthRequestToken((error, oAuthToken, oAuthTokenSecret, results) => {
      if (error) {
        state.logger.error({ error }, 'Error on get unauthorized tokens for Garmin API');
        return reject(error);
      }

      resolve({ oAuthToken, oAuthTokenSecret });
    });
  });
};

export const getAccessUrl = async (state: State) => {
  tokens = await unauthorizedRequest(state);

  const segments = [
    'https://connect.garmin.com/oauthConfirm',
    `?oauth_token=${tokens.oAuthToken}`,
    '&oauth_callback=http://localhost:3000',
  ];

  return segments.join('');
};

export const getToken = (oauthToken: string, oauthVerifier: string, state: State): Promise<GarminTokenResponse> => {
  return new Promise((resolve, reject) => {
    oauth.getOAuthAccessToken(
      oauthToken,
      tokens.oAuthTokenSecret,
      oauthVerifier,
      (
        error,
        oAuthAccessToken,
        oAuthAccessTokenSecret,
        results,
      ) => {
        if (error) {
          state.logger.error({ error }, 'Error on get user token for Garmin API');
          return reject(error);
        }

        resolve({
          oAuthAccessToken,
          oAuthAccessTokenSecret,
          results,
        });
      });

  });
};

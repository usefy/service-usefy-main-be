import * as _ from 'lodash';
import moment from 'moment';
import { ordersTable } from '../tables/ordersTable';
import { State } from '../../../models';
import { OrderStatus } from '../models/order';

export const ordersRepo = {

  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return ordersTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  update: async (id: string, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return ordersTable.update(
      id,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  delete: async (id: string, state: State) => {
    return ordersTable.update(
      id,
      {
        deletedAt: moment().toISOString(),
        orderStatus: OrderStatus.canceled,
      },
      state,
    );
  },

  getOne: async (key, state: State) => ordersTable.get(key, state),

  getByListingId: async (listingId: string, state: State) => ordersTable.getByListingId(listingId, state),

  getById: async (id: string, state: State) => ordersTable.getById(id, state),

  getByUserUid: async (uid: string, state: State) => ordersTable.getByUserUid(uid, state),

  getByUserUidAndId: async (uid: string, id: string, state: State) => ordersTable.getByUserUidAndId(uid, id, state),

  getByStatus: async ({ orderStatus, limit, offset }: { orderStatus: OrderStatus, limit: number, offset?: string }, state: State) => {
    return ordersTable.getByStatus(orderStatus, limit, offset, state);
  },

  deleteAll: state => ordersTable.deleteAll(['uid', 'id'], state),

  validate: document => ordersTable.validate(document),

  countAll: (state: State) => ordersTable.countAll(state),

  pending: (state: State) => ordersTable.getPending(state),
};

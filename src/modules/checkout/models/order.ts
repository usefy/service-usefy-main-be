import moment from 'moment';
import { PaymentMethod, CreditCard } from '../../payments/services/paymentService';

export interface OrderBody {
  listingId: string;
  coupon: string;
  comment: string;
  shipment: {
    method: string;
    cost: number;
    zipcode: string;
    address: string;
    address2: string;
    number: string;
    district: string;
    country: string;
    state: string;
    city: string;
  };
  payment: {
    method: PaymentMethod,
    installments: number,
    creditCard: CreditCard,
  };
}

export interface OrderUpdateBody {
  summary: string;
  status: string;
  trackingNumber?: string;
}

export enum OrderStatus {
  new = 'new',
  authorization = 'authorization',
  paymentIssue = 'paymentIssue',
  awaitShipment = 'awaitShipment',
  shipped = 'shipped',
  delivered = 'delivered',
  received = 'received',
  disputed = 'disputed',
  returned = 'returned',
  returnReceived = 'returnReceived',
  canceled = 'canceled',
  revertedPayment = 'revertedPayment',

  // num: "Pedido Enviado"
  // "Pré-Autorização"
  // "Aguardando Envio (pagamento processado)"
  // "Enviado" "Entregue"
  // "Confirmado (entrega com sucess)"
  // "Disputa"
  // "Devolvido"
  // "Cancelado"
}

export class OrderHistory {
  uid: string;
  summary: string;
  status: OrderStatus;
  previousStatus?: OrderStatus;
  createdAt: Date | string;

  constructor(uid: string, summary: string, status: OrderStatus, previousStatus?: OrderStatus) {
    this.uid = uid;
    this.summary = summary;
    this.status = status;
    this.createdAt = moment().toISOString();

    if (previousStatus) this.previousStatus = previousStatus;
  }
}

import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';
import { errors } from '../../../errors';
import { OrderStatus } from '../models/order';
import { Order } from 'aws-sdk/clients/mediaconvert';

const tableName = `usefy-${config.server.configEnv}-orders`;

export interface OrderProps {
  id: string;
  uid: string; // buyer
  listingId: string; // seller will is here
  orderStatus: string;
  shipment: {
    method: string;
    cost: number;
    zipcode: string;
    address: string;
    address2: string;
    district: string;
    country: string;
    state: string;
    city: string;
  };
  history: {
    summary: string;
    status: string;
    previousStatus: string;
    uid: string;
    createdAt: string;
  };
  createdAt: string;
  coupon: string;
  trackingNumber: string;
  updatedAt: string;
  deletedAt: string;
  receipt: string;
  escrowId: string;
}

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  uid: joi.string().required(), // buyer
  listingId: joi.string().required(), // seller will is here
  orderStatus: joi.string().required(),
  shipment: joi.object().keys({
    method: joi.string().allow('').allow(null),
    cost: joi.number(),
    zipcode: joi.string().allow('').allow(null),
    address: joi.string().allow('').allow(null),
    address2: joi.string().allow('').allow(null),
    district: joi.string().allow('').allow(null),
    country: joi.string().allow('').allow(null),
    state: joi.string().allow('').allow(null),
    city: joi.string().allow('').allow(null),
  }),
  history: joi.array().items(joi.object().keys({
    summary: joi.string().required(),
    status: joi.string().required(),
    previousStatus: joi.string(),
    uid: joi.string().required(),
    createdAt: joi.string().required(),
  })).required(),
  createdAt: joi.date().iso().required(),
  coupon: joi.string(),
  trackingNumber: joi.string(),
  updatedAt: joi.date().iso(),
  deletedAt: joi.date().iso(),
  receipt: joi.string(),
  escrowId: joi.string(),
});

export class OrdersModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByListingId(listingId: string, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'listingId_id_index',
      FilterExpression: 'attribute_not_exists(deletedAt)',
      KeyConditionExpression: '#listingId = :listingId',
      ExpressionAttributeNames: {
        '#listingId': 'listingId',
      },
      ExpressionAttributeValues: {
        ':listingId': listingId,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items![0];
  }

  async getById(id: string, state: State) {
    const params = {
      TableName: this.tableName,
      FilterExpression: 'attribute_not_exists(deletedAt)',
      KeyConditionExpression: '#id = :id',
      ExpressionAttributeNames: {
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items![0];
  }

  async getByUserUidAndId(uid: string, id: string, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'uid_id_index',
      FilterExpression: 'attribute_not_exists(deletedAt)',
      KeyConditionExpression: '#uid = :uid and #id = :id',
      ExpressionAttributeNames: {
        '#uid': 'uid',
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
        ':uid': uid,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items![0];
  }

  async getByUserUid(uid: string, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'uid_id_index',
      FilterExpression: 'attribute_not_exists(deletedAt)',
      KeyConditionExpression: '#uid = :uid',
      ExpressionAttributeNames: {
        '#uid': 'uid',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items;
  }

  async getByStatus(orderStatus: OrderStatus, limit: number, offset: string, state) {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#orderStatus = :orderStatus',
      ExpressionAttributeNames: {
        '#orderStatus': 'orderStatus',
      },
      ExpressionAttributeValues: {
        ':orderStatus': orderStatus,
      },
      ConsistentRead: true,
      Limit: limit,
    };

    if (offset) {
      params['ExclusiveStartKey'] = offset;
    }

    const result = await this.dynamoScan(params, state);
    return { data: result.Items, lastKey: result.LastEvaluatedKey };
  }

  async update(id: string, document, state: State) {
    const validation = joi.validate(document, this.validationScheme);
    if (validation.error) throw errors.validation(_.get(validation, 'error.message'), validation.error);

    // tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    let documentCopy = _.cloneDeep(document);
    // these field will not be updated
    const toOmit = ['createdAt', 'uid', 'id', 'shipment', 'listingId', 'coupon'];
    documentCopy = _.omit(documentCopy, toOmit);

    for (const key in documentCopy) {
      if (documentCopy.hasOwnProperty(key) && documentCopy[key]) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(documentCopy, key);
      }
    }

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        id,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return data;
  }

  async getPending(state: State) {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#orderStatus = :authorization or #orderStatus = :delivered',
      ExpressionAttributeNames: {
        '#orderStatus': 'orderStatus',
      },
      ExpressionAttributeValues: {
        ':authorization': OrderStatus.authorization,
        ':delivered': OrderStatus.delivered,
      },
      ConsistentRead: true,
    };

    const result = await this.dynamoScan(params, state);
    return { data: result.Items };
  }

  async getOrdersByStatusUpdatedAt(
    updatedAtStart: string,
    updatedAtEnd: string,
    itemStatus: string,
    state,
  ): Promise<OrderProps[]> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#updatedAt >= :updatedAtStart and #updatedAt <= :updatedAtEnd and #itemStatus = :itemStatus',
      ExpressionAttributeNames: {
        '#updatedAt': 'updatedAt',
        '#itemStatus': 'itemStatus',
      },
      ExpressionAttributeValues: {
        ':updatedAtStart': updatedAtStart,
        ':updatedAtEnd': updatedAtEnd,
        ':itemStatus': itemStatus,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <OrderProps[]>data || <OrderProps[]>[];
  }

}

export const ordersTable = new OrdersModel(tableName, validationScheme);

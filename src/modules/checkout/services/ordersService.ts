import moment = require('moment');
import * as _ from 'lodash';
import { State } from '../../../models';
import { ordersRepo } from '../repositories/orders';
import { getSingleListing } from '../../listings/services/listingService';
import { getCoupon } from '../../coupons/services/couponService';
import { profileService, fullProfileService } from '../../users/services/profileService';
import { OrderStatus, OrderHistory } from '../models/order';
import { updateOrder } from './checkoutService';
import { trackPackage, defaultConfig } from '../../shipment/components/correios/remote/correiosSRO';
import { User } from '../../users/models/User';
import * as moip from '../../payments/components/moip';
import { userRepo } from '../../users/repositories/users';
import { customerRepo } from '../../payments/repositories/customerRepo';
import { config } from '../../../config';
import { getUserAccountService } from '../../payments/services/accountService';
import * as paymentOrderService from '../../payments/services/orderService';
import { getPaymentService, readPaymentService } from '../../payments/services/paymentService';
import get = require('lodash/get');
import { MoipPaymentStatus } from '../../payments/models/Payment';
import { orderRepo } from '../../payments/repositories/orderRepo';
import { logger } from '../../../logger';
import { ListingItem, ListingStatus } from '../../listings/models';
import { listingsRepo } from '../../listings/repositories/listings';
import * as tracking from '../../../components/tracking';
import { OrderProps } from '../../checkout/tables/ordersTable';

const orderDeliveredOrReceived = (orderHistory: OrderHistory[]): boolean => {
  const found = _.find(orderHistory, ((history) => {
    return history.status === OrderStatus.delivered || history.status === OrderStatus.received;
  }));

  return !!found;
};

export const getByListingId = (listingId, state: State) =>
  ordersRepo.getByListingId(listingId, state);

export const getOrder = async (param, state: State) => {
  const { id } = param;
  let order = await ordersRepo.getById(id, state);
  const moipOrder = await paymentOrderService.getOrderService({ orderId: order.id }, state);
  const moipPayment = await getPaymentService({ orderId: moipOrder.id }, state);

  if (order && moipPayment) {
    const listingItem = await getSingleListing({ id: order.listingId }, state);
    const [
      coupon,
      buyer,
      seller,
      payment,
    ] = await Promise.all([
      getCoupon({ id: order.coupon }, state),
      profileService({ username: null, uid: order.uid }, state),
      profileService({ username: null, uid: listingItem.firebaseUid }, state), // TODO: uid should be the standard
      readPaymentService({ paymentId: moipPayment.id }, state),
    ]);

    if (
      payment.status === MoipPaymentStatus.authorized &&
      order.orderStatus !== OrderStatus.authorization &&
      !orderDeliveredOrReceived(order.history)
    ) {
      const newHistory = new OrderHistory(state.user.uid, 'Pagamento autorizado.', OrderStatus.authorization, order.status);
      const updatedOrder = Object.assign(
        order,
        {
          orderStatus: OrderStatus.authorization,
          history: order.history.concat([newHistory]),
        },
      );

      await ordersRepo.update(id, updatedOrder, state);
      order = updateOrder;
    }

    return {
      ...order,
      listingItem,
      coupon,
      buyer,
      seller,
      payment: {
        status: payment.status,
        method: payment.fundingInstrument.method,
        fundingInstrument: payment.fundingInstrument,
        links: {
          boleto: get(payment._links, 'payBoleto', ''),
        },
      },
      id: order.id,
      trackingNumber: order.trackingNumber,
      shipment: order.shipment,
      history: order.history,
      status: order.status,
    };
  }
};

export const getOrders = async (state: State) => {
  const ordersList = await ordersRepo.getByUserUid(state.user.uid, state);

  if (ordersList && ordersList.length > 0) {
    const orders = await Promise.all(ordersList!.map(o => getOrder({ id: o.id }, state)));

    return orders;
  }

  state.logger.info('No orders found');
  return [];
};

export const processingDeliveredOrders = async (state: State) => {
  const limit = 30;
  const lastUpdate = 168; // 7 dias em horas

  try {
    const orders = await ordersRepo.getByStatus(
      {
        limit, offset: null, orderStatus: OrderStatus.delivered,
      },
      state,
    );

    if (orders && orders.data.length > 0) {
      const { data } = orders;
      await Promise.all(
        data.map(async (order) => {
          const updatedAt = moment(order.updatedAt);
          const now = moment();

          if (
            updatedAt.isValid() &&
            order.orderStatus === OrderStatus.delivered &&
            updatedAt.add(lastUpdate, 'hours').isBefore(now)
          ) {
            await updateOrder(
              {
                id: order.id,
              },
              {
                status: OrderStatus.received,
                summary: 'Equipo recebido com sucesso. Atualização automática.',
                trackingNumber: '',
              },
              state,
            );

            // release escrow
            const escrow = moip.moipCreateEscrow(order.escrowId, state);
            state.logger.info({ order, escrow }, 'Escrow released');
          }
        }),
      );
    }
  } catch (err) {
    state.logger.info({ err, limit }, 'Error processing delivered orders.');
    throw err;
  }
};

const trackDelivered = async (order, state) => {
  const { trackingNumber } = order;
  const trackerPackage = await trackPackage(trackingNumber, defaultConfig, state);
  const lastEvent: any = _.last(_.get(trackerPackage, 'event'));

  return (
    ['BDE', 'BDI', 'BDR'].indexOf(lastEvent.tipo) >= 0 &&
    parseInt(lastEvent.state, 10) === 0 &&
    parseInt(lastEvent.state, 10) === 1
  );
};

export const processingShippedOrders = async (state: State) => {
  let limit = 30;

  const orders = (offset = null) => ordersRepo.getByStatus(
    {
      limit,
      offset,
      orderStatus: OrderStatus.shipped,
    },
    state,
  ).then(({ data, lastKey }) => {
    const done = data.reduce(
      async (trackedOrders, order) => {
        if (await trackDelivered(order, state)) {
          trackedOrders.push(order);
        }
      },
      [],
    ).map(async (order) => {
      return await updateOrder(
        {
          id: order.id,
        },
        {
          status: OrderStatus.delivered,
          summary: 'Equipo entregue com sucesso. Atualização automática.',
        },
        state,
      );
    });

    limit -= done.length;

    return { lastKey, data: done };
  }).then(({ lastKey }) => {
    if (limit > 0) {
      return orders(lastKey);
    }
  });

  try {
    await orders();
  } catch (err) {
    state.logger.info({ err, limit }, 'Error processing shipped orders.');
    throw err;
  }
};

const processingOrdersCheckNotReceived = async (params, state: State) => {
  const orders = await orderRepo.getOrdersByNotReceivedByUpdatedAt(
      params.startDate,
      params.endDate,
      state,
    );

  return Promise.all(orders.map(async (order: OrderProps) => {
    const buyer: User = await userRepo.getByUserUid(order.uid, state);
    const equipo: ListingItem = await listingsRepo.getById(order.listingId, state);
    const seller: User = await userRepo.getByUserUid(equipo.firebaseUid, state);

    return tracking.send(
      {
        id: buyer.uid,
        name: params.name,
        data: {
          buyer: {
            displayName: buyer.displayName,
          },
          seller : {
            displayName: seller.displayName,
          },
          equipo: {
            title: equipo.title,
            amount: Number(equipo.price * 100),
          },
        },
      },
      state,
    );
  }));
};

const processingOrdersCheckNotDelivered = async (params, state: State) => {
  const orders = await orderRepo.getOrdersByNotDeliveredByUpdatedAt(
      params.startDate,
      params.endDate,
      state,
    );

  return Promise.all(orders.map(async (order) => {
    const buyer: User = await userRepo.getByUserUid(order.uid, state);
    const equipo: ListingItem = await listingsRepo.getById(order.listingId, state);
    const seller: User = await userRepo.getByUserUid(state.user.uid, state);

    return tracking.send(
      {
        id: buyer.uid,
        name: params.name,
        data: {
          buyer: {
            displayName: buyer.displayName,
          },
          seller : {
            displayName: seller.displayName,
          },
          equipo: {
            title: equipo.title,
            amount: Number(equipo.price * 100),
          },
        },
      },
      state,
    );
  }));
};

export const processingOrdersNotCheckReceivedAfter3Days = async (state: State) => {
  try {
    return processingOrdersCheckNotReceived(
      {
        startDate: moment().subtract(3, 'days').startOf().toISOString(),
        endDate: moment().subtract(3, 'days').endOf().toISOString(),
        name: config.customerio.track.order.buyer.notCheckReceivedAfter3Days,
      },
      state,
    );
  } catch (err) {
    state.logger.info({ err }, 'Error processing orders not received after 7 days');
  }
};

export const processingOrdersNotCheckReceivedAfter5Days = async (state: State) => {
  try {
    return processingOrdersCheckNotReceived(
      {
        startDate: moment().subtract(5, 'days').startOf().toISOString(),
        endDate: moment().subtract(5, 'days').endOf().toISOString(),
        name: config.customerio.track.order.buyer.notCheckReceivedAfter5Days,
      },
      state,
    );
  } catch (err) {
    state.logger.info({ err }, 'Error processing orders not received after 7 days');
  }
};

export const processingOrdersNotCheckReceivedAfter7Days = async (state: State) => {
  try {
    return processingOrdersCheckNotReceived(
      {
        startDate: moment().subtract(7, 'days').startOf().toISOString(),
        endDate: moment().subtract(7, 'days').endOf().toISOString(),
        name: config.customerio.track.order.buyer.notCheckReceivedAfter7Days,
      },
      state,
    );
  } catch (err) {
    state.logger.info({ err }, 'Error processing orders not received after 7 days');
  }
};

export const processingOrdersNotCheckDeliveredAfter3Days = async (state: State) => {
  try {
    return processingOrdersCheckNotDelivered(
      {
        startDate: moment().subtract(3, 'days').startOf().toISOString(),
        endDate: moment().subtract(3, 'days').endOf().toISOString(),
        name: config.customerio.track.order.seller.notCheckDeliveredAfter3Days,
      },
      state,
    );
  } catch (err) {
    state.logger.info({ err }, 'Error processing orders not delivery after 3 days');
  }
};

export const processingOrdersNotCheckDeliveredAfter5Days = async (state: State) => {
  try {
    return processingOrdersCheckNotDelivered(
      {
        startDate: moment().subtract(5, 'days').startOf().toISOString(),
        endDate: moment().subtract(5, 'days').endOf().toISOString(),
        name: config.customerio.track.order.seller.notCheckDeliveredAfter5Days,
      },
      state,
    );
  } catch (err) {
    state.logger.info({ err }, 'Error processing orders not delivery after 5 days');
  }
};

export const processingOrdersNotCheckDeliveredAfter7Days = async (state: State) => {
  try {
    return processingOrdersCheckNotDelivered(
      {
        startDate: moment().subtract(7, 'days').startOf().toISOString(),
        endDate: moment().subtract(7, 'days').endOf().toISOString(),
        name: config.customerio.track.order.seller.notCheckDeliveredAfter7Days,
      },
      state,
    );
  } catch (err) {
    state.logger.info({ err }, 'Error processing orders not delivery after 7 days');
  }
};

const getCustomer = async ({ user, shipment }, state: State): Promise<moip.MoipCustomerResponse> => {
  let moipCustomer: moip.MoipCustomerResponse | null = null;

  const customer = await customerRepo.getByUid(user.uid, state);

  if (customer) {
    moipCustomer = await moip.moipReadCustomer(customer.id, state);

    // TODO: validate if the shipment address is the same or create a new one
    state.logger.info({ moipCustomer }, 'User is already a customer');
    return moipCustomer;
  }

  // Create customer on moip
  moipCustomer = await moip.moipCreateCustomer(
    {
      ownId: user.uid,
      firstName: user.firstName,
      lastName: user.lastName,
      fullname: `${user.firstName} ${user.lastName}`,
      email: user.email,
      birthDate: moment(user.birthDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      taxDocument: {
        type: 'CPF',
        number: user.taxNumber!,
      },
      phone: {
        countryCode: user.phone!.countryCode,
        areaCode: user.phone!.areaCode,
        number: user.phone!.phone,
      },
      shippingAddress: {
        street: shipment.address,
        streetNumber: shipment.number, // TODO: fix number
        complement: shipment.address2,
        district: shipment.district,
        zipCode: shipment.zipcode,
        city: shipment.city,
        state: shipment.state,
        country: shipment.country,
      },
    },
    state,
  );

  state.logger.info({ moipCustomer }, 'Customer created for a user');
  return moipCustomer;
};

export const createOrder = async ({ listingItem, shipment, order, coupon }, state: State) => {
  // load user
  const user: User = await fullProfileService(state.user.uid, state);
  const seller: User = await fullProfileService(listingItem.firebaseUid, state);
  const sellerAccount = await getUserAccountService(seller.uid, state);
  const customer = await getCustomer({ user, shipment }, state);

  // Save order
  let newOrder = await ordersRepo.create(order, state);

  const items = [
    {
      product: listingItem.title,
      quantity: 1,
      price: listingItem.price * 100, // valor em centavos ex: 1,99 * 100 = 199
    },
  ];

  const amount: any = {
    currency: 'BRL',
    subtotals: {},
  };

  if (shipment.cost > 0) {
    amount.subtotals = {
      ...{ shipment: shipment.cost },
    };
  }

  if (coupon) {
    amount.subtotals = {
      ...{
        discount: coupon.value * 100, // valor em centavos ex: 1,99 * 100 = 199
      },
    };
  }

  let moipOrder: moip.MoipOrderResponse;

  try {
    moipOrder = await moip.moipCreateOrder(
      {
        amount,
        items,
        ownId: newOrder.id,
        customer: {
          id: customer.id,
        },
        receivers: [{
          type: 'SECONDARY',
          feePayor: false,
          moipAccount: {
            id: sellerAccount.id,
          },
          amount: {
            percentual: config.moip.sellerSplit * 100,
          },
        }],
      },
      state,
    );
  } catch (err) {
    // isso quer dizer que deu erro no MOIP
    // rollback, apagar a ordem do lado da Qip

    logger.info({ err, newOrder }, 'Rolling back and deleting the new order');
    await orderRepo.delete(newOrder.id, state);

    throw err;
  }

  // save order on success
  newOrder = await ordersRepo.update(
    newOrder.id,
    Object.assign(
      order,
      {
        id: newOrder.id,
        receipt: JSON.stringify(moipOrder),
        createdAt: newOrder.createdAt,
      },
    ),
    state,
  );

  return { moipOrder, order: newOrder };
};

export const getPendingOrders = async (state: State) => {
  const ordersPending = await ordersRepo.pending(state);

  if (ordersPending && ordersPending.data.length > 0) {
    const orders = await Promise.all(ordersPending.data!.map(o => getOrder({ id: o.id }, state)));

    return orders;
  }

  state.logger.info('No orders pending found');
  return [];
};

export const processingOrdersWaitingPayment = async (state: State) => {
  const limit = 30;

  const { data: orders } = await ordersRepo.getByStatus(
    {
      limit,
      offset: null,
      orderStatus: OrderStatus.paymentIssue,
    },
    state,
  );

  orders.forEach(async (order) => {
    const moipOrder = await paymentOrderService.getOrderService(
      { orderId: order.id },
      state,
    );
    const moipPayment = await getPaymentService(
      { orderId: moipOrder.id },
      state,
    );
    const payment = await readPaymentService(
      { paymentId: moipPayment.id },
      state,
    );

    let newHistory: OrderHistory;
    let updatedOrder;

    switch (payment.status) {
      case MoipPaymentStatus.authorized:
        newHistory = new OrderHistory(
          state.user.uid,
          'Pagamento autorizado.',
          OrderStatus.authorization,
          order.status,
        );

        updatedOrder = Object.assign(order, {
          orderStatus: OrderStatus.authorization,
          history: order.history.concat([newHistory]),
        });

        await ordersRepo.update(order.id, updatedOrder, state);
        break;

      case MoipPaymentStatus.rejected:
      case MoipPaymentStatus.canceled:
        newHistory = new OrderHistory(
          state.user.uid,
          'Pagamento cancelado.',
          OrderStatus.canceled,
          order.status,
        );

        updatedOrder = Object.assign(order, {
          orderStatus: OrderStatus.canceled,
          history: order.history.concat([newHistory]),
        });

        const [listingItem] = await Promise.all([
          getSingleListing({ id: order.listingId }, state),
          ordersRepo.update(order.id, updatedOrder, state),
        ]);

        await listingsRepo.update(
          listingItem.firebaseUid,
          listingItem.id,
          {
            listingItem,
            itemStatus: ListingStatus.enabled,
          },
          state,
        );

        const buyer: User = await userRepo.getByUserUid(order.uid, state);
        const equipo: ListingItem = await listingsRepo.getById(order.listingId, state);
        const seller: User = await userRepo.getByUserUid(state.user.uid, state);

        await tracking.send(
          {
            id: buyer.uid,
            name: config.customerio.track.payment.canceled,
            data: {
              buyer: {
                displayName: buyer.displayName,
              },
              seller : {
                displayName: seller.displayName,
              },
              equipo: {
                title: equipo.title,
                amount: Number(equipo.price * 100),
              },
            },
          },
          state,
        );

        break;

      default:
        break;
    }

  });
};

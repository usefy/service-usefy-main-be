import * as _ from 'lodash';
import moment from 'moment';
import v4 from 'uuid/v4';
import { State } from '../../../models';
import { errors } from '../../../errors';
import { OrderBody, OrderStatus, OrderHistory, OrderUpdateBody } from '../models/order';
import { User } from '../../users/models/User';
import { getSingleListing, updateListing, updateStatusSoldListing } from '../../listings/services/listingService';
import { ordersRepo } from '../repositories/orders';
import { userRepo } from '../../users/repositories/users';
import { getCoupon } from '../../coupons/services/couponService';
import * as orderService from './ordersService';
import { Dictionary } from 'express-serve-static-core';
import { createPaymentService } from '../../payments/services/paymentService';
import { ListingStatus } from '../../listings/models';
import * as tracking from '../../../components/tracking';
import { getSinglePublicListing } from '../../listings/services/publicListingService';
import { config } from '../../../config';
import { getUserAccountService } from '../../payments/services/accountService';

const findItemOrder = async ({ listingId }: { listingId: string }, state: State) => {
  try {
    state.logger.info('Find a order for a listing');
    return ordersRepo.getByListingId(listingId, state);
  } catch (error) {
    state.logger.info({ listingId }, 'Error on get listing orders');
    throw error;
  }
};

const validateListing = async ({ listingId }: { listingId: string }, state: State) => {
  try {
    // listing Id exists
    const listingItem = await getSinglePublicListing({ id: listingId }, state);

    // listing have no order
    const listingOrder = await findItemOrder({ listingId }, state);

    // listing order found
    if (listingOrder) {
      state.logger.info({ listingId }, 'Listing already have an order');
      throw errors.badRequest('Listing already have an order', { listingId });
    }

    return listingItem;
  } catch (err) {
    if (err.name === 'NotFoundError') {
      state.logger.info({ listingId, err }, 'Listing not found when creating order');
      throw errors.badRequest('Listing item not found', { listingId });
    }

    if (err.name === 'BadRequestError') {
      throw err;
    }

    state.logger.info({ listingId, err }, 'Unknown error validating listing when create a new order');
    throw err;
  }
};

const validateListingWithOrder = async ({ listingId }: { listingId: string }, state: State) => {
  try {
    // listing Id exists
    const listingItem = await getSingleListing({ id: listingId }, state);

    return listingItem;
  } catch (err) {
    if (err.name === 'NotFoundError') {
      state.logger.info({ listingId, err }, 'Listing not found when updating order');
      throw errors.badRequest('Listing item not found', { listingId });
    }

    if (err.name === 'BadRequestError') {
      throw err;
    }

    state.logger.info({ listingId, err }, 'Unknown error validating listing when updating order');
    throw err;
  }
};

const validateCoupon = async ({ coupon }: { coupon: string }, state: State) => {
  // validate coupon (exists, valid date)
  try {
    if (!coupon) {
      return null;
    }

    const foundCoupon = await getCoupon({ id: coupon }, state);

    if (moment().isAfter(foundCoupon.validUntil)) {
      state.logger.info({ coupon }, 'Coupon is expired when create a new order');
      throw errors.badRequest('Coupon is expired', { coupon });
    }

    return foundCoupon;
  } catch (err) {
    if (err.name === 'NotFoundError') {
      state.logger.info({ coupon, err }, 'Coupon not found when creating order');
      throw errors.badRequest('Coupon not found', { coupon });
    }

    if (err.name === 'BadRequestError') {
      throw err;
    }

    state.logger.info({ coupon, err }, 'Unknown error validating coupon when create a new order');
    throw err;
  }
};

export const createOrder = async (body: OrderBody, state: State) => {
  const {
    listingId,
    coupon,
    shipment,
    payment,
  } = body;
  const buyer = state.user;

  // validate listing
  const listingItem = await validateListing({ listingId }, state);

  // validate owner
  if (buyer.uid === listingItem.firebaseUid) {
    state.logger.info({ buyer, listingItem }, 'Owner cannot buy his own listing');
    throw errors.forbidden('Owner cannot buy his own listing', {});
  }

  // validate coupon
  const foundCoupon = await validateCoupon({ coupon }, state);

  await getUserAccountService(state.user.uid, state);

  // TODO: validate shipment cost

  // Create Order
  const order = {
    listingId,
    shipment,
    coupon,
    id: v4(),
    uid: state.user.uid,
    orderStatus: OrderStatus.new,
    history: [
      new OrderHistory(state.user.uid, 'Order Created', OrderStatus.new),
    ],
  };

  try {
    const { moipOrder, order: newOrder } = await orderService.createOrder({ listingItem, shipment, order, coupon: foundCoupon }, state);
    const moipPayment = await createPaymentService({ moipOrderId: moipOrder.id, order: newOrder }, payment, state);
    const seller: User = await userRepo.getByUserUid(listingItem.firebaseUid, state);

    await updateStatusSoldListing(listingId , state);

    const createdOrder = await orderService.getOrder({ id: newOrder.id }, state);

    state.logger.info({ newOrder, moipOrder, moipPayment }, 'New order was created');

    await tracking.send(
      {
        id: seller.firebaseUid,
        name: config.customerio.track.checkout.seller,
        data: {
          equipo: {
            title: listingItem.title,
            amount: listingItem.amount,
          },
          seller: {
            displayName: seller.displayName,
          },
          buyer: {
            displayName: buyer.displayName,
          },
        },
      },
      state,
    );

    await tracking.send(
      {
        id: buyer.uid,
        name: config.customerio.track.checkout.buyer,
        data: {
          order: {
            friendlyId: new Date(newOrder.createdAt).getTime(),
          },
          equipo: {
            title: listingItem.title,
            amount: listingItem.amount,
          },
          seller: {
            displayName: seller.displayName,
          },
          buyer: {
            displayName: buyer.displayName,
          },
        },
      },
      state,
    );

    return createdOrder;
  } catch (err) {
    state.logger.warn({ err, order }, 'Error on creating a new order.');

    // rollback listing status on error
    await updateListing({ id: listingId }, { itemStatus: listingItem.itemStatus }, state);

    throw err;
  }
};

const trackingSendByStatus = {
  received: async (id, data, state) => await tracking.send(
    {
      id,
      data,
      name: config.customerio.track.equipo.received,
    },
    state,
  ),
  delivered: async (id, data, state) => await tracking.send(
    {
      id,
      data,
      name: config.customerio.track.equipo.delivered,
    },
    state,
  ),
};

export const updateOrder = async (param: { id: string } | Dictionary<string>, body: OrderUpdateBody, state: State) => {
  const { id } = param;

  if (!Object.values(OrderStatus).includes(<OrderStatus>body.status)) {
    state.logger.info({ body }, 'Unknown status when update order');
    throw errors.badRequest('Status is invalid', { status: body.status });
  }

  const order = await ordersRepo.getById(id, state);

  if (order) {
    if (order.itemStatus === OrderStatus.received && body.status === OrderStatus.delivered) {
      return { order };
    }

    const listingItem = await validateListingWithOrder({ listingId: order.listingId }, state);
    const seller: User = await userRepo.getByUserUid(listingItem.firebaseUid, state);
    const buyer = state.user;

    const newHistory = new OrderHistory(
      (state.user && state.user.uid !== undefined) ? state.user.uid : order.uid,
      body.summary,
      OrderStatus[body.status],
      order.status,
    );

    const document = {
      ...order,
      orderStatus: body.status,
      history: order.history.concat([newHistory]),
    };

    if (body.trackingNumber) document['trackingNumber'] = body.trackingNumber;

    await ordersRepo.update(id, document, state);

    if (trackingSendByStatus[body.status]) {
      await trackingSendByStatus[body.status](
        state.user.uid,
        {
          equipo: {
            title: listingItem.title,
            amount: listingItem.amount,
          },
          seller: {
            displayName: seller.displayName,
          },
          buyer: {
            displayName: buyer.displayName,
          },
        },
        state,
      );
    }

    return { order };
  }
};

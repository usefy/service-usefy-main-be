import { userRepo as userRepository } from '../../users/repositories/users';
import { listingsRepo as equipoRepository } from '../../listings/repositories/listings';
import { accountRepo as accountMoipRepository } from '../../payments/repositories/accountRepo';
import { customerRepo as customerRepository } from '../../payments/repositories/customerRepo';

import { getApp } from '../../../components/firebase/init';
import * as firebase from '../../../components/firebase/user/index';
import * as Moip from '../../payments/components/moip';

import {
  createUserFaker,
  createEquipoFaker,
  createShipmentFaker,
  createOrderFaker,
  createAccountFaker,
  createCustomerFaker,
  createStateFaker,
  createMoipFaker,
} from '../../../faker/index';
import { createTables } from '../../../components/dyno/mock/createLocalTables';
import { createCouponFaker } from '../../../faker/coupon';
import { createOrder } from './ordersService';

jest.setTimeout(30000);

describe('Order service', () => {
  beforeAll(async () => {
    await createTables();
    await getApp();
  });

  describe('Create order', () => {
    const userFaker = createUserFaker();
    const userMoipFaker = createMoipFaker(userFaker);
    const sellerFaker = createUserFaker();
    const sellerMoipFaker = createMoipFaker(sellerFaker);
    const equipoFaker = createEquipoFaker({
      firebaseUid: sellerFaker.uid,
    });
    const shipmentFaker = createShipmentFaker();
    const orderFaker = createOrderFaker();
    const couponFaker = createCouponFaker();
    const state = createStateFaker({
      user: userFaker,
    });

    it('should create an order', async () => {
      const user = await userRepository.create(userFaker.uid, userFaker, state);
      await firebase.create(userFaker);
      await firebase.create(sellerFaker);
      const seller = await userRepository.create(sellerFaker.uid, sellerFaker, state);
      const equipo = await equipoRepository.create(equipoFaker, state);

      const userMoip = await Moip.moipCreateAccount(userMoipFaker, state);
      const sellerMoip = await Moip.moipCreateAccount(sellerMoipFaker, state);

      const accountUserMoip = await accountMoipRepository.create(
        {
          uid: user.uid,
          id: userMoip.id,
          accessToken: userMoip.accessToken,
          channelId: userMoip.channelId,
          linksSelf: userMoip._links.self.href,
          login: userMoip.login,
        },
        state,
      );

      await accountMoipRepository.create(
        {
          uid: seller.uid,
          id: sellerMoip.id,
          accessToken: sellerMoip.accessToken,
          channelId: sellerMoip.channelId,
          linksSelf: sellerMoip._links.self.href,
          login: sellerMoip.login,
        },
        state,
      );

      const customerFaker = createCustomerFaker({
        moipAccountId: accountUserMoip.id,
      });

      await customerRepository.create(customerFaker, state);

      expect(() => {
        createOrder(
          {
            listingItem: equipo,
            shipment: shipmentFaker,
            order: orderFaker,
            coupon: couponFaker,
          },
          state,
        );
      }).not.toThrow();
    });
  });
});

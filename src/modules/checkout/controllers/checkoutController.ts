import { Request } from 'express';
import { createOrder, updateOrder } from '../services/checkoutService';
import { getOrder } from '../services/ordersService';

export async function postCheckout(req: Request) {
  return createOrder(req.body, req.state);
}

export async function getCheckout(req: Request) {
  return getOrder(req.params, req.state);
}

export async function putCheckout(req: Request) {
  return updateOrder(req.params, req.body, req.state);
}

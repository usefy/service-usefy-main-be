import supertest from 'supertest';
import { app } from '../../../app';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';

jest.mock('../../../helpers/authHandler');

const mockedValidate = validateFirebaseIdToken as jest.MockedFunction<typeof validateFirebaseIdToken>;

const body = {
  listingId: '1234',
  shipment: {
    method: 'Correios',
    cost: 10.00,
    zipcode: '012345-001',
    address: 'Rua da aurora, 0',
    address2: 'ap 85',
    district: 'Paraiso',
    country: 'Brasil',
    city: 'São Paulo',
    state: 'SP',
  },
  payment: {
    method: 'CREDIT_CARD',
    installments: 1,
    creditCard: {
      hash: '12345',
      fullname: 'Dustin',
      birthdate: '1997-01-20',
      taxDocument: {
        type: 'CPF',
        number: '123456789-65',
      },
    },
  },
};

describe('Orders Module', () => {
  describe('post checkout endpoint validations', () => {
    beforeAll(() => {
      mockedValidate.mockImplementation(async (req, res, next) => { next(); });
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    it('should not create order without a listing id', async () => {
      const incompleteBody = body;
      delete incompleteBody.listingId;
      await supertest(app)
        .post('/v1/checkout/order')
        .send(incompleteBody)
        .expect(400);
    });

    it('should not create order without a payment method', async () => {
      const incompleteBody = body;
      delete incompleteBody.payment.method;
      await supertest(app)
        .post('/v1/checkout/order')
        .send(incompleteBody)
        .expect(400);
    });

    it('should not create order with an invalid payment method', async () => {
      const wrongBody = body;
      wrongBody.payment.method = 'FIADO';
      await supertest(app)
        .post('/v1/checkout/order')
        .send(wrongBody)
        .expect(400);
    });

    it('should not create order without payment details', async () => {
      const incompleteBody = body;
      delete incompleteBody.payment;
      await supertest(app)
        .post('/v1/checkout/order')
        .send(incompleteBody)
        .expect(400);
    });

  });
});

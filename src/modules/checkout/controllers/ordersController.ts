import { Request } from 'express';
import * as ordersService from '../services/ordersService';

export async function getOrders(req: Request) {
  return ordersService.getOrders(req.state);
}

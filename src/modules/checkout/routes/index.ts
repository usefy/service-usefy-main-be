import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';

import { checkoutPost, checkoutGet, checkoutPut } from './validations';
import * as checkoutController from '../controllers/checkoutController';
import * as ordersController from '../controllers/ordersController';

export const router = Router();

router
  .route('/checkout/order')
  .post(
    validateFirebaseIdToken,
    validateReqParams(checkoutPost),
    routerDefaultHandler(checkoutController.postCheckout),
  );

router
  .route('/order/:id')
  .get(
    validateFirebaseIdToken,
    validateReqParams(checkoutGet),
    routerDefaultHandler(checkoutController.getCheckout),
  )
  .put(
    validateFirebaseIdToken,
    validateReqParams(checkoutPut),
    routerDefaultHandler(checkoutController.putCheckout),
  );

router
  .route('/orders')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(ordersController.getOrders),
  );

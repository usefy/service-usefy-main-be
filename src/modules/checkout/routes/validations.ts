import * as joi from '@hapi/joi';

export const checkoutPost: joi.SchemaLike = joi.object().keys({
  body: joi.object().keys({
    listingId: joi.string().required(),
    coupon: joi.string(),
    comment: joi.string(),
    shipment: joi.object().keys({
      method: joi.string().allow('').allow(null),
      cost: joi.number(),
      zipcode: joi.string().allow('').allow(null),
      address: joi.string().allow('').allow(null),
      address2: joi.string().allow('').allow(null),
      district: joi.string().allow('').allow(null),
      country: joi.string().allow('').allow(null),
      state: joi.string().allow('').allow(null),
      city: joi.string().allow('').allow(null),
    }),
    payment: joi.object().keys({
      method: joi.string().valid('CREDIT_CARD', 'BOLETO').required(),
      installments: joi.number().min(1).max(12).required(),
      creditCard: joi.object().keys({
        hash: joi.string().required(),
        fullname: joi.string().required(),
        birthdate: joi.string().required(),
        taxDocument: joi.object().keys({
          type: joi.string().valid('CPF', 'CNPJ').required(),
          number: joi.string().required(),
        }).required(),
        billingAddress: joi.object().keys({
          street: joi.string().required(),
          streetNumber: joi.string().required(),
          district: joi.string().required(),
          city: joi.string().required(),
          state: joi.string().required(),
          zipCode: joi.string().required(),
        }).required(),
      }),
    }).required(),
  }).required(),
});

export const checkoutGet: joi.SchemaLike = joi.object().keys({
  params: joi.object().keys({
    id: joi.string().required(),
  }).required(),
});

export const checkoutPut: joi.SchemaLike = joi.object().keys({
  params: joi.object().keys({
    id: joi.string().required(),
  }).required(),
  body: joi.object().keys({
    summary: joi.string().required(),
    status: joi.string().required(),
    trackingNumber: joi.string(),
  }).required(),
});

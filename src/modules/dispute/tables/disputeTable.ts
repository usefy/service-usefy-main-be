import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';

const tableName = `usefy-${config.server.configEnv}-disputes`;

// NOTE: firebaseUid1 will be aways the listing publisher

const validationScheme = joi.object().keys({
  uid: joi.string().required(),
  orderId: joi.string().required(),
  subject: joi.string().required(),
  body: joi.string().required(),
  files: joi.array().items(
    joi.string().uri(),
  ),
  helpDeskData: joi.string().required(),
  ticketUrl: joi.string().required(),
  createdAt: joi.date(),
});

export class DisputeModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUidAndOrderId(uid: string, orderId: string, state: State) {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#uid = :uid and #orderId = :orderId',
      ExpressionAttributeNames: {
        '#uid': 'uid',
        '#orderId': 'orderId',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
        ':orderId': orderId,
      },
      ConsistentRead: false,
    };
    const { Items: data } = await this.dynamoQuery(params, state);
    return data![0];
  }
}

export const disputesTable = new DisputeModel(tableName, validationScheme);

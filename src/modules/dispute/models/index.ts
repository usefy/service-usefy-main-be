import moment from 'moment';

export interface DisputeDocument {
  uid: string;
  orderId: string;
  subject: string;
  body: string;
  files?: string[];
  helpDeskData: string;
  ticketUrl: string;
  createdAt?: string | Date | moment.Moment;
}

import * as _ from 'lodash';
import moment from 'moment';
import { State } from '../../../models';
import { disputesTable } from '../tables/disputeTable';
import { DisputeDocument } from '../models';

export const disputeRepo = {

  create: async (document: DisputeDocument, state: State): Promise<DisputeDocument> => {
    const documentCopy = _.cloneDeep(document);
    return disputesTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  getByUidAndOrderId: (uid: string, orderId: string, state: State) => disputesTable.getByUidAndOrderId(uid, orderId, state),

  validate: document => disputesTable.validate(document),

  countAll: (state: State) => disputesTable.countAll(state),
};

import { State } from '../../../models';
import { DisputeDocument } from '../models';
import { disputeRepo } from '../repositories/disputeRepo';
import { profileService } from '../../users/services/profileService';
import { getOrder } from '../../checkout/services/ordersService';
import { OrderStatus } from '../../checkout/models/order';
import { errors } from '../../../errors';
import { updateOrder } from '../../checkout/services/checkoutService';

export const loadDisputeService = async (params: { uid: string, orderId: string }, state: State) => {
  const { uid, orderId } = params;

  try {
    const dispute = await disputeRepo.getByUidAndOrderId(uid, orderId, state);

    const [order, user] = await Promise.all([
      getOrder({ id: orderId }, state),
      profileService({ uid, username: null }, state),
    ]);

    return {
      order,
      user,
      uid,
      orderId,
      subject: dispute.subject,
      body: dispute.body,
      helpDeskData: dispute.helpDeskData,
      ticketUrl: dispute.ticketUrl,
    };

  } catch (error) {

  }
};

export const createTicket = async (params: { subject: string, body: string, files?: string[] }, state: State) => {
  state.logger.info({ params }, 'Create ticket at help desk platform');

  const { subject, body, files } = params;

  // TODO: integrate with help desk

  return {
    data: { subject, body, files },
    url: 'https://example.com',
  };
};

export const validateOrder = async (orderId: string, state: State) => {
  try {
    const order = await getOrder({ id: orderId }, state);

    if (order.status !== OrderStatus.delivered) {
      state.logger.info({ order }, 'Unable to create a dispute, invalid status');
      throw errors.badRequest('Order should be delivered to open a dispute', { status });
    }
  } catch (err) {
    state.logger.info({ err }, 'Error on validating order');
    throw errors.serverError('Error on validating order', {});
  }
};

export const createDisputeService = async (body, state: State) => {
  const {
    orderId,
    subject,
    body: disputeBody,
    files,
  } = body;

  // validate order
  await validateOrder(orderId, state);

  // create a ticket
  const { data: helpDeskData, url: ticketUrl } = await createTicket(
    { subject, files, body: disputeBody },
    state,
  );

  try {
    // update order
    await updateOrder(
      { id: orderId },
      {
        status: OrderStatus.disputed,
        summary: 'Disputa em andamento',
      },
      state,
    );
  } catch (err) {
    state.logger.warn({ err, orderId }, 'Error on update the order status to disputed');
    // continue
  }

  // save database
  const document: DisputeDocument = {
    orderId,
    subject,
    ticketUrl,
    body: disputeBody,
    helpDeskData: JSON.stringify(helpDeskData),
    uid: state.user.uid,
  };

  try {
    const dispute = await disputeRepo.create(document, state);

    state.logger.info({ dispute }, 'New dispute created');

    return loadDisputeService({ orderId, uid: state.user.uid }, state);
  } catch (err) {
    state.logger.warn({ err, document }, 'Error saving dispute');
    throw err;
  }
};

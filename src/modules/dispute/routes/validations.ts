import * as joi from '@hapi/joi';

export const disputePost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      orderId: joi.string().required(),
      subject: joi.string().required(),
      body: joi.string().required(),
      files: joi.array().items(
        joi.string().uri(),
      ),
    })
    .required(),
});

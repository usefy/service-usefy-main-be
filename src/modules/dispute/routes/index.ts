import { Router } from 'express';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';
import { disputePost } from './validations';

export const router = Router();

router
  .route('/dispute')
  .post(
    validateFirebaseIdToken,
    validateReqParams(disputePost),

  );

import { Request } from 'express';
import { createDisputeService } from '../services/disputeService';

export const disputePost = async (req: Request) => createDisputeService(req.body, req.state);

import { Request, Response } from 'express';
import {
  getUserListings,
  newListing,
  getUserSingleListing,
  updateListing,
  getEnabledListings,
  getListingsCuration,
} from '../services/listingService';
import { searchListings } from '../services/searchService';
import { getSinglePublicListing } from '../services/publicListingService';

export async function getListing(req: Request) {
  return getUserListings(req.state, req.query);
}

export async function getListings(req: Request) {
  return getEnabledListings(req.state, req.query);
}

export async function getSingleListing(req: Request) {
  return getUserSingleListing(req.params, req.state);
}

export async function postListing(req: Request) {
  return newListing(req.body, req.state);
}

export async function patchListing(req: Request) {
  return updateListing(<{ id: string }>req.params, req.body, req.state);
}

export async function getSearch(req: Request) {
  return searchListings(req.query, req.state);
}

export async function getPublicSingleListing(req: Request) {
  return getSinglePublicListing(req.params, req.state);
}

export async function getCuration(req: Request) {
  return getListingsCuration(req.state);
}

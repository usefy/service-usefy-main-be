import { Request } from 'express';
import { getUsersFavorites, addToFavorites, removeFromFavorites } from '../services/favoritesService';

export async function getFavorites(req: Request) {
  return getUsersFavorites(req.state);
}

export async function postFavorites(req: Request) {
  return addToFavorites(req.param, req.state);
}

export async function deleteFavorites(req: Request) {
  return removeFromFavorites(req.param, req.state);
}

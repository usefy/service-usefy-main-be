import { NextFunction, Request, Response, Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import * as listingController from '../controllers/listingController';
import * as favoritesController from '../controllers/favoritesController';
import { validateReqParams } from '../../../helpers/validateReqParams';
import { listingPost, listingSingle, listingPatch, searchGet, listingPublicGet, favoritesPostDelete } from './validations';

export const router = Router();

router
  .route('/favorites/')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(favoritesController.getFavorites),
  );

router
  .route('/listings/')
  .get(
    routerDefaultHandler(listingController.getListings),
  );

router
  .route('/favorites/:id')
  .post(
    validateFirebaseIdToken,
    validateReqParams(favoritesPostDelete),
    routerDefaultHandler(favoritesController.postFavorites),
  )
  .delete(
    validateFirebaseIdToken,
    validateReqParams(favoritesPostDelete),
    routerDefaultHandler(favoritesController.deleteFavorites),
  );

router
  .route('/listing/:id/:slug')
  .get(
    validateReqParams(listingPublicGet),
    routerDefaultHandler(listingController.getPublicSingleListing),
  );

router
  .route('/listing/:id')
  .get(
    validateFirebaseIdToken,
    validateReqParams(listingSingle),
    routerDefaultHandler(listingController.getSingleListing),
  )
  .patch(
    validateFirebaseIdToken,
    validateReqParams(listingPatch),
    routerDefaultHandler(listingController.patchListing),
  );

router
  .route('/listing')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(listingController.getListing),
  )
  .post(
    validateFirebaseIdToken,
    validateReqParams(listingPost),
    routerDefaultHandler(listingController.postListing),
  );

router
  .route('/search')
  .get(
    validateReqParams(searchGet),
    routerDefaultHandler(listingController.getSearch),
  );

router
  .route('/curation')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(listingController.getCuration),
  );

import * as joi from '@hapi/joi';

export const listingSingle: joi.SchemaLike = joi.object().keys({
  params: joi.object().keys({
    id: joi.string().required(),
  }).required(),
});

export const listingPost: joi.SchemaLike = joi.object().keys({
  body: joi.object().keys({
    title: joi.string().min(10).max(1024).required(),
    story: joi.string().max(5120).required(),
    price: joi.number().min(1).positive().precision(2).required(),
    itemCondition: joi.string().required(),
    size: joi.string(),
    manufacturer: joi.string().required(),
    yearOfManufacture: joi.string().regex(/^[0-9]{4}$/),
    model: joi.string(),
    material: joi.string(),
    capacity: joi.string(),
    hasInvoice: joi.boolean().required(),
    hasWarranty: joi.boolean().required(),
    sports: joi.array().items(
      joi.string().guid().required(),
    ).required(),
    photos: joi.array().items(
      joi.string().uri(),
    ),
    cover: joi.string().uri(),
    shipmentWeight: joi.number(),
    shipmentDepth: joi.number(),
    shipmentHeight: joi.number(),
    shipmentWidth: joi.number(),
    shipmentDiameter: joi.number(),
  }).required(),
});

export const listingPatch: joi.SchemaLike = joi.object().keys({
  params: joi.object().keys({
    id: joi.string().required(),
  }).required(),
  body: joi.object().keys({
    title: joi.string().min(10).max(1024),
    price: joi.number().min(1).positive().precision(2),
    itemCondition: joi.string(),
    size: joi.string(),
    manufacturer: joi.string(),
    yearOfManufacture: joi.string().regex(/^[0-9]{4}$/),
    model: joi.string(),
    material: joi.string(),
    capacity: joi.string(),
    hasInvoice: joi.boolean(),
    hasWarranty: joi.boolean(),
    sports: joi.array().items(
      joi.string().guid().required(),
    ),
    photos: joi.array().items(
      joi.string().uri(),
    ),
    cover: joi.string().uri(),
    shipmentWeight: joi.number(),
    shipmentDepth: joi.number(),
    shipmentHeight: joi.number(),
    shipmentWidth: joi.number(),
    shipmentDiameter: joi.number(),
    itemStatus: joi.string(),
  }).required(),
});

export const searchGet: joi.SchemaLike = joi.object().keys({
  query: joi.object().keys({
    keyword: joi.string(),
    offset: joi.string(),
    limit: joi.number(),
    sort: joi.string(),
    sports: joi.alternatives().try(joi.array().items(joi.string()), joi.string()),
    priceFrom: joi.number().min(1).positive(),
    priceTo: joi.number().min(1).positive(),
    hasInvoice: joi.boolean(),
    itemCondition: joi.string(),
    // location: joi.string(),
  }).required(),
});

export const listingPublicGet: joi.SchemaLike = joi.object().keys({
  params: joi.object().keys({
    id: joi.string().required(),
    slug: joi.string().required(),
  }).required(),
});

export const favoritesPostDelete: joi.SchemaLike = joi.object().keys({
  params: joi.object().keys({
    id: joi.string().required(),
  }).required(),
});

import * as _ from 'lodash';
import { State } from '../../../models';
import { listingsRepo } from '../repositories/listings';
import { errors } from '../../../errors';
import { ListingStatus, ListingItem } from '../models';
import { sportsRepo } from '../../sports/repositories/sportsRepo';
import { profileService } from '../../users/services/profileService';
import { Sport } from '../../sports/models/Sport';

export async function getSinglePublicListing(params, state: State) {
  state.logger.info('User listings service');
  const { id } = params;

  try {
    const item = await listingsRepo.getEnabledById(id, state);

    if (!item) {
      state.logger.warn({ item, id }, 'Item not found on updating listing');
      throw errors.notFound('Listing item not found', { id });
    }

    const sports: Promise<Sport>[] = item.sports.map(
      async (sportItem: string): Promise<Sport> => {
        const sport = await sportsRepo.getOneById(sportItem, state);

        if (sport) {
          return <Sport>sport;
        }

        state.logger.warn('sport not found');
        return <Sport>{};
      },
    );

    item.sports = await Promise.all(sports);
    item.user = await profileService(
      { username: undefined, uid: item.firebaseUid },
      state,
    );

    return item;
  } catch (err) {
    state.logger.error({ err }, 'Error getting a listing');
    throw err;
  }
}

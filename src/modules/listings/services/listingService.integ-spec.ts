import { userRepo as userRepository } from '../../users/repositories/users';
import { listingsRepo as equipoRepository } from '../../listings/repositories/listings';

import { createTables } from '../../../components/dyno/mock/createLocalTables';
import { getApp } from '../../../components/firebase/init';
import { updateStatusSoldListing } from './listingService';
import { ListingStatus } from '../models';
import {
  createUserFaker,
  createEquipoFaker,
  createStateFaker,
} from '../../../faker/index';

describe('Listing Service', () => {
  beforeAll(async () => {
    await createTables();
    await getApp();
  });

  describe('Update equipo to sold', () => {
    const userFaker = createUserFaker();
    const sellerFaker = createUserFaker();
    const equipoFaker = createEquipoFaker({
      firebaseUid: sellerFaker.uid,
    });
    const state = createStateFaker({
      user: userFaker,
    });

    it('should be return status sold', async () => {
      await userRepository.create(userFaker.uid, userFaker, state);
      await userRepository.create(sellerFaker.uid, sellerFaker, state);
      const equipo = await equipoRepository.create(equipoFaker, state);

      const { itemStatus } = await updateStatusSoldListing(equipo.id, state);

      expect(itemStatus).toBe(ListingStatus.sold);
    });

    it('should throw an error when invalid id', async () => {
      await expect(updateStatusSoldListing('invalid_id', state))
        .rejects.toThrow();
    });
  });
});

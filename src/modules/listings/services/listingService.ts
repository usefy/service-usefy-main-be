import * as _ from 'lodash';
import { State } from '../../../models';
import { listingsRepo } from '../repositories/listings';
import { errors } from '../../../errors';
import { ListingStatus, ListingItem } from '../models';
import v4 from 'uuid/v4';
import moment from 'moment';
import { sportsRepo } from '../../sports/repositories/sportsRepo';
import { userRepo } from '../../users/repositories/users';
import * as notification from '../../../components/notification';
import { processKeywords } from './searchService';
import { profileService } from '../../users/services/profileService';
import { config } from '../../../config';
import { ordersRepo } from '../../checkout/repositories/orders';
import { Sport } from '../../sports/models/Sport';
import { User } from '../../users/models/User';
import * as tracking from '../../../components/tracking';
import { ticket } from '../../../components/ticket';

const createSearchableProperties = (listingItem) => {
  const listingClone = _.cloneDeep(listingItem);
  if (_.has(listingItem, 'title')) {
    listingClone.titleSearch = listingItem.title.toLowerCase();
  }
  if (_.has(listingItem, 'story')) {
    listingClone.storySearch = listingItem.story.toLowerCase();
  }
  return listingClone;
};

export interface ItemFilter {
  itemStatus: string;
}

export async function getUserListings(state: State, filter: ItemFilter) {
  state.logger.info('User listings service');
  const { uid } = state.user;

  try {
    let listings = await listingsRepo.getByUserUid(uid, state);

    if (filter && filter.itemStatus === ListingStatus.sold) {
      const getOrdersPromise = listings
        .map(async (item) => {
          const order = await ordersRepo.getByListingId(item.id, state);
          return Object.assign({}, item, { order });
        });

      listings = await Promise.all(getOrdersPromise);
      listings = listings.filter(listing => !!listing.order);

      const getBuyersPromise = listings
        .map(async (item) => {
          const buyer = await profileService({ uid: item.order.uid }, state);
          return Object.assign({}, item, { buyer });
        });

      listings = await Promise.all(getBuyersPromise);
    }

    return listings;
  } catch (err) {
    state.logger.error({ err }, 'Error getting user\'s listings');
    throw err;
  }
}

export async function getUserSingleListing(params, state: State) {
  state.logger.info('User\'s single listings service');

  const { id } = params;
  const { uid } = state.user;
  const isAdmin = (config.adminsUid.indexOf(uid) >= 0);

  try {

    // apenas o usuario dono ou o admin pode executar essa query de equipo
    const item = isAdmin ?
      await listingsRepo.getById(id, state) :
      await listingsRepo.getByUserUidAndId(uid, id, state);

    if (!item) {
      state.logger.warn({ item, id }, 'Item not found on updating listing');
      throw errors.notFound('Listing item not found', { id });
    }

    const sports: Promise<Sport>[] = item.sports.map(
      async (sportItem: string): Promise<Sport> => {
        const sport = await sportsRepo.getOneById(sportItem, state);

        if (sport) {
          return <Sport>sport;
        }

        state.logger.warn('sport not found');
        return <Sport>{};
      },
    );

    item.sports = await Promise.all(sports);
    item.user = await profileService({ username: undefined, uid: item.firebaseUid }, state);

    return item;
  } catch (err) {
    state.logger.error({ err }, 'Error getting single listing');
    throw err;
  }
}

export async function getSingleListing(params, state: State) {
  const { id } = params;
  state.logger.info('Single listings service');

  try {
    const item = await listingsRepo.getById(id, state);

    if (!item) {
      state.logger.warn({ item, id }, 'Item not found');
      throw errors.notFound('Listing item not found', { id });
    }

    return item;
  } catch (err) {
    state.logger.error({ err }, 'Error getting single listing');
    throw err;
  }
}

async function extraValidations(document: ListingItem, state: State) {
  const conditions = [
    'as-new',
    'lightly-used',
    'used',
  ];

  const {
    itemCondition,
    yearOfManufacture,
    sports,
  } = document;

  // validate conditions
  if (itemCondition && conditions.indexOf(itemCondition!.toString()) < 0) {
    state.logger.warn({ itemCondition }, 'Invalid condition for new listing');
    throw errors.badRequest('Invalid condition', { itemCondition });
  }

  // validate year
  if (yearOfManufacture && moment().year(parseInt(yearOfManufacture, 10)).isAfter(moment(), 'year')) {
    state.logger.warn({ yearOfManufacture }, 'Invalid year of manufacture for new listing');
    throw errors.badRequest('Invalid uear of manufacture', { yearOfManufacture });
  }

  // validate sport
  if (sports) {
    for (const sport of sports!) {
      const foundSport = await sportsRepo.getOneById(<string>sport, state);
      if (!foundSport) {
        state.logger.warn({ sport, sports }, 'Invalid sport for a new listing.');
        throw errors.badRequest('Invalid sport', { sport });
      }
    }
  }
}

export async function newListing(body: any, state: State) {
  state.logger.info({ body }, 'New listing');
  const { uid } = state.user;

  const document: ListingItem = _.merge({}, _.cloneDeep(body), createSearchableProperties(body));

  await extraValidations(document, state);

  document.id = v4();
  document.firebaseUid = uid;
  // todo cadastro começa em rascunho
  document.itemStatus = ListingStatus.draft;

  try {
    const response = await listingsRepo.create(document, state);
    const { titleSearch, storySearch, ...listing } = response;

    await tracking.send(
      {
        id: uid,
        name: config.customerio.track.equipo.new,
        data: {},
      },
      state,
    );

    state.logger.info({ newListing: response }, 'New listing created');
    return listing;
  } catch (err) {
    state.logger.error({ err }, 'Error on creating new listing.');
    throw err;
  }
}

export async function updateStatusSoldListing(id: string, state: State) {
  const listing = await listingsRepo.getById(id, state);

  if (_.isEqual(listing, {})) {
    throw new Error('Not found equipo');
  }

  const response = await listingsRepo.update(
    listing.firebaseUid,
    id,
    {
      ...listing,
      itemStatus: ListingStatus.sold,
    },
    state,
  );

  const { titleSearch, storySearch, ...updatedListining } = response;

  return updatedListining;
}

export async function updateListing(params: { id: string }, body: any, state: State) {
  const { id } = params;
  state.logger.info({ body }, 'Update listing');

  const { uid } = state.user;
  const isAdmin = (config.adminsUid.indexOf(uid) >= 0);

  try {
    const currentListing = isAdmin ?
      await listingsRepo.getById(id, state) :
      await listingsRepo.getByUserUidAndId(uid, id, state);

    const currentStatus = currentListing.itemStatus;

    if (!currentListing) {
      state.logger.warn({ item: currentListing, listing: body }, 'Item not found on updating listing');
      throw errors.notFound('Listing item not found', { id });
    }

    // se menor guarda o valor antigo para mostrar no front
    if (_.has(body, 'price') && body.price < currentListing.price) {
      body.newPrice = body.price;
      body.price = currentListing.price;
    } else {
      body.newPrice = null;
    }

    await extraValidations(body, state);

    const clonedCurrentListing: ListingItem = Object.assign({}, currentListing, _.cloneDeep(body), createSearchableProperties(body));

    const newStatus = clonedCurrentListing.itemStatus;
    const isStatusChanged = (newStatus !== currentStatus);
    const isNowEnabled = (isStatusChanged && currentStatus !== ListingStatus.enabled && newStatus === ListingStatus.enabled);

    if (isNowEnabled) {
      // apenas admin pode habilitar um listing
      if (!isAdmin) {
        state.logger.warn({ uid, id }, 'Listing enabling not authorized');
        throw errors.badRequest('Status change not authorized or invalid', {});
      }

      clonedCurrentListing.publishedAt = moment().toISOString();
    }

    const updatedListing = await listingsRepo.update(currentListing.firebaseUid, id, clonedCurrentListing, state);
    const seller: User = await userRepo.getByUserUid(state.user.uid, state);

    if (isStatusChanged) {
      // await notifyStatusChange(response!, state);

      if (newStatus === ListingStatus.waitingReview) {
        await ticket.create(
          {
            message: `
              Nome: ${seller.displayName}
              E-mail: ${seller.email}
              Nome do Eqipo: ${currentListing.title}
              Data de Inclusão do Eqipo: ${moment(currentListing.createdAt).format('DD/MM/YYYY HH:MM:SS')}
              Link: ${config.frontend.url}/${id}
            `,
          },
          state,
        );

        await tracking.send(
          {
            id: uid,
            name: config.customerio.track.equipo.review,
            data: {
              equipo: {
                title: currentListing.title,
                amount: currentListing.amount,
              },
              seller: {
                displayName: seller.displayName,
              },
            },
          },
          state,
        );
      }
    }

    if (isNowEnabled) {
      // update indexes and keywords for search
      state.logger.info({ status: newStatus, id: updatedListing!.id }, 'Listing item status changed to enabled');

      // await notifyEnabledListing(response!, state);

      await processKeywords(<ListingItem>clonedCurrentListing, state);
      await tracking.send(
        {
          id: uid,
          name: config.customerio.track.equipo.enabled,
          data: {
            equipo: {
              title: currentListing.title,
            },
            seller: {
              displayName: seller.displayName,
            },
          },
        },
        state,
      );
    }

    const { titleSearch, storySearch, ...listing } = updatedListing;

    state.logger.info({ listing: updatedListing }, 'Listing updated');
    return listing;
  } catch (err) {
    state.logger.error({ err }, 'Error on updating a listing');
    throw err;
  }
}

export interface Filter {
  sport: string;
  itemCondition: string;
  keyword: string;
}

export async function getEnabledListings(state: State, filter?: Filter) {
  state.logger.info('Get enabled listings service');

  try {
    let listings = await listingsRepo.getEnabled(state, filter);

    listings = listings.map(async (item) => {
      const seller = await profileService({ uid: item.firebaseUid }, state);

      return Object.assign({}, item, { seller });

    });

    listings = await Promise.all(listings);
    return listings;
  } catch (err) {
    state.logger.error({ err }, 'Error getting enabled listings');
    throw err;
  }
}

export async function getListingsCuration(state: State) {
  state.logger.info('Get listings curation service');

  try {
    let listings = await listingsRepo.getReview(state);

    listings = listings.map(async (item) => {
      const seller = await profileService({ uid: item.firebaseUid }, state);

      return Object.assign({}, item, { seller });

    });

    listings = await Promise.all(listings);
    return listings;
  } catch (err) {
    state.logger.error({ err }, 'Error listings curation');
    throw err;
  }
}

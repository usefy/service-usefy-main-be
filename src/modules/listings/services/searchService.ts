import * as _ from 'lodash';
import * as TimSort from 'timsort';
import moment = require('moment');
import { State } from '../../../models';
import { ListingItem, ListingSearchSort } from '../models';
import { listingsKeywordsRepo } from '../repositories/listingsKeywords';
import { sportsRepo } from '../../sports/repositories/sportsRepo';
import { listingsRepo } from '../repositories/listings';
import { ordersRepo } from '../../checkout/repositories/orders';
import { errors } from '../../../errors';
import { singleSportService } from '../../sports/services/sportsService';
import { profileService } from '../../users/services/profileService';

export const processKeywords = async (item: ListingItem, state: State) => {
  state.logger.info({ id: item.id }, 'Starting the extracting keywords process');

  let keywords: string[] = [];
  let sports: string[] = [];

  keywords = keywords.concat(item.title!.split(' ').map(keyword => keyword.toLowerCase()));
  keywords = keywords.concat(item.story!.split(' ').map(keyword => keyword.toLowerCase()));

  if (item.manufacturer) {
    keywords = keywords.concat(item.manufacturer!.split(' ').map(keyword => keyword.toLowerCase()));
  }
  if (item.model) {
    keywords = keywords.concat(item.model!.split(' ').map(keyword => keyword.toLowerCase()));
  }

  try {
    for (const sport of item.sports!) {
      const foundSport = await sportsRepo.getOneById(<string>sport, state);
      sports.push(foundSport!.id);
    }

    // pog para remover os duplicados
    keywords = [...new Set(keywords)];
    sports = [...new Set(sports)];

    // deletar antes de gravar novos
    await listingsKeywordsRepo.deleteAllById(item.id!, state);

    const promises = keywords.map(async (keyword) => {
      await listingsKeywordsRepo.create(
        {
          keyword,
          sports,
          id: item.id,
          firebaseUid: item.firebaseUid,
          price: item.price,
          itemCondition: item.itemCondition,
          hasInvoice: item.hasInvoice,
          publishedAt: item.publishedAt,
        },
        state,
      );
    });

    return Promise.all(promises);
  } catch (err) {
    state.logger.error({ err }, 'Error on processing item keywords.');
    throw err;
  }
};

/**
 * build query parameters for next page
 *
 * @param {*} params
 * @returns {string} partial url
 */
function buildNext(params) {
  // TODO: handle offset
  const tempParams = Object.keys(params).map((item) => {
    return item === 'offset' ? `${item}=${encodeURIComponent(JSON.stringify(params[item]))}` : `${item}=${params[item]}`;
  });
  return `/v1/search?${tempParams.join('&')}`;
}

function listingSort(collection: any[], sort: ListingSearchSort): any[] {
  const recent = ({ publishedAt: a }, { publishedAt: b }) => a === b ? 0 : (moment(a).isBefore(b) ? -1 : 1);
  const priceDesc = ({ price: a }, { price: b }) => (a - b);
  const priceAsc = ({ price: a }, { price: b }) => (b - a);

  switch (sort) {
    case ListingSearchSort.priceAsc:
      TimSort.sort(collection, priceAsc);
      break;
    case ListingSearchSort.priceDesc:
      TimSort.sort(collection, priceDesc);
      break;
    case ListingSearchSort.recent:
    default:
      TimSort.sort(collection, recent);
      break;
  }

  return collection;
}

export const searchListings = async (
  params,
  state: State,
) => {
  const {
    keyword,
    sort = 'recent',
    offset,
    limit = 24,
    sports,
    priceFrom,
    priceTo,
    hasInvoice,
    itemCondition,
  } = params;
  let keywords;
  let listings: any[] = [];
  let foundIds: any[] = [];
  let parsedOffset = offset ? JSON.parse(decodeURIComponent(offset)) : undefined;

  if (keyword) {
    keywords = keyword.split(' ').map(keyword => keyword.toLowerCase());
  }

  if (priceFrom && priceTo && priceFrom > priceTo) {
    throw errors.badRequest('Invalid price range', { priceFrom, priceTo });
  }

  do {
    const params = {
      keywords,
      limit,
      priceFrom,
      priceTo,
      hasInvoice,
      itemCondition,
    };

    if (parsedOffset) {
      params['offset'] = parsedOffset;
    }

    if (sports) {
      params['sports'] = Array.isArray(sports) ? sports : [sports];
    }

    const { data, lastKey } = await listingsKeywordsRepo.search(params, state);

    foundIds = foundIds.concat(data);

    // clean duplicates
    const seen = {};
    foundIds = foundIds.filter((item) => {
      return seen.hasOwnProperty(item.id) ? false : (seen[item.id] = true);
    });

    parsedOffset = lastKey;
  } while (foundIds.length < limit && parsedOffset);

  if (foundIds && foundIds.length > 0) {
    listings = await listingsRepo.getByKeys(foundIds, state);

    // sort results
    listings = listingSort(listings, sort);
  }

  listings = listings.map(async (item) => {
    const sports = await Promise.all(item.sports.map(async sport => singleSportService(sport, state)));
    const seller = await profileService({ uid: item.firebaseUid }, state);

    return Object.assign({}, item, { sports, seller });

  });

  listings = await Promise.all(listings);

  // return only listings that are enabled
  listings = listings.filter(l => l.itemStatus === 'enabled');

  const response: {
    listings: any[],
    count: number,
    next?: string,
  } = { listings, count: foundIds!.length || 0 };

  const toBuild = _.merge({}, params, { offset: parsedOffset ? parsedOffset : false }, { limit, sort });
  response.next = buildNext(toBuild);

  return response;
};

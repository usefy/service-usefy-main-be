import get from 'lodash/get';
import { favoritesRepo } from '../repositories/favorites';
import { State } from '../../../models';
import { FavoritesDocument } from '../models';
import { getSinglePublicListing } from './publicListingService';

export const getUsersFavorites = async (state: State) => {
  const favorites = await favoritesRepo.getByUserUid(state.user.uid, state);

  if (favorites.length < 1) {
    return { listings: favorites, count: favorites.length };
  }

  const listings = favorites.map(async (fav: FavoritesDocument) => {
    try {
      const l = await getSinglePublicListing({ id: fav.id }, state);
      return l;
    } catch (err) {
      state.logger.info({ err, favorite: fav }, 'Get favorite listing returned error.');
      if (get(err, 'statusCode') === 404) return null;
      throw err;
    }
  });

  const completeListings =  Promise.all(listings);

  return { listings: completeListings, count: favorites.length };
};

export const addToFavorites = async (params, state: State) => {
  const { id } = params;

  const fav = await favoritesRepo.create(
    {
      id,
      uid: state.user.uid,
    },
    state,
  );

  state.logger.info(
    {
      id,
      uid: state.user.uid,
    },
    'New favorite was created.',
  );

  return fav;
};

export const removeFromFavorites = async (params, state: State) => {
  const { id } = params;

  await favoritesRepo.delete(
    {
      id,
      uid: state.user.uid,
    },
    state,
  );

  state.logger.info(
    {
      id,
      uid: state.user.uid,
    },
    'New favorite was removed.',
  );

  return '';
};

import * as _ from 'lodash';
import moment from 'moment';
import { favoritesTable } from '../tables/favoritesTable';
import { State } from '../../../models';
import { FavoritesDocument } from '../models';

export const favoritesRepo = {

  create: async (document: Partial<FavoritesDocument>, state: State): Promise<FavoritesDocument> => {
    const documentCopy = _.cloneDeep(document);
    return favoritesTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  getOne: async (key, state: State) => favoritesTable.get(key, state),

  getByUserUid: async (uid: string, state: State) => favoritesTable.getByUserUid(uid, state),

  deleteAll: state => favoritesTable.deleteAll(['firebaseUid', 'id'], state),

  delete: (key, state: State) => favoritesTable.delete(key, state),

  validate: document => favoritesTable.validate(document),

  countAll: (state: State) => favoritesTable.countAll(state),
};

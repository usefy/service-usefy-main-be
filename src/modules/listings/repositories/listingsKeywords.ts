import * as _ from 'lodash';
import moment from 'moment';
import { listingsKeywordTable } from '../tables/listingsKeywordTable';
import { State } from '../../../models';

export const listingsKeywordsRepo = {

  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return listingsKeywordTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  search: (params, state: State) => listingsKeywordTable.search(params, state),

  deleteAllById: (id: string, state: State) => listingsKeywordTable.deleteAllById(id, state),

  deleteAll: state => listingsKeywordTable.deleteAll(['id', 'keyword'], state),

  delete: (key, state: State) => listingsKeywordTable.delete(key, state),

  validate: document => listingsKeywordTable.validate(document),

  countAll: (state: State) => listingsKeywordTable.countAll(state),
};

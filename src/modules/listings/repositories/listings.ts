import * as _ from 'lodash';
import moment from 'moment';
import { listingsTable } from '../tables/listingsTable';
import { State } from '../../../models';
import { Filter } from '../services/listingService';

export const listingsRepo = {

  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return listingsTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  update: async (uid: string, id: string, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return listingsTable.update(
      uid,
      id,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  getOne: async (key, state: State) => {
    const listings = await listingsTable.get(key, state);
    return listings.map(({ titleSearch, storySearch, ...listing }) => listing || {});
  },

  getByKeys: async (keys, state: State) => {
    const listings = await listingsTable.getByKeys(keys, state);
    return listings.map(({ titleSearch, storySearch, ...listing }) => listing || {});
  },

  getById: async (id: string, state: State) => {
    const { titleSearch, storySearch, ...listing } = await listingsTable.getById(id, state) || {};
    return listing;
  },

  getEnabledById: async (id: string, state: State) => {
    const { titleSearch, storySearch, ...listing } = await listingsTable.getEnabledById(id, state) || {};
    return listing;
  },

  getEnabled: async (state: State, filter?: Filter) => {
    const listings = await listingsTable.getEnabled(state, filter);
    return listings.map(({ titleSearch, storySearch, ...listing }) => listing || {});
  },

  getByUserUid: async (uid: string, state: State) => {
    const listings = await listingsTable.getByUserUid(uid, state);
    return listings.map(({ titleSearch, storySearch, ...listing }) => listing || {});
  },

  getByUserUidAndId: async (uid: string, id: string, state: State) => {
    const { titleSearch, storySearch, ...listing } = await listingsTable.getByUserUidAndId(uid, id, state) || {};
    return listing;
  },

  deleteAll: state => listingsTable.deleteAll(['firebaseUid', 'id'], state),

  delete: (key, state: State) => listingsTable.delete(key, state),

  validate: document => listingsTable.validate(document),

  countAll: (state: State) => listingsTable.countAll(state),

  getReview: (state: State) => listingsTable.getReview(state),
};

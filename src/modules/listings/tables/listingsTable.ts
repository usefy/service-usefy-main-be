import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';
import { errors } from '../../../errors';
import { ListingStatus } from '../models';
import { Filter } from '../services/listingService';

const tableName = `usefy-${config.server.configEnv}-listings`;

const validationScheme = joi.object().keys({
  firebaseUid: joi.string().required(),
  id: joi.string().required(),
  title: joi.string().min(10).max(1024).required(),
  titleSearch: joi.string().min(10).max(1024),
  story: joi.string().max(5120).required(),
  storySearch: joi.string().max(5120),
  price: joi.number().min(1).positive().precision(2).required(),
  newPrice: joi.number().min(1).positive().precision(2).allow(null),
  itemCondition: joi.string().required(),
  size: joi.string(),
  manufacturer: joi.string().required(),
  yearOfManufacture: joi.string().regex(/^[0-9]{4}$/),
  model: joi.string(),
  material: joi.string(),
  capacity: joi.string(),
  hasInvoice: joi.boolean().required(),
  hasWarranty: joi.boolean().required(),
  sports: joi.array().items(
    joi.string().guid().required(),
  ).required(),
  photos: joi.array().items(
    joi.string().uri(),
  ),
  cover: joi.string().uri(),
  shipmentWeight: joi.number(),
  shipmentDepth: joi.number(),
  shipmentHeight: joi.number(),
  shipmentWidth: joi.number(),
  shipmentDiameter: joi.number(),
  itemStatus: joi.string().required(),
  createdAt: joi.date().iso().required(),
  updatedAt: joi.date().iso(),
  publishedAt: joi.date().iso(),
});

export class ListingModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUserUid(uid: string, state: State) {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#firebaseUid = :firebaseUid',
      ExpressionAttributeNames: {
        '#firebaseUid': 'firebaseUid',
      },
      ExpressionAttributeValues: {
        ':firebaseUid': uid,
      },
      ConsistentRead: true,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items;
  }

  async getById(id: string, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'id_index',
      KeyConditionExpression: '#id = :id',
      ExpressionAttributeNames: {
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items![0];
  }

  async getEnabledById(id: string, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'id_index',
      KeyConditionExpression: '#id = :id',
      FilterExpression: '#itemStatus = :status',
      ExpressionAttributeNames: {
        '#id': 'id',
        '#itemStatus': 'itemStatus',
      },
      ExpressionAttributeValues: {
        ':id': id,
        ':status': ListingStatus.enabled,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items![0];
  }

  async getEnabled(state: State, filter: Filter) {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#itemStatus = :status',
      ExpressionAttributeNames: {
        '#itemStatus': 'itemStatus',
      },
      ExpressionAttributeValues: {
        ':status': ListingStatus.enabled,
      },
      ConsistentRead: false,
    };

    if (filter.keyword) {
      params.ExpressionAttributeNames['#title'] = 'titleSearch';
      params.ExpressionAttributeNames['#story'] = 'storySearch';

      params.FilterExpression += ' AND (';
      filter.keyword.toLowerCase().split(' ').forEach((keyword, index) => {
        const keywordItem = `:keyword${index}`;
        params.ExpressionAttributeValues[keywordItem] = keyword;
        params.FilterExpression += `${!!index ? 'OR ' : ''}contains (#title, ${keywordItem}) OR contains (#story, ${keywordItem})`;
      });
      params.FilterExpression += ')';
    }

    if (filter && filter.sport) {
      params.ExpressionAttributeNames['#sports'] = 'sports';
      params.FilterExpression += ' AND contains (#sports, :sportId)';
      params.ExpressionAttributeValues[':sportId'] = filter.sport;
    }

    if (filter.itemCondition) {
      params.ExpressionAttributeNames['#itemCondition'] = 'itemCondition';
      params.ExpressionAttributeValues[':itemCondition'] = filter.itemCondition;
      params.FilterExpression += ' AND #itemCondition = :itemCondition';
    }

    const { Items } = await this.dynamoScan(params, state);
    return Items;
  }

  async getByUserUidAndId(uid: string, id: string, state: State) {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#firebaseUid = :firebaseUid and #id = :id',
      ExpressionAttributeNames: {
        '#firebaseUid': 'firebaseUid',
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
        ':firebaseUid': uid,
      },
      ConsistentRead: true,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items![0];
  }

  async update(uid: string, id: string, document, state: State) {
    const validation = joi.validate(document, this.validationScheme);
    if (validation.error) throw errors.validation(_.get(validation, 'error.message'), validation.error);

    // tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    let documentCopy = _.cloneDeep(document);
    const toOmit = ['firebaseUid', 'id'];
    documentCopy = _.omit(documentCopy, toOmit);

    for (const key in documentCopy) {
      if (documentCopy.hasOwnProperty(key) && documentCopy[key]) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(documentCopy, key);
      }
    }

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        id,
        firebaseUid: uid,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return data;
  }

  async getByKeys(keys, state: State) {
    const params = { RequestItems: {} };

    params.RequestItems[this.tableName] = {
      Keys: keys,
      ConsistentRead: false,
      ProjectionExpression: 'firebaseUid, id, title, price, itemCondition, hasInvoice, sports, cover, photos, itemStatus, publishedAt',
    };

    state.logger.info({ dynamodbCall: params }, 'batchGet call to get items from ids list');
    const { Responses } = await this.client.batchGet(params).promise();
    return Responses ? Responses![this.tableName] : [];
  }

  async getReview(state: State) {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#itemStatus = :status',
      ExpressionAttributeNames: {
        '#itemStatus': 'itemStatus',
      },
      ExpressionAttributeValues: {
        ':status': ListingStatus.waitingReview,
      },
      ConsistentRead: false,
    };

    const { Items } = await this.dynamoScan(params, state);
    return Items;
  }
}

export const listingsTable = new ListingModel(tableName, validationScheme);

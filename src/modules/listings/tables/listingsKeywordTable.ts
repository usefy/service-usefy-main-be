import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';

const tableName = `usefy-${config.server.configEnv}-listings-keywords`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  keyword: joi.string().required(),
  price: joi.number().min(1).positive().precision(2).required(),
  itemCondition: joi.string().required(),
  hasInvoice: joi.boolean().required(),
  sports: joi.array().items(joi.string().required()).required(),
  firebaseUid: joi.string().required(),
  publishedAt: joi.date().iso(),
  createdAt: joi.date().iso(),
});

export class ListingKeywordModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async deleteAllById(id, state) {
    // we are looping until the scan is empty
    while (true) {
      const { Items } = await this.client
        .scan({
          TableName: this.tableName,
          FilterExpression: '#id = :id',
          ExpressionAttributeNames: {
            '#id': 'id',
          },
          ExpressionAttributeValues: {
            ':id': id,
          },
          ConsistentRead: true,
          Limit: 40,
        })
        .promise();

      if (!Items || _.isEmpty(Items)) {
        break;
      }

      const params = {
        RequestItems: {},
      };
      const keyList = ['id', 'keyword'];

      params.RequestItems[this.tableName] = Items.map((item) => {
        const key = {};
        keyList.forEach((k) => {
          key[k] = item[k];
        });
        return { DeleteRequest: { Key: key } };
      });

      state.logger.info(
        { dynamodbCall: params },
        'batchWrite call to remove all items',
      );
      await this.client.batchWrite(params).promise();
    }
    return true;
  }

  async search(
    {
      keywords,
      offset,
      limit,
      sports,
      priceFrom,
      priceTo,
      hasInvoice,
      itemCondition,
    },
    state: State,
  ) {
    // tslint:disable-next-line: variable-name
    let FilterExpression = '';
    // tslint:disable-next-line: variable-name
    const ExpressionAttributeValues = {};
    // tslint:disable-next-line: variable-name
    const ExpressionAttributeNames = {};

    if (keywords) {
      FilterExpression = 'contains (:keywords, #keyword)';
      ExpressionAttributeValues[':keywords'] = keywords;
      ExpressionAttributeNames['#keyword'] = 'keyword';
    }

    if (sports && sports.length > 0) {
      ExpressionAttributeNames['#sports'] = 'sports';

      FilterExpression += ` ${FilterExpression ? 'AND' : ''} (${sports
        .map((sport, index) => {
          ExpressionAttributeValues[`:sport${index}`] = sport;
          return `contains(#sports, :sport${index})`;
        })
        .join(' OR ')})`;
    }

    if (priceFrom) {
      ExpressionAttributeNames['#price'] = 'price';
      ExpressionAttributeValues[':priceFrom'] = parseInt(priceFrom, 10);
      FilterExpression += ` ${FilterExpression ? 'AND' : ''} #price >= :priceFrom`;
    }
    if (priceTo) {
      if (!ExpressionAttributeNames.hasOwnProperty('#price')) {
        ExpressionAttributeNames['#price'] = 'price';
      }

      ExpressionAttributeValues[':priceTo'] = parseInt(priceTo, 10);
      FilterExpression += ` ${FilterExpression ? 'AND' : ''} #price <= :priceTo`;
    }

    if (hasInvoice) {
      const tempHasInvoice = hasInvoice === 'true';
      ExpressionAttributeNames['#hasInvoice'] = 'hasInvoice';
      ExpressionAttributeValues[':hasInvoice'] = tempHasInvoice;
      FilterExpression += ` ${FilterExpression ? 'AND' : ''} #hasInvoice = :hasInvoice`;
    }

    if (itemCondition) {
      ExpressionAttributeNames['#itemCondition'] = 'itemCondition';
      ExpressionAttributeValues[':itemCondition'] = itemCondition;
      FilterExpression += ` ${FilterExpression ? 'AND' : ''} #itemCondition = :itemCondition`;
    }

    const params = {
      FilterExpression,
      ExpressionAttributeValues,
      ExpressionAttributeNames,
      TableName: this.tableName,
      IndexName: 'id_publishedAt_index',
      ScanIndexForward: false,
      Limit: limit,
      ProjectionExpression: 'id, firebaseUid',
      ConsistentRead: false,
    };

    if (offset) {
      params['ExclusiveStartKey'] = offset;
    }

    const result = await this.dynamoScan(params, state);
    return { data: result.Items, lastKey: result.LastEvaluatedKey };
  }
}

export const listingsKeywordTable = new ListingKeywordModel(
  tableName,
  validationScheme,
);

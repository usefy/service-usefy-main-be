import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';
import { FavoritesDocument } from '../models';

const tableName = `usefy-${config.server.configEnv}-listings-favorites`;

const validationScheme = joi.object().keys({
  uid: joi.string().required(),
  id: joi.string().required(),
  createdAt: joi.date().required(),
});

export class ListingFavoritesModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUserUid(uid: string, state: State): Promise<FavoritesDocument[]> {
    const params = {
      TableName: this.tableName,
      IndexName: 'uid_index',
      KeyConditionExpression: '#uid = :uid',
      ExpressionAttributeNames: {
        '#uid': 'uid',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
      },
      // ConsistentRead: true,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return <FavoritesDocument[]>Items;
  }
}

export const favoritesTable = new ListingFavoritesModel(tableName, validationScheme);

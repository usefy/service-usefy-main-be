import { string } from 'joi';
import { Sport } from '../../sports/models/Sport';

export enum ListingStatus {
  draft = 'draft',
  waitingReview = 'waiting-review',
  profileIncomplete = 'profile-incomplete',
  disabled = 'disabled',
  enabled = 'enabled',
  sold = 'sold',
  delivered = 'delivered',
  received = 'received',
}

export interface ListingItem {
  firebaseUid?: string;
  id?: string;
  title?: string;
  titleSerch?: string;
  story?: string;
  storySearch?: string;
  price?: number;
  itemCondition?: string;
  size?: string;
  manufacturer?: string;
  yearOfManufacture?: string;
  model?: string;
  material?: string;
  capacity?: string;
  hasInvoice?: boolean;
  hasWarranty?: boolean;
  sports?: Sport[] | string[];
  photos?: string[];
  cover?: string[];
  itemStatus?: string;
  createdAt?: string;
  updatedAt?: string;
  publishedAt?: string;
}

export enum ListingSearchSort {
  recent = 'recent',
  priceDesc = 'price+',
  priceAsc = 'price-',
}

export interface FavoritesDocument {
  uid: string;
  id: string;
  createdAt: Date | string;
}

import availableBanksJson from '../tables/available-banks.json';

export const availableBanksRepo = {
  all: () => availableBanksJson,
};

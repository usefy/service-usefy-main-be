import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import * as availableBanksController from '../controllers/availableBanksController';

export const router = Router();

router
  .route('/available-banks')
  .get(
    routerDefaultHandler(availableBanksController.get),
  );

import { availableBanksRepo } from '../repositories/availableBanksRepo';
import { State } from '../../../models';

export function availableBanksService(state: State) {
  state.logger.info('Available Banks service');

  const allAvailableBanks = availableBanksRepo.all();

  return allAvailableBanks;
}

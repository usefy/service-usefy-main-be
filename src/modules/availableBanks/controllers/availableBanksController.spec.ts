import supertest from 'supertest';
import { app } from '../../../app';

describe('Available Banks Module', () => {
  describe('All banks module', () => {
    it('should list of all available banks', async () => {
      await supertest(app)
        .get('/v1/available-banks/')
        .expect(200);
    });
  });
});

import { Request, Response } from 'express';
import { availableBanksService } from '../services/availableBanksService';

export async function get(req: Request, res: Response) {
  return availableBanksService(req.state);
}

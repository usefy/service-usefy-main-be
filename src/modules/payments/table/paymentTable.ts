import * as joi from '@hapi/joi';
import * as _ from 'lodash';

import { DefaultModel } from '../../../components/dyno';
import { config } from '../../../config';
import { State } from '../../../models';
import { PaymentDocument } from '../models/Payment';

const tableName = `usefy-${config.server.configEnv}-moipPayments`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  uid: joi.string().required(),
  orderId: joi.string().required(),
  moipOrderId: joi.string().required(),
  amount: joi.string().required(),
  installmentCount: joi.number().required(),
  method: joi.string().required(),
  paymentStatus: joi.string().required(),
  linksSelf: joi.string().required(),
  updatedAt: joi.date(),
  createdAt: joi.date(),
});

export class PaymentModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUid(uid: string, state: State): Promise<PaymentDocument[] | null> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#uid = :uid',
      ExpressionAttributeNames: {
        '#uid': 'uid',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <PaymentDocument[]>data || null;
  }

  async getById(id: string, state: State): Promise<PaymentDocument | null> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#id = :id',
      ExpressionAttributeNames: {
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <PaymentDocument>data[0] || null;
  }

  async getByOrderId(orderId: string, state: State): Promise<PaymentDocument | null> {
    const params = {
      TableName: this.tableName,
      IndexName: 'orderId_uid_index',
      FilterExpression: '#orderId = :orderId',
      ExpressionAttributeNames: {
        '#orderId': 'orderId',
      },
      ExpressionAttributeValues: {
        ':orderId': orderId,
      },
      ConsistentRead: false,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <PaymentDocument>data[0] || null;
  }

  async update(key: {id: string, uid: string}, document: Partial<PaymentDocument>, state: State): Promise<PaymentDocument | null> {
    // tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    for (const key in document) {
      if (document.hasOwnProperty(key) && document[key]) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(document, key);
      }
    }

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: key,
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return <PaymentDocument>data;
  }
}

export const paymentTable = new PaymentModel(
  tableName,
  validationScheme,
);

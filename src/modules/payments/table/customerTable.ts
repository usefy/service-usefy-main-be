import * as joi from '@hapi/joi';

import { DefaultModel } from '../../../components/dyno';
import { config } from '../../../config';
import { State } from '../../../models';
import { CustomerDocument } from '../models/Customer';

const tableName = `usefy-${config.server.configEnv}-moipCustomers`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  uid: joi.string().required(),
  createdAt: joi.string(),
  moipAccountId: joi.string(),
  linksSelf: joi.string(),
});

export class CustomerTable extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUid(uid: string, state: State): Promise<CustomerDocument | null> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#uid = :uid',
      ExpressionAttributeNames: {
        '#uid': 'uid',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <CustomerDocument>data![0] || null;
  }
}

export const customerTable = new CustomerTable(
  tableName,
  validationScheme,
);

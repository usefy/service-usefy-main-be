import * as joi from '@hapi/joi';

import { DefaultModel } from '../../../components/dyno';
import { config } from '../../../config';
import { State } from '../../../models';
import { OrderStatus } from '../../checkout/models/order';
import { OrderDocument } from '../models/Orders';

const tableName = `usefy-${config.server.configEnv}-moipOrders`;

const validationScheme = joi.object().keys({
  uid: joi.string().required(),
  id: joi.string().required(),
  orderId: joi.string().required(),
  amount: joi.string().required(),
  createdAt: joi.date(),
  updatedAt: joi.date(),
  payments: joi.string(),
  escrows: joi.string(),
  linksSelf: joi.string(),
  linksCheckout: joi.string(), // Text
});

export class MoipOrderModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getById(id: string, state: State): Promise<OrderDocument | null> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#id = :id',
      ExpressionAttributeNames: {
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <OrderDocument>data![0] || null;
  }

  async getByOrderId(orderId: string, state: State): Promise<OrderDocument | null> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#orderId = :orderId',
      ExpressionAttributeNames: {
        '#orderId': 'orderId',
      },
      ExpressionAttributeValues: {
        ':orderId': orderId,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <OrderDocument>data![0] || null;
  }
}

export const orderTable = new MoipOrderModel(tableName, validationScheme);

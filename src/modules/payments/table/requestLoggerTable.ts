import * as joi from '@hapi/joi';

import { DefaultModel } from '../../../components/dyno';
import { config } from '../../../config';
import { State } from '../../../models';
import { RequestLogger } from '../models/RequestLogger';

const tableName = `usefy-${config.server.configEnv}-requestLoggers`;

const validationScheme = joi.object().keys({
  response: joi.string(), // Text
  method: joi.string(), // Text
  params: joi.string(), // Text
  query: joi.string(), // Text
  body: joi.string(), // Text
  uri: joi.string(), // Text
  headers: joi.string(), // Text
});

export class RequestLoggerModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async update(uid: string, document: RequestLogger, state: State) {
    /*// tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    for (const key in document) {
      if (document.hasOwnProperty(key) && document[key]) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(document, key);
      }
    }

    console.log(UpdateExpression, ExpressionAttributeValues);

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        firebaseUid: uid,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return data; */
  }
}

export const requestLoggerTable = new RequestLoggerModel(
  tableName,
  validationScheme,
);

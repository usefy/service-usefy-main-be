import * as joi from '@hapi/joi';

import { DefaultModel } from '../../../components/dyno';
import { config } from '../../../config';
import { State } from '../../../models';
import { CustomersFundingInstrumentDocument } from '../models/Customer';

const tableName = `usefy-${config.server.configEnv}-moipCustomersFundingInstrument`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  uid: joi.string().required(),
  creditCard: joi.string().required(),
  card: joi.string().required(),
  createdAt: joi.string(),
});

export class CustomersFundingInstrument extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUid(uid: string, state: State): Promise<CustomersFundingInstrumentDocument[] | null> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#uid = :uid',
      ExpressionAttributeNames: {
        '#uid': 'uid',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <CustomersFundingInstrumentDocument[]>data || null;
  }
}

export const customersFundingInstrument = new CustomersFundingInstrument(
  tableName,
  validationScheme,
);

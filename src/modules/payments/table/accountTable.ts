import * as joi from '@hapi/joi';
import * as _ from 'lodash';

import { DefaultModel } from '../../../components/dyno';
import { config } from '../../../config';
import { State } from '../../../models';
import { AccountDocument } from '../models/Account';

const tableName = `usefy-${config.server.configEnv}-moipAccounts`;

const validationScheme = joi.object().keys({
  uid: joi.string().required(),
  id: joi.string().required(),
  accessToken: joi.string().required(),
  channelId: joi.string().required(),
  createdAt: joi.date(),
  linksSelf: joi.string(),
  login: joi.string(),
});

export class MoipAccountModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async update({ uid, id }: { uid: string, id: string }, document: AccountDocument, state: State) {
    // tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};
    const newDocument = _.omit(document, ['uid', 'id']);

    for (const key in newDocument) {
      if (newDocument.hasOwnProperty(key) && newDocument[key]) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(newDocument, key);
      }
    }

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        uid,
        id,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return data;
  }

  async getByUid(uid: string, state: State): Promise<AccountDocument> {
    const params = {
      TableName: this.tableName,
      FilterExpression: '#uid = :uid',
      ExpressionAttributeNames: {
        '#uid': 'uid',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return <AccountDocument>data![0] || null;
  }
}

export const accountTable = new MoipAccountModel(
  tableName,
  validationScheme,
);

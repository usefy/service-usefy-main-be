import * as joi from '@hapi/joi';

import { DefaultModel } from '../../../components/dyno';
import { config } from '../../../config';
import { State } from '../../../models';
import { MoipBankAccount } from '../models/MoipBankAccount';

const tableName = `usefy-${config.server.configEnv}-moipBankAccounts`;

const validationScheme = joi.object().keys({
  uid: joi.string().required(),
  id: joi.string().required(),
  agencyNumber: joi.string().required(),
  holder: joi.object().keys({
    taxDocument: joi.object().keys({
      type: joi.string().valid('CPF', 'CNPJ').required(),
      number: joi.string().required(),
    }).required(),
    thirdParty: joi.boolean().required(),
    fullname: joi.string().required(),
  }),
  accountNumber: joi.string().required(),
  status: joi.string().required(),
  createdAt: joi.date(),
  accountCheckNumber: joi.string(),
  linksSelf: joi.string(),
  bankName: joi.string(),
  type: joi.string(),
  agencyCheckNumber: joi.string(),
  bankNumber: joi.string(),
});

export class MoipBankAccountModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async update(uid: string, document: MoipBankAccount, state: State) {
    /*// tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    for (const key in document) {
      if (document.hasOwnProperty(key) && document[key]) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(document, key);
      }
    }

    console.log(UpdateExpression, ExpressionAttributeValues);

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        firebaseUid: uid,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return data; */
  }
}

export const moipBankAccountTable = new MoipBankAccountModel(
  tableName,
  validationScheme,
);

import * as _ from 'lodash';
import { Request, Response, NextFunction } from 'express';
import { config } from '../../../config';
import { errors } from '../../../errors';
import { notificationReceived } from '../services/notificationService';

export function validateNotificationsToken(req, res, next) {
  req.state.logger.info({ headers: req.headers }, 'Check if request have the notification token');

  if (_.get(req, 'headers.authorization') !== config.moip.webhookToken) {
    return next(errors.unauthorized('Unauthorized', {}));
  }

  next();
}

export async function notificationPost(req: Request) {
  return notificationReceived(req.body, req.state);
}

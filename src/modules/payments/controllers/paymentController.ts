import { Request, Response } from 'express';

import {
  createPaymentCaptureService,
  createPaymentRefundService,
  createPaymentService,
  createPaymentVoidService,
  readPaymentRefundService,
  readPaymentService,
} from '../services/paymentService';

import { getInstallmentsAmount } from '../services/installmentService';

export async function paymentGet(req: Request, res: Response) {
  return readPaymentService(req.params, req.state);
}

export async function paymentPost(req: Request, res: Response) {
  return createPaymentService(req.params, req.body, req.state);
}

export async function paymentPostCapture(req: Request, res: Response) {
  return createPaymentCaptureService(req.params, req.state);
}

export async function paymentPostVoid(req: Request, res: Response) {
  return createPaymentVoidService(req.params, req.body);
}

export async function paymentGetRefund(req: Request, res: Response) {
  return readPaymentRefundService(req.params, req.state);
}

export async function paymentPostRefund(req: Request, res: Response) {
  return createPaymentRefundService(req.params, req.body);
}

export async function paymentGetMethods(req: Request) {

  const installmentsCount = 12;

  const installmentsAmount = await getInstallmentsAmount(<{ listingId: string }>req.params, installmentsCount, req.state);

  return [
    {
      installmentsCount,
      installmentsAmount,
      method: 'CREDIT_CARD',
      title: 'Cartão de Crédito',
    },
    {
      method: 'BOLETO',
      title: 'Boleto',
    },
  ];
}

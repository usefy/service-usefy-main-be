import { Request } from 'express';

import {
  createBankaccountService,
  deleteBankaccountService,
  readBankaccountService,
  readAllBankaccountService,
} from '../services/bankaccountService';

export async function allBankaccountsGet(req: Request) {
  return readAllBankaccountService(req.state);
}

export async function bankaccountPost(req: Request) {
  return createBankaccountService(req.body, req.state);
}

export async function bankaccountGet(req: Request) {
  return readBankaccountService(req.params, req.state);
}

export async function bankaccountDelete(req: Request) {
  return deleteBankaccountService(req.params, req.state);
}

import { Request } from 'express';
import { statementsService, futureStatementsService, detailStatementsService } from '../services/statementsService';

export async function statementsGet(req: Request) {
  return statementsService(req.param, req.state);
}

export async function futureStatementsGet(req: Request) {
  return futureStatementsService(req.param, req.state);
}

export async function statementsDetailGet(req: Request) {
  return detailStatementsService(req.param, req.state);
}

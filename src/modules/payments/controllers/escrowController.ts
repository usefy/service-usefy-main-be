import { Request, Response } from 'express';

import { createEscrowService } from '../services/escrowService';

export async function escrowPost(req: Request, res: Response) {
  return createEscrowService(req.body, req.state);
}

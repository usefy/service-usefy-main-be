import { Request, Response } from 'express';

import { readBalanceService } from '../services/balanceService';

export async function balanceGet(req: Request, res: Response) {
  return readBalanceService(req.state);
}

import { Request } from 'express';
import { createTransferService, getTransferService, getAllTransfersService } from '../services/transferService';

export async function transfersPost(req: Request) {
  return createTransferService(req.body, req.state);
}

export async function transfersGetAll(req: Request) {
  return getAllTransfersService(req.state);
}

export async function transfersDetailGet(req: Request) {
  return getTransferService(req.param, req.state);
}

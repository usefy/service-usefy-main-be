import { Request, Response } from 'express';

import {
  createOrderRefundService,
  createOrderService,
  readOrderRefundService,
  readOrderService,
} from '../services/orderService';

export async function orderGet(req: Request, res: Response) {
  return readOrderService(req.params, req.state);
}

export async function orderPost(req: Request, res: Response) {
  return createOrderService(req.body, req.state);
}

export async function orderGetRefund(req: Request, res: Response) {
  return readOrderRefundService(req.params, req.state);
}

export async function orderPostRefund(req: Request, res: Response) {
  return createOrderRefundService(req.body, req.state);
}

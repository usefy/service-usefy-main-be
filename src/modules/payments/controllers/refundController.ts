import { Request, Response } from 'express';

import { readRefundService } from '../services/refundService';

export async function refundGet(req: Request, res: Response) {
  return readRefundService(req.params, req.state);
}

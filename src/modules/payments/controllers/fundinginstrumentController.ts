import { Request, Response } from 'express';

import {
  createFundinginstrumentService,
  deleteFundinginstrumentService,
  getFundinginstrumentService,
} from '../services/fundinginstrumentService';

export async function fundinginstrumentGet(req: Request) {
  return getFundinginstrumentService(req.state);
}

export async function fundinginstrumentPost(req: Request, res: Response) {
  return createFundinginstrumentService(req.body, req.state);
}

export async function fundinginstrumentDelete(req: Request, res: Response) {
  return deleteFundinginstrumentService(req.params, req.state);
}

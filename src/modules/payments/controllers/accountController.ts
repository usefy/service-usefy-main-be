import { Request, Response } from 'express';

import { createAccountService, getAccountService } from '../services/accountService';

export async function accountPost(req: Request, res: Response) {
  return createAccountService(req.state);
}

export async function accountGet(req: Request) {
  return getAccountService(req.state);
}

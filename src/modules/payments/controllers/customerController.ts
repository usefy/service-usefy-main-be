import { Request } from 'express';

import {
  createCustomerService,
  getCustomerService,
} from '../services/customerService';

export async function customerGet(req: Request) {
  return getCustomerService(req.state);
}

export async function customerPost(req: Request) {
  return createCustomerService(req.body, req.state);
}

import * as _ from 'lodash';
import moment from 'moment';

import { State } from '../../../models';
import { accountTable } from '../table/accountTable';
import { AccountDocument } from '../models/Account';

export const accountRepo = {
  validate: document => accountTable.validate(document),

  countAll: (state: State) => accountTable.countAll(state),

  get: async (key, state: State) => accountTable.get(key, state),

  getByUid: async (uid: string, state: State) => accountTable.getByUid(uid, state),

  create: async (document: AccountDocument, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return accountTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  update: async (key, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return accountTable.update(
      key,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  delete: (key, state: State) => accountTable.delete(key, state),

  deleteAll: state => accountTable.deleteAll(['id', 'uid'], state),
};

import * as _ from 'lodash';
import moment from 'moment';

import { State } from '../../../models';
import { requestLoggerTable } from '../table/requestLoggerTable';

export const requestLoggerRepo = {
  validate: document => requestLoggerTable.validate(document),

  countAll: (state: State) => requestLoggerTable.countAll(state),

  get: async (key, state: State) => requestLoggerTable.get(key, state),

  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return requestLoggerTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  update: async (key, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return requestLoggerTable.update(
      key,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  updateOrCreate: async (key, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    const existentDocument = requestLoggerTable.get(key, state);

    if (existentDocument) {
      return requestLoggerRepo.update(key, document, state);
    }

    return requestLoggerRepo.create(document, state);
  },

  delete: (key, state: State) => requestLoggerTable.delete(key, state),

  deleteAll: state => requestLoggerTable.deleteAll(['id'], state),
};

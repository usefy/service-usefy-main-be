import * as _ from 'lodash';
import moment from 'moment';

import { State } from '../../../models';
import { customerTable } from '../table/customerTable';
import { CustomerDocument } from '../models/Customer';

export const customerRepo = {
  validate: document => customerTable.validate(document),

  countAll: (state: State) => customerTable.countAll(state),

  getByUid: async (uid: string, state: State) => customerTable.getByUid(uid, state),

  create: async (document: CustomerDocument, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return customerTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  delete: (key, state: State) => customerTable.delete(key, state),

  deleteAll: state =>
    customerTable.deleteAll(['id', 'uid'], state),
};

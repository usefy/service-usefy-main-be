import * as _ from 'lodash';
import moment from 'moment';

import { State } from '../../../models';
import { paymentTable } from '../table/paymentTable';
import { PaymentDocument } from '../models/Payment';

export const paymentRepo = {
  validate: document => paymentTable.validate(document),

  countAll: (state: State) => paymentTable.countAll(state),

  get: async (key, state: State) => paymentTable.get(key, state),

  getByUid: async (uid: string, state: State) => paymentTable.getByUid(uid, state),

  getByOrderId: async (uid: string, state: State) => paymentTable.getByOrderId(uid, state),

  getById: async (id: string, state: State) => paymentTable.getById(id, state),

  create: async (document: PaymentDocument, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return paymentTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  update: async (key: { id: string, uid: string }, document: Partial<PaymentDocument>, state: State) => {
    const documentCopy = _.cloneDeep(document);

    return paymentTable.update(
      key,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  delete: (key, state: State) => paymentTable.delete(key, state),

  deleteAll: state => paymentTable.deleteAll(['id', 'moipId'], state),
};

import * as _ from 'lodash';
import moment from 'moment';

import { State } from '../../../models';
import { moipBankAccountTable } from '../table/moipBankAccountTable';

export const moipBankAccountRepo = {
  validate: document => moipBankAccountTable.validate(document),

  countAll: (state: State) => moipBankAccountTable.countAll(state),

  get: async (key, state: State) => moipBankAccountTable.get(key, state),

  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return moipBankAccountTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  update: async (key, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return moipBankAccountTable.update(
      key,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  updateOrCreate: async (key, document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    const existentDocument = moipBankAccountTable.get(key, state);

    if (existentDocument) {
      return moipBankAccountRepo.update(key, document, state);
    }

    return moipBankAccountRepo.create(document, state);
  },

  delete: (key, state: State) => moipBankAccountTable.delete(key, state),

  deleteAll: state => moipBankAccountTable.deleteAll(['id', 'moipId'], state),
};

import * as _ from 'lodash';
import moment from 'moment';

import { State } from '../../../models';
import { orderTable as moipOrderTable } from '../table/orderTable';
import { OrderDocument } from '../models/Orders';
import { OrderStatus } from '../../checkout/models/order';
import { ordersTable, OrderProps } from '../../checkout/tables/ordersTable';

export const orderRepo = {
  validate: (document: OrderDocument) => moipOrderTable.validate(document),

  countAll: (state: State) => moipOrderTable.countAll(state),

  get: async (key, state: State) => moipOrderTable.get(key, state),

  getById: async (id: string, state: State) => moipOrderTable.getById(id, state),

  getByOrderId: async (orderId: string, state: State) => moipOrderTable.getByOrderId(orderId, state),

  getOrdersByNotReceivedByUpdatedAt: async (updatedAtStart: string, updatedAtEnd: string, state: State) =>
    ordersTable.getOrdersByStatusUpdatedAt(
      updatedAtStart,
      updatedAtEnd,
      OrderStatus.delivered,
      state,
    ),

  getOrdersByNotDeliveredByUpdatedAt: async (updatedAtStart: string, updatedAtEnd: string, state: State) =>
    ordersTable.getOrdersByStatusUpdatedAt(
      updatedAtStart,
      updatedAtEnd,
      OrderStatus.awaitShipment,
      state,
    ),

  create: async (document: OrderDocument, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return moipOrderTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  update: async (uid: string, document: OrderDocument, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return ordersTable.update(
      uid,
      _.merge({}, documentCopy, {
        updatedAt: moment().toISOString(),
      }),
      state,
    );
  },

  delete: (key, state: State) => moipOrderTable.delete(key, state),

  deleteAll: state =>
    moipOrderTable.deleteAll(['id', 'uid'], state),
};

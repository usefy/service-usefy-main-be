import * as _ from 'lodash';
import moment from 'moment';

import { State } from '../../../models';
import { customersFundingInstrument } from '../table/customerFundingInstrumentTable';
import { CustomersFundingInstrumentDocument } from '../models/Customer';

export const customersFundingInstrumentRepo = {
  validate: document => customersFundingInstrument.validate(document),

  countAll: (state: State) => customersFundingInstrument.countAll(state),

  getByUid: async (uid: string, state: State) => customersFundingInstrument.getByUid(uid, state),

  create: async (document: CustomersFundingInstrumentDocument, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return customersFundingInstrument.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  delete: (key, state: State) => customersFundingInstrument.delete(key, state),

  deleteAll: state =>
    customersFundingInstrument.deleteAll(['id', 'uid'], state),
};

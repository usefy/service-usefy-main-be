import { State } from '../../../../../models';
import { errors } from '../../../../../errors';
import { moip } from './moipClient';

export const moipReadRefund = async (
  refundId: string,
  state: State,
): Promise<any> => {
  try {
    const { body: payload } = await moip.refund.get(refundId);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on read refund');
    throw errors.badGateway('Error on read refund', { refundId });
  }
};

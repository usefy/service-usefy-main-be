import axios from 'axios';

import { State } from '../../../../../models';
import { config } from '../../../../../config';
import { errors } from '../../../../../errors';

const remote = axios.create({
  baseURL: config.moip.baseUrl,
  headers: {
    Accept: 'application/json',
    'User-Agent': `Usefy/${config.version.apiVersion}`,
  },
});

export interface MoipBankaccountResponse {
  id: string;
  agencyNumber: number;
  holder: {
    taxDocument: {
      number: string;
      type: string;
    };
    fullname: string;
  };
  accountNumber: number;
  status: string;
  createdAt: string;
  accountCheckNumber: string;
  _links: {
    self: {
      href: string;
    };
  };
  bankName: string;
  type: string;
  agencyCheckNumber: string;
  bankNumber: string;
}

/* params = {
  bankNumber: "237",
  agencyNumber: "12345",
  agencyCheckNumber: "0",
  accountNumber: "12345678",
  accountCheckNumber: "7",
  type: "CHECKING",
  holder: {
      taxDocument: {
          type: "CPF",
          number: "622.134.533-22"
      },
      fullname: "Demo Moip"
  } */
export const moipCreateBankaccount = async (
  id: string,
  accessToken: string,
  params,
  state: State,
): Promise<MoipBankaccountResponse> => {
  try {
    const url = `/accounts/${id}/bankaccounts`;
    const response = await remote.post(url, params, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create bankaccount');
    throw errors.badGateway('Error on create bankaccount', { id });
  }
};

export const moipReadBankaccount = async (
  id: string,
  accessToken: string,
  state: State,
): Promise<MoipBankaccountResponse> => {
  try {
    const url = `/bankaccounts/${id}`;
    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on read bankaccount');
    throw errors.badGateway('Error on read bankaccount', { id });
  }
};

export const moipReadAllBankaccount = async (
  id: string,
  accessToken: string,
  state: State,
): Promise<MoipBankaccountResponse> => {
  try {
    const url = `/accounts/${id}/bankaccounts`;
    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, "Moip: Error on read all user's bankaccount");
    throw errors.badGateway("Error on read all user's  bankaccount", {
      id,
    });
  }
};

export const moipDeleteBankaccount = async (
  id: string,
  accessToken: string,
  state: State,
): Promise<MoipBankaccountResponse> => {
  try {
    const url = `/bankaccounts/${id}`;
    const response = await remote.delete(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on delete bankaccount');
    throw errors.badGateway('Error on delete bankaccount', { id });
  }
};

import { State } from '../../../../../models';
import { MoipPaymentResponse } from './payment';
import { errors } from '../../../../../errors';
import { moip } from './moipClient';

export const moipCreateEscrow = async (
  escrowId: string,
  state: State,
): Promise<MoipPaymentResponse> => {
  try {
    const { body: payload } = await moip.escrow.release(escrowId);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create escrow');
    throw errors.badGateway('Error on create escrow', { escrowId });
  }
};

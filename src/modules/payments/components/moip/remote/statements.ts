import axios from 'axios';
import moment from 'moment';

import { config } from '../../../../../config';
import { State } from '../../../../../models';
import { errors } from '../../../../../errors';

const remote = axios.create({
  baseURL: config.moip.baseUrl,
  headers: {
    Accept: 'application/json',
    'User-Agent': `Usefy/${config.version.apiVersion}`,
  },
});

const dateFormat = 'YYYY-MM-DD';
const format = (date: string | Date | moment.Moment) => moment(date).format(dateFormat);

export interface MoipStatementLine {
  amount: number;
  description: string;
  type: number;
  date: string | Date | moment.Moment;
  _links: {
    self: {
      href: string;
    };
  };
}

export interface MoipStatementsResponse {
  summary: {
    creditSum: number;
    debitSum: number;
  };
  lines: MoipStatementLine[];
}

export const getStatements = async (
  { begin, end, accessToken }: { begin: string | Date | moment.Moment, end: string | Date | moment.Moment, accessToken: string },
  state: State,
): Promise<MoipStatementsResponse> => {
  try {
    const url = `/statements?begin=${format(begin)}&end=${format(end)}`;
    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error get statements');
    throw errors.badGateway('Error get statements', {});
  }
};

export const getFutureStatements = async (
  { begin, end, accessToken }: { begin: string | Date | moment.Moment, end: string | Date | moment.Moment, accessToken: string },
  state: State,
): Promise<MoipStatementsResponse> => {
  try {
    const url = `/statements?begin=${format(begin)}&end=${format(end)}`;

    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error get future statements');
    throw errors.badGateway('Error get future statements', {});
  }
};

export interface MoipStatementDetailEntry {
  description: string;
  external_id: string;
  reschedule: any; // TODO: definir corretamente esse tipo
  scheduledFor: string;
  status: string;
  moipAccount: {
    account: string;
  };
  fees: {
    amount: number;
    type: string;
  }[];
  type: string;
  grossAmount: number;
  moipAccountId: number;
  id: number;
  installment: {
    amount: number;
    number: number;
  };
  references: {
    value: string;
    type: string;
  }[];
  eventId: string;
  createdAt: string;
  updatedAt: string;
}
export interface MoipStatementDetailResponse {
  summary: {
    description: string;
    entrySum: number;
    date: string | Date | moment.Moment;
    entryCount: number;
  };
  entries: MoipStatementDetailEntry[];
  _links: {
    self: {
      href: string;
    };
  };
}

export const getStatementsDetail = async (
  { type, date, accessToken }: { type: string, date: string | Date | moment.Moment, accessToken: string },
  state: State,
): Promise<MoipStatementDetailResponse> => {
  try {
    const url = `/statements/details?type=${type}&date=${format(date)}`;
    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error get statements detail');
    throw errors.badGateway('Error get statements detail', {});
  }
};

export const getFutureStatementsDetail = async (
  { type, date, accessToken }: { type: string, date: string | Date | moment.Moment, accessToken: string },
  state: State,
): Promise<MoipStatementDetailResponse> => {
  try {
    const url = `/futurestatements/details?type=${type}&date=${format(date)}`;
    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error get future statements detail');
    throw errors.badGateway('Error get future statements detail', {});
  }
};

import { State } from '../../../../../models';
import { errors } from '../../../../../errors';
import { moip } from './moipClient';

export interface MoipAccountResponse {
  id: string;
  login: string;
  accessToken: string;
  channelId: string;
  type: string;
  transparentAccount: boolean;
  email: {
    address?: string;
    confirmed: boolean;
  };
  person: {
    name?: string;
    lastName?: string;
    birthDate?: Date;
    taxDocument?: {
      number?: string;
      type?: string;
    };
    phone?: {
      number: number;
      areaCode: number;
      countryCode: number;
      verified: boolean;
      phoneType: string;
    };
    address?: {
      street: string;
      streetNumber: number;
      complement: string;
      district: string;
      zipCode: string;
      city: string;
      state: string;
      country: string;
    };
    identityDocument?: {
      number: string;
      issuer: string;
      issueDate: string;
      type: string;
    };
  };
  createdAt: string;
  _links: {
    self: {
      href: string;
      title: string;
    };
  };
}

/* params = {
  email: {
    address: 'dev.moip@labs.moip.com.br',
  },
  person: {
    name: 'Runscope',
    lastName: 'Random 9123',
    taxDocument: {
      type: 'CPF',
      number: '123.456.798-91',
    },
    identityDocument: {
      type: 'RG',
      number: '434322344',
      issuer: 'SSP',
      issueDate: '2000-12-12',
    },
    birthDate: '1990-01-01',
    phone: {
      countryCode: '55',
      areaCode: '11',
      number: '965213244',
    },
    address: {
      street: 'Av. Brigadeiro Faria Lima',
      streetNumber: '2927',
      district: 'Itaim',
      zipCode: '01234-000',
      city: 'São Paulo',
      state: 'SP',
      country: 'BRA',
    },
  },
  type: 'MERCHANT',
  transparentAccount: false,
} */
export interface CreateAccountParams {
  email: {
    address: string;
  };
  person: {
    name: string;
    lastName: string;
    taxDocument: {
      type: string;
      number: string;
    };
    identityDocument: {
      type: string;
      number: string;
      issuer: string;
      issueDate: string;
    };
    birthDate: string;
    phone: {
      countryCode: string;
      areaCode: string;
      number: string;
    };
    address: {
      street: string;
      streetNumber: string;
      district: string;
      zipCode: string;
      city: string;
      state: string;
      country: string;
    };
  };
  type: string;
  transparentAccount: boolean;
}

export const moipCreateAccount = async (
  params: CreateAccountParams,
  state: State,
): Promise<MoipAccountResponse> => {
  try {
    const { body: payload } = await moip.account.create(params);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create account');
    throw errors.badGateway('Error on create account', {});
  }
};

export const moipGetAccount = async (
  params: {
    id: string,
  },
  state: State,
): Promise<MoipAccountResponse> => {
  try {
    const response = await moip.account.getOne(params.id);
    return response.body;
  } catch (error) {
    state.logger.warn({ error }, 'Moip: Error on get account');
    throw errors.badGateway('Error on get account', { id: params.id });
  }
};

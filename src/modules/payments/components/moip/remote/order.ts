import v4 from 'uuid/v4';

import { State } from '../../../../../models';
import { config } from '../../../../../config';
import { orderRepo } from '../../../../payments/repositories/orderRepo';
import { errors } from '../../../../../errors';
import { moip } from './moipClient';

export interface MoipOrderResponse {
  id: string;
  ownId: string;
  status: string;
  platform: string;
  createdAt: string;
  updatedAt: string;
  amount: {
    paid: number;
    total: number;
    fees: number;
    refunds: number;
    liquid: number;
    otherReceivers: number;
    currency: string;
    subtotals: {
      shipping: number;
      addition: number;
      discount: number;
      items: number;
    };
  };
  items: [
    {
      price: number;
      detail: string;
      quantity: number;
      product: string;
      category: string;
    }
  ];
  addresses: [
    {
      streetNumber: string;
      street: string;
      city: string;
      complement: string;
      district: string;
      zipCode: string;
      state: string;
      type: string;
      country: string;
    }
  ];
  customer: {
    id: string;
    ownId: string;
    fullname: string;
    createdAt: string;
    birthDate: string;
    email: string;
    phone: {
      countryCode: string;
      areaCode: string;
      number: string;
    };
    taxDocument: {
      type: string;
      number: string;
    };
    addresses: [
      {
        streetNumber: string;
        street: string;
        city: string;
        complement: string;
        district: string;
        zipCode: string;
        state: string;
        type: string;
        country: string;
      }
    ];
    shippingAddress: {
      zipCode: string;
      street: string;
      streetNumber: string;
      complement: string;
      city: string;
      district: string;
      state: string;
      country: string;
    };
    _links: {
      self: {
        href: string;
      };
      hostedAccount: {
        redirectHref: string;
      };
    };
  };
  payments: [];
  escrows: [];
  refunds: [];
  entries: [];
  events: [
    {
      type: string;
      createdAt: string;
      description: string;
    }
  ];
  receivers: [
    {
      moipAccount: {
        id: string;
        login: string;
        fullname: string;
      };
      type: string;
      amount: {
        total: number;
        fees: number;
        refunds: number;
      };
      feePayor: boolean;
    },
    {
      moipAccount: {
        id: string;
        login: string;
        fullname: string;
      };
      type: string;
      amount: {
        total: number;
        fees: number;
        refunds: number;
      };
      feePayor: boolean;
    }
  ];
  _links: {
    self: {
      href: string;
    };
    checkout: {
      payCheckout: {
        redirectHref: string;
      };
      payCreditCard: {
        redirectHref: string;
      };
      payBoleto: {
        redirectHref: string;
      };
      payOnlineBankDebitItau: {
        redirectHref: string;
      };
    };
  };
}

export interface CreateOrderParams {
  own_id: string;
  amount: {
    currency: string;
    subtotals?: {
      addition?: number,
      shipping?: number,
      discount?: number,
    };
  };
  items: {
    product: string;
    quantity: number;
    detail?: string;
    price: number;
  }[];
  customer: {
    id: string;
  };
  receivers: {
    type: string;
    feePayor: boolean,
    moipAccount: {
      id: string;
    };
    amount: {
      fixed: number,
    }
  }[];

}
export const moipCreateOrder = async (
  params,
  state: State,
): Promise<MoipOrderResponse> => {
  try {
    const { body: payload } = await moip.order.create(params);

    // save order on table
    await orderRepo.create(
      {
        uid: v4(),
        id: payload.id,
        orderId: payload.ownId,
        amount: JSON.stringify(payload.amount),
        linksSelf: payload._links.self.href,
        linksCheckout: JSON.stringify(payload._links.checkout),
      },
      state,
    );
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create order');
    throw errors.badGateway('Error on create order', {});
  }
};

export const moipReadOrder = async (
  orderId: string,
  state: State,
): Promise<MoipOrderResponse> => {
  try {
    const { body: payload } = await moip.order.getOne(orderId);

    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on read order');
    throw errors.badGateway('Error on read order', { orderId });
  }
};

export interface MoipOrderRefundResponse {
  id: string;
  status: string;
  events: [
    {
      type: string;
      createdAt: string;
    },
    {
      type: string;
      createdAt: string;
    }
  ];
  amount: {
    total: number;
    gross: number;
    fees: number;
    currency: string;
  };
  type: string;
  refundingInstrument: {
    creditCard: {
      brand: string;
      first6: string;
      last4: string;
      store: boolean;
    };
    method: string;
  };
  createdAt: string;
  _links: {
    self: {
      href: string;
    };
    order: {
      href: string;
      title: string;
    };
    payment: {
      href: string;
      title: string;
    };
  };
}

export const moipCreateOrderRefund = async (
  orderId: string,
  state: State,
): Promise<MoipOrderRefundResponse> => {
  try {
    const { body: payload } = await moip.order.refunds.create(orderId);

    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create order refund');
    throw errors.badGateway('Error on create order refund', { orderId });
  }
};

export const moipReadOrderRefund = async (
  orderId: string,
  state: State,
): Promise<MoipOrderRefundResponse> => {
  try {
    const { body: payload } = await moip.order.refunds.get(orderId);

    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on read order refund');
    throw errors.badGateway('Error on read order refund', { orderId });
  }
};

import { State } from '../../../../../models';
import { customerRepo } from '../../../repositories/customerRepo';
import { errors } from '../../../../../errors';
import { moip } from './moipClient';

export interface MoipCustomerResponse {
  id: string;
  ownId: string;
  fullname: string;
  createdAt: string;
  birthDate: string;
  email: string;
  phone: {
    countryCode: string;
    areaCode: string;
    number: string;
  };
  taxDocument: {
    type: string;
    number: string;
  };
  shippingAddress: {
    zipCode: string;
    street: string;
    streetNumber: string;
    complement: string;
    city: string;
    district: string;
    state: string;
    country: string;
  };
  _links: {
    self: {
      href: string;
    };
    hostedAccount: {
      redirectHref: string;
    };
  };
}

export interface CreateCustomerParams {
  ownId: string;
  fullname: string;
  firstName?: string;
  lastName?: string;
  email: string;
  birthDate: string;
  taxDocument: {
    type: string;
    number: string;
  };
  phone: {
    countryCode: string;
    areaCode: string;
    number: string;
  };
  shippingAddress: {
    city: string;
    complement: string;
    district: string;
    street: string;
    streetNumber: string;
    zipCode: string;
    state: string;
    country: string;
  };
}

/* params = {
  ownId: '1521656695',
  fullname: 'Jose Silva',
  email: 'jose_silva0@email.com',
  birthDate: '1988-12-30',
  taxDocument: {
    type: 'CPF',
    number: '22222222222',
  },
  phone: {
    countryCode: '55',
    areaCode: '11',
    number: '66778899',
  },
  shippingAddress: {
    city: 'Sao Paulo',
    complement: '8',
    district: 'Itaim',
    street: 'Avenida Faria Lima',
    streetNumber: '2927',
    zipCode: '01234000',
    state: 'SP',
    country: 'BRA',
  },
} */
export const moipCreateCustomer = async (
  params: CreateCustomerParams,
  state: State,
): Promise<MoipCustomerResponse> => {
  try {
    const { body: payload } = await moip.customer.create(params);

    await customerRepo.create(
      {
        id: payload.id,
        uid: payload.ownId,
        linksSelf: payload._links.self.href || '',
      },
      state,
    );

    return payload;
  } catch (err) {
    const { errors } = err.error;
    const errorsCode: string[] = errors.map((e: any) => e.code);

    if (errorsCode.indexOf('CUS-008') >= 0) {
      const { body: allClients } = await moip.customer.getAll();
      const customer = allClients.customers.find((client: any) => client.ownId === params.ownId);

      if (customer) {
        await customerRepo.create(
          {
            id: customer.id,
            uid: customer.ownId,
            linksSelf: customer._links.self.href || '',
          },
          state,
        );

        return customer;
      }
    }

    state.logger.warn({ err }, 'Moip: Error on create customer');
    throw errors.badGateway('Error on create a customer', {});
  }
};

export const moipReadCustomer = async (
  customerId: string,
  state: State,
): Promise<MoipCustomerResponse> => {
  try {
    const { body: payload } = await moip.customer.getOne(customerId);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on read customer');
    throw errors.badGateway('Error on read customer', { customerId });
  }
};

export interface MoipFuncinginstrumentResponse {
  creditCard: {
    id: string;
    brand: string;
    first6: string;
    last4: string;
    store: boolean;
  };
  card: {
    brand: string;
    store: boolean;
  };
  method: string;
}

/* params = {
  method: "CREDIT_CARD",
  creditCard: {
      expirationMonth: "05",
      expirationYear: "18",
      number: "4012001037141112",
      cvc: "123",
      holder: {
          fullname: "Jose Portador da Silva",
          birthdate: "1988-12-30",
          taxDocument: {
              type: "CPF",
              number: "33333333333"
          },
          phone: {
              countryCode: "55",
              areaCode: "11",
              number: "66778899"
          }
      }

  } */
export const moipCreateFundinginstrument = async (
  customerId: string,
  params,
  state: State,
): Promise<MoipFuncinginstrumentResponse> => {
  try {
    const { body: payload } = await moip.customer.createCreditCard(customerId, params);

    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create fundinginstrument');
    throw errors.badGateway('Error on create a credit card', { customerId });
  }
};

export const moipDeleteFundinginstrument = async (
  creditcardId: string,
  state: State,
): Promise<any> => {
  try {
    const { body: payload } = await moip.customer.removeCreditCard(creditcardId);

    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on delete fundinginstrument');
    throw errors.badGateway('Error on delete a credit card', { creditcardId });
  }
};

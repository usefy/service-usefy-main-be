import axios from 'axios';
import moment from 'moment';

import { config } from '../../../../../config';
import { State } from '../../../../../models';
import { errors } from '../../../../../errors';

const remote = axios.create({
  baseURL: config.moip.baseUrl,
  headers: {
    Accept: 'application/json',
    'User-Agent': `Usefy/${config.version.apiVersion}`,
  },
});

export interface TransferResponse {
  updatedAt: string | Date | moment.Moment;
  fee: number;
  amount: 500;
  id: string;
  transferInstrument: {
    method: string;
    bankAccount: {
      id: string;
      agencyNumber: string;
      holder: {
        taxDocument: {
          number: string;
          type: string;
        },
        fullname: string;
      };
      accountNumber: string;
      accountCheckNumber: string;
      bankName: string;
      type: string;
      agencyCheckNumber: string;
      bankNumber: string;
    }
  };
  status: string;
  createdAt: string;
  events: {
    createdAt: string;
    description: string;
    type: string;
  }[];
  entries: {
    external_id: string;
    scheduledFor: string;
    status: string;
    moipAccount: {
      account: string;
    },
    fees: [
      {
        amount: 0,
        type: string;
      }
    ],
    type: string;
    grossAmount: -500,
    moipAccountId: 9125,
    updatedAt: string;
    id: 197194,
    installment: {
      amount: 1,
      number: 1,
    },
    references: [
      {
        value: string;
        type: string;
      }
    ],
    eventId: string;
    createdAt: string;
    description: string;
    settledAt: string;
    liquidAmount: -500
  }[];
  _links: {
    self: {
      href: string;
    };
  };
}

export enum TransferType{
  bankAccount = 'BANK_ACCOUNT',
  moipAccount = 'MOIP_ACCOUNT',
}

export const getTransfer = async (
  { id, accessToken }: { id: string, accessToken: string },
  state: State,
): Promise<TransferResponse> => {
  try {
    const url = `/transfers/${id}`;
    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error get transfer detail');
    throw errors.badGateway('Error get transfer detail', {});
  }
};

export const getAllTransfers = async (
  { accessToken }: { accessToken: string },
  state: State,
): Promise<TransferResponse> => {
  try {
    const url = '/transfers';
    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error get transfer');
    throw errors.badGateway('Error get transfer', {});
  }
};

export const createTransfer = async (
  {
    amount,
    bankAccount: {
      id:
      bankAccountId,
    },
    accessToken,
  }: {
    amount: number;
    bankAccount: {
      id: string;
    };
    accessToken: string;
  },
  state: State,
): Promise<TransferResponse> => {
  try {
    const url = '/transfers';
    const data = {
      amount,
      transferInstrument: {
        method: TransferType.bankAccount, // we will note transfer between moip accounts
        bankAccount: {
          id: bankAccountId,
        },
      },
    };
    const response = await remote.post(url, data, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error create transfer');
    throw errors.badGateway('Error create transfer', {});
  }
};

import moipSdkNode from 'moip-sdk-node';

import { config } from '../../../../../config';

export const moip = moipSdkNode({
  accessToken: config.moip.accessToken,
  production: config.moip.production,
});

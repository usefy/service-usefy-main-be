import { State } from '../../../../../models';
import { config } from '../../../../../config';
import { errors } from '../../../../../errors';
import * as WirecardError from '../../../../../errors/WirecardError';
import * as tracking from '../../../../../components/tracking';
import { moip } from './moipClient';

export interface MoipPaymentResponse {
  id: string;
  status: string;
  delayCapture: boolean;
  amount: {
    total: number;
    gross: number;
    fees: number;
    refunds: number;
    liquid: number;
    currency: string;
  };
  installmentCount: number;
  statementDescriptor: string;
  fundingInstrument: {
    creditCard: {
      id: string;
      brand: string;
      first6: string;
      last4: string;
      store: boolean;
      holder: {
        birthdate: string;
        birthDate: string;
        taxDocument: {
          type: string;
          number: string;
        };
        billingAddress: {
          street: string;
          streetNumber: string;
          district: string;
          city: string;
          state: string;
          country: string;
          zipCode: string;
        };
        fullname: string;
      };
    };
    method: string;
  };
  fees: [
    {
      type: string;
      amount: number;
    }
  ];
  escrows: [
    {
      id: string;
    }
  ];
  events: [
    {
      type: string;
      createdAt: string;
    },
    {
      type: string;
      createdAt: string;
    }
  ];
  receivers: [
    {
      moipAccount: {
        id: string;
        login: string;
        fullname: string;
      };
      type: string;
      amount: {
        total: number;
        refunds: number;
      };
    }
  ];
  device: {
    userAgent: string;
    ip: string;
    geolocation: {
      latitude: number;
      longitude: number;
    };
    fingerprint: string;
  };
  _links: {
    self: {
      href: string;
    };
    order: {
      href: string;
      title: string;
    };
  };
  createdAt: string;
  updatedAt: string;
}

export const moipCreatePayment = async (
  orderId: string,
  params,
  state: State,
): Promise<MoipPaymentResponse> => {
  try {
    const { body: payload } = await moip.payment.create(orderId, params);
    return payload;
  } catch (err) {
    const wirecardError = WirecardError.find(err);

    const message = wirecardError ? wirecardError.message : '';

    await tracking.send(
      {
        id: state.user.uid,
        name: config.customerio.track.checkout.error.wirecard,
        data: {
          error: {
            message,
          },
        },
      },
      state,
    );

    state.logger.warn({ err }, 'Moip: Error on create payment');
    throw errors.badGateway('Error on create a payment', { orderId });
  }
};

export const moipReadPayment = async (
  paymentId: string,
  state: State,
): Promise<MoipPaymentResponse> => {
  try {
    const { body: payload } = await moip.payment.getOne(paymentId);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on read payment');
    throw errors.badGateway('Error on read payment', { paymentId });
  }
};

export const moipCreatePaymentCapture = async (
  paymentId: string,
  state: State,
): Promise<MoipPaymentResponse> => {
  try {
    const { body: payload } = await moip.payment.preAuthorizationCapture(paymentId);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create payment capture');
    throw errors.badGateway('Error on create payment capture', { paymentId });
  }
};

export const moipCreatePaymentVoid = async (
  paymentId: string,
  state: State,
): Promise<MoipPaymentResponse> => {
  try {
    const { body: payload } = await moip.payment.preAuthorizationCancel(paymentId);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create payment void');
    throw errors.badGateway('Error on create payment void', { paymentId });
  }
};

export const moipCreatePaymentRefund = async (
  paymentId: string,
  state: State,
): Promise<MoipPaymentResponse> => {
  try {
    const { body: payload } = await moip.payment.refund(paymentId);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on create payment refund');
    throw errors.badGateway('Error on create payment refund', { paymentId });
  }
};

export const moipReadPaymentRefund = async (
  paymentId: string,
  state: State,
): Promise<MoipPaymentResponse> => {
  try {
    const { body: payload } = await moip.payment.refunds.get(paymentId);
    return payload;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on read payment refund');
    throw errors.badGateway('Error on read payment refund', { paymentId });
  }
};

import axios from 'axios';

import { State } from '../../../../../models';
import { config } from '../../../../../config';
import { errors } from '../../../../../errors';

const remote = axios.create({
  baseURL: config.moip.baseUrl,
  headers: {
    Accept: 'application/json',
    'User-Agent': `Usefy/${config.version.apiVersion}`,
  },
});

export const moipReadBalance = async (
  state: State,
  accessToken: string,
): Promise<any> => {
  try {
    const url = '/balances';
    const response = await remote.get(url, {
      headers: {
        Authorization: `OAuth ${accessToken}`,
      },
    });

    return response.data;
  } catch (err) {
    state.logger.warn({ err }, 'Moip: Error on read balance');
    throw errors.badGateway('Error on read balance', {});
  }
};

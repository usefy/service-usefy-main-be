export interface MoipCustomerTaxDocument {
  number?: string;
  type?: string;
}

//         "taxDocument": {
//             "number": "742.520.863-61",
//             "type": "CPF"
//         },

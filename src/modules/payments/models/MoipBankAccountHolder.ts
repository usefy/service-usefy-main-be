import { MoipPersonTaxDocument } from './MoipPersonTaxDocument';

export interface MoipBankAccountHolder {
  taxDocument?: MoipPersonTaxDocument;
  fullname?: string;
}

//     "holder": {
//         "taxDocument": {
//             "number": "622.134.533-22",
//             "type": "CPF"
//         },
//         "fullname": "Demo Moip"
//     },

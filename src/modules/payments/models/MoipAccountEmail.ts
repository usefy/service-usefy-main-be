export interface MoipAccountEmail {
  address?: string;
  confirmed: boolean;
}

//     "email": {
//         "address": "dobxerg0a8@labs.moip.com.br",
//         "confirmed": false
//     },

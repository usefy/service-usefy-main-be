
export interface OrderDocument {
  id: string;
  uid: string;
  orderId: string;
  amount: string;
  createdAt?: Date;
  updatedAt?: Date;
  payments?: string;
  escrows?: string;
  linksSelf?: string;
  linksCheckout?: string;
}

export enum OrderStatus {
  created = 'CREATED',
  waiting = 'WAITING',
  paid = 'PAID',
  notPaid = 'NOT_PAID',
  reverted= 'REVERTED',
}

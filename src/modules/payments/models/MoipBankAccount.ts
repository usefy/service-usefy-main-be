import { MoipBankAccountHolder } from './MoipBankAccountHolder';

export interface MoipBankAccount {
  moipId: string;
  agencyNumber: string;
  holder?: MoipBankAccountHolder;
  accountNumber: string;
  status: string;
  createdAt?: Date;
  accountCheckNumber?: string;
  linksSelf?: string;
  bankName?: string;
  type?: string;
  agencyCheckNumber?: string;
  bankNumber?: string;
}

// Request
// {
//     "bankNumber": "237",
//     "agencyNumber": "12345",
//     "agencyCheckNumber": "0",
//     "accountNumber": "12345678",
//     "accountCheckNumber": "7",
//     "type": "CHECKING",
//     "holder": {
//         "taxDocument": {
//             "type": "CPF",
//             "number": "622.134.533-22"
//         },
//         "fullname": "Demo Moip"
//     }
// }

// Response
// {
//     "id": "BKA-DN6831N81J7K",
//     "agencyNumber": 12345,
//     "holder": {
//         "taxDocument": {
//             "number": "622.134.533-22",
//             "type": "CPF"
//         },
//         "fullname": "Demo Moip"
//     },
//     "accountNumber": 12345678,
//     "status": "NOT_VERIFIED",
//     "createdAt": "2015-10-29T15:50:27.746-02:00",
//     "accountCheckNumber": "7",
//     "_links": {
//         "self": {
//             "href": "https://sandbox.moip.com.br/v2/accounts/MPA-8307EF11B83E/bankaccounts"
//         }
//     },
//     "bankName": "BANCO BRADESCO S.A.",
//     "type": "CHECKING",
//     "agencyCheckNumber": "0",
//     "bankNumber": "237"
// }

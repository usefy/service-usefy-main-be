import { MoipCustomerAddress } from './MoipCustomerAddress';
import { MoipCustomerPhone } from './MoipCustomerPhone';
import { MoipCustomerTaxDocument } from './MoipCustomerTaxDocument';

export interface CustomerDocument {
  id: string;
  uid: string;
  createdAt?: Date;
  moipAccountId?: string;
  linksSelf?: string;
}

export interface CustomersFundingInstrumentDocument {
  id: string;
  uid: string;
  creditCard: string;
  card: string;
}

// Request
// {
//   "ownId": "meu_id_sandbox_1231234",
//   "fullname": "Jose Silva",
//   "email": "jose_silva0@email.com",
//   "birthDate": "1988-12-30",
//   "taxDocument": {
//     "type": "CPF",
//     "number": "22222222222"
//   },
//   "phone": {
//     "countryCode": "55",
//     "areaCode": "11",
//     "number": "66778899"
//   },
//   "shippingAddress": {
//     "city": "Sao Paulo",
//     "complement": "8",
//     "district": "Itaim",
//     "street": "Avenida Faria Lima",
//     "streetNumber": "2927",
//     "zipCode": "01234000",
//     "state": "SP",
//     "country": "BRA"
//   }
// }

// Response
// {
//   "id": "CUS-Y6L4AGQN8HKQ",
//   "ownId": "meu_id_sandbox_1231234",
//   "fullname": "Jose Silva",
//   "createdAt": "2015-01-14T11:28:22-0200",
//   "birthDate": "1988-12-30",
//   "email": "jose_silva0@email.com",
//   "phone": {
//     "countryCode": "55",
//     "areaCode": "11",
//     "number": "66778899"
//   },
//   "taxDocument": {
//     "type": "CPF",
//     "number": "22222222222"
//   },
//   "shippingAddress": {
//     "zipCode": "01234000",
//     "street": "Avenida Faria Lima",
//     "streetNumber": "2927",
//     "complement": "8",
//     "city": "Sao Paulo",
//     "district": "Itaim",
//     "state": "SP",
//     "country": "BRA"
//   },
//   "moipAccount": {
//     "id": "MPA-M1M01PACC0NT"
//   },
//   "_links": {
//     "self": {
//       "href": "https://sandbox.moip.com.br/v2/customers/CUS-Y6L4AGQN8HKQ"
//     }
//   }
// }

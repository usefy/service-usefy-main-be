export interface MoipCustomerPhone {
  number: number;
  areaCode: number;
  countryCode: number;
}

//         "phone": {
//             "number": "965213244",
//             "areaCode": "11",
//             "countryCode": "55"
//         },

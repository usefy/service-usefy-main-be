export interface MoipPayment {
  moipId: string;
  status: string;
  amount: string;
  installmentCount: number;
  fees?: Text;
  linksSelf: string;
  updatedAt?: Date;
  createdAt?: Date;
}

export interface PaymentDocument {
  id: string;
  uid: string;
  orderId: string;
  moipOrderId: string;
  amount: string;
  installmentCount: number;
  method: string;
  paymentStatus: string;
  linksSelf?: string;
  updatedAt?: Date;
  createdAt?: Date;
}

export enum MoipPaymentStatus {
  created = 'CREATED',
  waiting = 'WAITING',
  inAnalysis = 'IN_ANALYSIS',
  preAuthorized = 'PRE_AUTHORIZED',
  authorized = 'AUTHORIZED',
  rejected = 'REJECTED',
  canceled = 'CANCELLED',
  refunded = 'REFUNDED',
  reversed = 'REVERSED',
  settled = 'SETTLED',
}

// Request
// {
//   "installmentCount": 1,
//   "fundingInstrument": {
//     "method": "CREDIT_CARD",
//     "creditCard": {
//         "hash": "HhL0kbhfid+jwgj5l6Kt9EPdetDxQN8s7uKUHDYxDC/XoULjzik44rSda3EcWuOcL17Eb8JjWc1JI7gsuwg9P0rJv1mJQx+d3Dv1puQYz1iRjEWWhn" +
//         "B1bw0gTvnnC/05KbWN5M8oTiugmhVK02Rt2gpbcTtpS7VWyacfgesBJFavYYMljYg8p2YGHXkXrMuQiOCeemKLk420d0OTMBba27jDVVJ663HZDrObnjFXJH/4" +
//         "B5irkj+HO5genV+V4PYoLcOESG4nrI3oFAsMGsLLcdJo0NNvkEmJpn0e9GzureKKFYisYU+BEd9EMr/odS0VMvOYRV65HbPTspIkjl2+3Q==",
//       "holder": {
//         "fullname": "Jose Portador da Silva",
//         "birthdate": "1988-12-30",
//         "taxDocument": {
//           "type": "CPF",
//           "number": "33333333333"
//         },
//         "phone": {
//           "countryCode": "55",
//           "areaCode": "11",
//           "number": "66778899"
//         }
//       }
//     }
//   }
// }

// Response
// {
//   "id": "PAY-VZ1HI48256ZX",
//   "status": "IN_ANALYSIS",
//   "amount": {
//     "fees": 187,
//     "refunds": 0,
//     "liquid": 1813,
//     "currency": "BRL",
//     "total": 2000
//   },
//   "installmentCount": 1,
//   "fundingInstrument": {
//     "creditCard": {
//       "id": "CRC-V0AAG27AAFG7",
//       "brand": "MASTERCARD",
//       "first6": "555566",
//       "last4": "8884",
//       "holder": {
//         "birthdate": "30/12/1988",
//         "taxDocument": {
//           "type": "CPF",
//           "number": "33333333333"
//         },
//         "fullname": "Jose Portador da Silva"
//       }
//     },
//     "method": "CREDIT_CARD"
//   },
//   "fees": [
//     {
//       "type": "TRANSACTION",
//       "amount": 187
//     }
//   ],
//   "events": [
//     {
//       "createdAt": "2015-01-14T12:17:50-0200",
//       "type": "PAYMENT.IN_ANALYSIS"
//     },
//     {
//       "createdAt": "2015-01-14T12:17:48-0200",
//       "type": "PAYMENT.CREATED"
//     }
//   ],
//   "_links": {
//     "order": {
//       "title": "ORD-VULX1EWDKXHF",
//       "href": "https://sandbox.moip.com.br/v2/orders/ORD-VULX1EWDKXHF"
//     },
//     "self": {
//       "href": "https://sandbox.moip.com.br/v2/payments/PAY-VZ1HI48256ZX"
//     }
//   },
//   "updatedAt": "2015-01-14T12:17:50-0200",
//   "createdAt": "2015-01-14T12:17:48-0200"
// }

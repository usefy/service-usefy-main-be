export interface MoipOrderItem {
  detail?: string;
  quantity: number;
  price: number;
  product: string;
}

//   "items": [
//     {
//       "detail": "Mais info...",
//       "quantity": 1,
//       "price": 1000,
//       "product": "Descrição do pedido"
//     }
//   ],

export interface RequestLogger {
  response?: Text;
  method?: Text;
  params?: Text;
  query?: Text;
  body?: Text;
  uri?: Text;
  headers?: Text;
}

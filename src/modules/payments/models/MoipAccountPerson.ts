import { MoipPersonAddress } from './MoipPersonAddress';
import { MoipPersonPhone } from './MoipPersonPhone';
import { MoipPersonTaxDocument } from './MoipPersonTaxDocument';

export interface MoipAccountPerson {
  name?: string;
  lastName?: string;
  birthDate?: Date;
  taxDocument?: MoipPersonTaxDocument;
  phone?: MoipPersonPhone;
  address?: MoipPersonAddress;
}

//     "person": {
//         "name": "Runscope",
//         "lastName": "Random 9123",
//         "birthDate": "1990-01-01",
//         "taxDocument": {
//             "number": "742.520.863-61",
//             "type": "CPF"
//         },
//         "phone": {
//             "number": "965213244",
//             "areaCode": "11",
//             "countryCode": "55"
//         },
//         "address": {
//             "street": "Av. Brigadeiro Faria Lima",
//             "streetNumber": "2927",
//             "complement": "APTO 123",
//             "district": "Itaim",
//             "zipCode": "01234000",
//             "city": "São Paulo",
//             "state": "SP",
//             "country": "BRA"
//         }
//     },

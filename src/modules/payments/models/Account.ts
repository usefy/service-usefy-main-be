export interface AccountDocument {
  uid: string;
  id: string;
  accessToken: string;
  channelId: string;
  createdAt?: Date;
  linksSelf?: string;
  login?: string;
}

// Response
// {
//     "id": "MPA-8307EF11B83E",
//     "accessToken": "414442d306f845e4aeda48a8c7786fd0_v2",
//     "channelId": "APP-7KHGFTMYHGLO",
//     "type": "MERCHANT",
//     "transparentAccount": false,
//     "createdAt": "2015-10-29T16:53:25.957Z",
//     "person": {
//         "name": "Runscope",
//         "lastName": "Random 9123",
//         "birthDate": "1990-01-01",
//         "taxDocument": {
//             "number": "742.520.863-61",
//             "type": "CPF"
//         },
//         "phone": {
//             "number": "965213244",
//             "areaCode": "11",
//             "countryCode": "55"
//         },
//         "address": {
//             "street": "Av. Brigadeiro Faria Lima",
//             "streetNumber": "2927",
//             "district": "Itaim",
//             "zipCode": "01234000",
//             "city": "São Paulo",
//             "state": "SP",
//             "country": "BRA"
//         }
//     },
//     "email": {
//         "address": "dobxerg0a8@labs.moip.com.br",
//         "confirmed": false
//     },
//     "_links": {
//         "self": {
//             "href": "https://sandbox.moip.com.br/moipaccounts/MPA-8307EF11B83E"
//         }
//     },
//     "login": "dobxerg0a8@labs.moip.com.br",
//     "externalId": "MPA-8307EF11B83E"
// }

export interface MoipPersonAddress {
  street: string;
  streetNumber: number;
  complement: string;
  district: string;
  zipCode: string;
  city: string;
  state: string;
  country: string;
}

//         "address": {
//             "street": "Av. Brigadeiro Faria Lima",
//             "streetNumber": "2927",
//             "complement": "APTO 123",
//             "district": "Itaim",
//             "zipCode": "01234000",
//             "city": "São Paulo",
//             "state": "SP",
//             "country": "BRA"
//         }

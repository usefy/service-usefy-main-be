import { State } from '../../../models';
import { User } from '../../users/models/User';
import { userRepo } from '../../users/repositories/users';
import { createTransfer, getTransfer, getAllTransfers } from '../components/moip/remote/transfers';
import { getAccountService, getAccount } from './accountService';
import { config } from '../../../config';
import * as tracking from '../../../components/tracking';
import * as WirecardError from '../../../errors/WirecardError';

export const createTransferService = async (body, state: State) => {
  const {
    amount,
    bankAccount: {
      id: banckAccountId,
    },
  } = body;

  state.logger.info({ amount, banckAccountId }, 'Creating transfer');

  const seller: User = await userRepo.getByUserUid(state.user.uid, state);

  try {
    const account = await getAccount(state);

    const transfer = await createTransfer(
      {
        amount,
        accessToken: account.accessToken,
        bankAccount: { id: banckAccountId },
      },
      state,
    );

    await tracking.send(
      {
        id: seller.uid,
        name: config.customerio.track.transfer.seller,
        data: {
          amount: Number(amount / 100),
          seller: {
            displayName: seller.displayName,
          },
        },
      },
      state,
    );

    state.logger.info({ transfer }, 'Transfer created with success.');

    return transfer;
  } catch (err) {
    const wirecardError = WirecardError.find(err);

    const message = wirecardError ? wirecardError.message : '';

    await tracking.send(
      {
        id: seller.uid,
        name: config.customerio.track.transfer.error.wirecard,
        data: {
          seller: {
            displayName: seller.displayName,
          },
          error: {
            message,
          },
        },
      },
      state,
    );

    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error creating transfer');
    throw err;
  }
};

export const getTransferService = async (params, state: State) => {
  const {
    id,
  } = params;

  state.logger.info({ id }, 'Getting transfer');

  try {
    const account = await getAccountService(state);

    const transfer = await getTransfer(
      {
        id,
        accessToken: account.accessToken,
      },
      state,
    );

    state.logger.info({ transfer }, 'Transfer returned with success.');

    return transfer;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error getting transfer');
    throw err;
  }
};

export const getAllTransfersService = async (state: State) => {
  state.logger.info('Getting all transfers');

  try {
    const account = await getAccountService(state);

    const transfers = await getAllTransfers(
      {
        accessToken: account.accessToken,
      },
      state,
    );

    state.logger.info({ transfers }, 'All transfers returned with success.');

    return transfers;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error getting all transfers');
    throw err;
  }
};

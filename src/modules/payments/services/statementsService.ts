import moment from 'moment';
import { State } from '../../../models';
import {
  getFutureStatements,
  getStatements,
  getStatementsDetail,
  getFutureStatementsDetail,
} from '../components/moip';
import { errors } from '../../../errors';
import { getAccountService } from './accountService';

const parseDate = (date: string | Date) => moment(date).isValid();

export async function statementsService(params, state: State) {
  state.logger.info({ params }, 'Get moip statements');
  const { begin, end } = params;

  try {
    // validate dates
    const dates = [begin, end];
    dates
      .map(parseDate)
      .map((v, i) => {
        if (!v) throw errors.badRequest('Invalid Date', { date: dates[i] });
      });

    const account = await getAccountService(state);

    const result = await getStatements({ begin, end, accessToken: account.accessToken }, state);

    state.logger.info({ statements: result }, 'Moip statements returned with success');
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error getting statements');
    throw err;
  }
}

export async function futureStatementsService(params, state: State) {
  state.logger.info({ params }, 'Get moip future statements');
  const { begin, end } = params;

  try {
    // validate dates
    const dates = [begin, end];
    dates
      .map(parseDate)
      .map((v, i) => {
        if (!v) throw errors.badRequest('Invalid Date', { date: dates[i] });
      });

    const account = await getAccountService(state);

    const result = await getFutureStatements({ begin, end, accessToken: account.accessToken }, state);

    state.logger.info({ statements: result }, 'Moip future statements returned with success');
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error getting future statements');
    throw err;
  }
}

export async function detailStatementsService(params, state: State) {
  state.logger.info({ params }, 'Get moip future statements');
  const { type, date } = params;

  try {
    if (!parseDate(date)) {
      throw errors.badRequest('Invalid Date', { date });
    }

    const account = await getAccountService(state);

    let result;

    if (moment(date).isAfter(moment())) {
      // future
      result = await getFutureStatementsDetail({ type, date, accessToken: account.accessToken }, state);
    } else {
      result = await getStatementsDetail({ type, date, accessToken: account.accessToken }, state);
    }

    state.logger.info({ statementDetail: result }, 'Moip statement detail returned with success');
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error getting statement detail');
    throw err;
  }
}

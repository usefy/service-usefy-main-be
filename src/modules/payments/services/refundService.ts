import { errors } from '../../../errors';
import { State } from '../../../models';
import { moipReadRefund } from '../components/moip';

export async function readRefundService(params, state: State) {
  try {
    const result = await moipReadRefund(params.refundId, state);

    state.logger.info({ result }, 'Payment refund read with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error reading payment refund');
    throw errors.badGateway('Error reading payment refund', { err });
  }
}

import * as joi from '@hapi/joi';
import moment from 'moment';
import { get, isNil } from 'lodash';
import { State } from '../../../models';

import * as moip from '../../payments/components/moip';
import { errors } from '../../../errors';
import { accountRepo } from '../repositories/accountRepo';
import { fullProfileService } from '../../users/services/profileService';

export const isUserReady = (user) => {
  const scheme = joi.object().keys({
    email: joi.string().email().required(),
    firstName: joi.string().required(),
    lastName: joi.string().required(),
    taxNumber: joi.string().required(),
    idNumber: joi.string(),
    idIssuer: joi.string(),
    idIssuerDate: joi.string(),
    birthDate: joi.string().required(),
    phone: joi.object().keys({
      countryCode: joi.number().required(),
      areaCode: joi.string().required(),
      phone: joi.string().required(),
    }).required(),
    address: joi.object().keys({
      address: joi.string().required(),
      complement: joi.string(),
      number: joi.string().required(),
      district: joi.string().required(),
      zipcode: joi.string().required(),
      currentCity: joi.string().required(),
      currentState: joi.string().required(),
      country: joi.string().required(),
    }).required(),
  }).required();

  const validation = joi.validate(user, scheme);
  if (validation.error) throw errors.validation(get(validation, 'error.message'), validation.error);
  return user;
};

export const createAccountService = async (state: State) => {
  // get user data
  state.logger.info('Create Payment Account Service');

  const getAccountMoip = await accountRepo.getByUid(
    state.user.uid,
    state,
  );

  if (getAccountMoip) {
    state.logger.info({ account: getAccountMoip }, 'User already has a moip account');

    throw errors.forbidden('User already has a moip account', {});
  }

  const {
    email,
    displayName,
    taxNumber,
    idIssuer,
    idIssuerDate,
    idNumber,
    birthDate,
    phone,
    address,
  } = await fullProfileService(state.user.uid, state);

  const names = displayName.split(' ');

  const payload = {
    email,
    taxNumber,
    idIssuer,
    idIssuerDate,
    idNumber,
    birthDate,
    phone,
    firstName: names[0],
    lastName: names[1],
    address: {
      address: address.address,
      complement: address.address2,
      number: address.number,
      district: address.district,
      zipcode: address.zipcode,
      currentCity: address.currentCity,
      currentState: address.currentState,
      country: address.country,
    },
  };

  // validate user is able to create the account
  try {
    await isUserReady(payload);
  } catch (err) {
    state.logger.info({ err }, 'Error validating user to create a payment account.');
    throw errors.badRequest('Missing data from user to cerate a payment account', {});
  }

  // create moip account

  const moipParams: moip.CreateAccountParams = {
    email: {
      address: payload.email,
    },
    person: {
      name: payload.firstName,
      lastName: payload.lastName,
      taxDocument: {
        type: 'CPF',
        number: payload.taxNumber!,
      },
      identityDocument: {
        type: 'RG',
        number: payload.idNumber!,
        issuer: payload.idIssuer!,
        issueDate: moment(payload.idIssuerDate!, 'DD-MM-YYYY').format('YYYY-MM-DD').toString(),
      },
      birthDate: moment(payload.birthDate!, 'DD-MM-YYYY').format('YYYY-MM-DD'),
      phone: {
        countryCode: payload.phone!.countryCode,
        areaCode: payload.phone!.areaCode,
        number: payload.phone!.phone,
      },
      address: {
        street: payload.address!.address,
        streetNumber: payload.address!.number,
        district: payload.address!.district,
        zipCode: payload.address!.zipcode,
        city: payload.address!.currentCity,
        state: payload.address!.currentState,
        country: payload.address!.country,
      },
    },
    type: 'MERCHANT', // Marketplaces devem utilizar o valor MERCHANT para criar contas para os seus usuários.
    transparentAccount: true,
  };

  try {
    const moipResponse = await moip.moipCreateAccount(moipParams, state);

    // update user after success
    await accountRepo.create(
      {
        uid: state.user.uid,
        id: moipResponse.id,
        accessToken: moipResponse.accessToken,
        channelId: moipResponse.channelId,
        linksSelf: moipResponse._links.self.href,
        login: moipResponse.login,
      },
      state,
    );

    return getAccountService(state);
  } catch (err) {
    state.logger.error({ err }, 'Error creating a transparent account on Moip');
    throw errors.badGateway('Error creating a payment account', {});
  }
};

export const getUserAccount = async (uid: string, state: State) => {
  try {
    const account = await accountRepo.getByUid(uid, state);

    if (isNil(account)) {
      state.logger.info({ uid }, 'Moip account not found in our table.');
      throw errors.notFound('Payment account not found.', {});
    }

    return account;
  } catch (error) {
    state.logger.error({ error }, 'Error user account');
    throw errors.badGateway('Error getting the user account', {});
  }
};

export const getUserAccountService = async (uid: string, state: State) => {
  try {
    const account = await accountRepo.getByUid(uid, state);

    if (isNil(account)) {
      state.logger.info({ uid }, 'Moip account not found in our table.');
      throw errors.notFound('Payment account not found.', {});
    }

    const moipResponse = await moip.moipGetAccount({ id: account.id }, state);

    return moipResponse;
  } catch (err) {
    if (isNil(err.statusCode)) {
      state.logger.error({ err }, 'Error get users account on Moip');
      throw errors.badGateway('Error getting the payment account', {});
    }

    throw err;
  }
};

export const getAccount = async (state: State) => getUserAccount(state.user.uid, state);

export const getAccountService = async (state: State) => getUserAccountService(state.user.uid, state);

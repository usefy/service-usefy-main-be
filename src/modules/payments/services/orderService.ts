import { errors } from '../../../errors';
import { State } from '../../../models';
import {
  moipCreateOrder,
  moipCreateOrderRefund,
  moipReadOrder,
  moipReadOrderRefund,
} from '../components/moip';
import { orderRepo } from '../repositories/orderRepo';

export async function getOrderService(params, state: State) {
  const { orderId, id } = params;
  let order;
  try {
    if (orderId && !id) {
      order = await orderRepo.getByOrderId(orderId, state);
    } else {
      order = await orderRepo.getById(id, state);
    }

    return order;
  } catch (err) {
    state.logger.error({ err }, 'Error getting order.');
    throw err;
  }
}

export async function readOrderService(params, state: State) {
  const { orderId } = params;
  try {
    const result = await moipReadOrder(orderId, state);

    state.logger.info({ result }, 'Payment order read with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error reading payment order');
    throw errors.badGateway('Error reading payment order', { err });
  }
}

export async function createOrderService(params, state: State) {
  try {
    const result = await moipCreateOrder(params, state);

    state.logger.info({ result }, 'Payment order created with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error creating payment order');
    throw errors.badGateway('Error creating payment order', { err });
  }
}

export async function readOrderRefundService(params, state: State) {
  const { orderId } = params;
  try {
    const result = await moipReadOrderRefund(orderId, state);

    state.logger.info({ result }, 'Payment order refund read with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error reading payment order refund');
    throw errors.badGateway('Error reading payment order refund', { err });
  }
}

export async function createOrderRefundService(params, state: State) {
  const { orderId } = params;
  try {
    const result = await moipCreateOrderRefund(orderId, state);

    state.logger.info({ result }, 'Payment order refund created with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error creating payment order refund');
    throw errors.badGateway('Error creating payment order refund', { err });
  }
}

export async function updateOrder(uid: string, params, state: State) {

  try {
    const result = await orderRepo.update(uid, params, state);

    state.logger.info({ result }, 'Updated payment order');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error updating order');
    throw errors.badGateway('Error updating order', { err });
  }
}

import { Dictionary } from 'express-serve-static-core';
import { errors } from '../../../errors';
import { State } from '../../../models';
import {
  moipCreatePayment,
  moipCreatePaymentCapture,
  moipCreatePaymentRefund,
  moipCreatePaymentVoid,
  moipReadPayment,
  moipReadPaymentRefund,
  MoipPaymentResponse,
} from '../components/moip';
import moment from 'moment';
import { config } from '../../../config';
import { paymentRepo } from '../repositories/paymentRepo';
import { getOrderService, updateOrder } from './orderService';

export async function readPaymentService(params, state: State) {
  const { paymentId } = params;
  try {
    const result = await moipReadPayment(paymentId, state);

    state.logger.info({ result }, 'Payment read with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error reading payment');
    throw errors.badGateway('Error reading payment', { err });
  }
}

export interface CreatePaymentParams {
  moipOrderId: string;
  order: any;
}

export enum PaymentMethod {
  creditCard = 'CREDIT_CARD',
  boleto = 'BOLETO',
}

export interface CreditCard {
  hash?: string;
  fullname: string;
  birthdate: string;
  taxDocument: {
    type: 'CPF' | 'CNPJ',
    number: string,
  };
  billingAddress: {
    street: string;
    streetNumber: string;
    district: string;
    city: string;
    state: string;
    country: string;
    zipCode: string;
  };
}

export interface CreatePaymentBody {
  method: PaymentMethod;
  installments?: number;
  creditCard?: CreditCard;
}

export async function createPaymentService(params: CreatePaymentParams | Dictionary<string>, body: CreatePaymentBody, state: State) {
  const { order, moipOrderId } = params;
  const { method, installments, creditCard } = body;
  try {
    // get order
    const orderMoip = await getOrderService({ id: moipOrderId }, state);

    if (!orderMoip) {
      state.logger.info({ moipOrderId, orderMoip }, 'Order not found at create payment');
      throw errors.notFound('Order not found', { orderId: moipOrderId });
    }

    let result: MoipPaymentResponse | null;
    switch (method) {
      case 'BOLETO':
        result = await moipCreatePayment(
          orderMoip!.id,
          {
            installmentCount: 1,
            escrow: {
              description: config.moip.escrowDescription,
            },
            fundingInstrument: {
              method: body.method,
              boleto: {
                expirationDate: moment().add(config.moip.boleto.daysToExpire, 'days').format('YYYY-MM-DD'),
                instructionLines: config.moip.boleto.instructionLines,
                logoUri: config.moip.boleto.logoUri,
              },
            },
          },
          state,
        );
        break;
      case 'CREDIT_CARD':
        result = await moipCreatePayment(
          orderMoip!.id,
          {
            installmentCount: installments,
            escrow: {
              description: config.moip.escrowDescription,
            },
            fundingInstrument: {
              method: body.method,
              creditCard: {
                store: false,
                hash: creditCard.hash,
                holder: {
                  fullname: creditCard.fullname,
                  birthdate: creditCard.birthdate,
                  taxDocument: creditCard.taxDocument,
                  billingAddress: {
                    street: creditCard.billingAddress.street,
                    streetNumber: creditCard.billingAddress.streetNumber,
                    district: creditCard.billingAddress.district,
                    city: creditCard.billingAddress.city,
                    state: creditCard.billingAddress.state,
                    country: 'BRA',
                    zipCode: creditCard.billingAddress.zipCode,
                  },
                },
              },
            },
          },
          state,
        );
        break;

      default:
        state.logger.info({ params, body }, 'Unknown payment method');
        throw errors.badRequest('Unknown payment method', { method: body.method });
    }

    // save payment
    await paymentRepo.create(
      {
        orderId: moipOrderId,
        id: result!.id,
        uid: state.user.uid,
        moipOrderId: orderMoip!.id,
        amount: JSON.stringify(result!.amount),
        installmentCount: result!.installmentCount,
        method: result!.fundingInstrument.method,
        paymentStatus: result!.status,
        linksSelf: result!._links.self.href,
      },
      state,
    );

    await updateOrder(
      order.id,
      {
        ...order,
        escrowId: result.escrows[0].id,
      },
      state,
    );

    state.logger.info({ result }, 'Payment payment created with success');
    return result;
  } catch (err) {

    // this error is already logged
    if ([404, 502].indexOf(err.statusCode) >= 0) {
      throw err;
    }

    state.logger.error({ err }, 'Error creating payment');
    throw errors.badGateway('Error creating payment', { err });
  }
}

export async function getPaymentService({ orderId }, state: State) {
  // get payment
  const payment = await paymentRepo.getByOrderId(orderId, state);

  return payment;
}

export async function updatePaymentService(key: { id: string, uid: string }, document, state: State) {
  try {
    const foundPayment = await paymentRepo.get(key, state);

    if (!foundPayment) {
      state.logger.info({ key, document }, 'Payment not found when updating');
      throw errors.notFound('Payment not found when updating', {});
    }

    const payment = await paymentRepo.update(
      key,
      document,
      state,
    );

    state.logger.info({ payment }, 'Payment updated with success');
    return payment;
  } catch (err) {
    // this error is already logged
    if ([404, 502].findIndex(err.statusCode) >= 0) {
      throw err;
    }

    state.logger.error({ err }, 'Error updating payment');
    throw errors.badGateway('Error updating payment', { err });
  }
}

export async function createPaymentCaptureService(params, state: State) {
  const { paymentId } = params;
  try {
    const result = await moipCreatePaymentCapture(paymentId, state);

    state.logger.info(
      { result },
      'Payment payment capture created with success',
    );
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error creating payment payment capture');
    throw errors.badGateway('Error creating payment payment capture', { err });
  }
}

export async function createPaymentVoidService(params, state: State) {
  const { paymentId } = params;
  try {
    const result = await moipCreatePaymentVoid(paymentId, state);

    state.logger.info({ result }, 'Payment payment void created with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error creating payment payment void');
    throw errors.badGateway('Error creating payment payment void', { err });
  }
}

export async function readPaymentRefundService(params, state: State) {
  const { paymentId } = params;
  try {
    const result = await moipReadPaymentRefund(paymentId, state);

    state.logger.info({ result }, 'Payment payment refund red with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error reading payment payment refund');
    throw errors.badGateway('Error reading payment payment refund', { err });
  }
}

export async function createPaymentRefundService(params, state: State) {
  const { paymentId } = params;
  try {
    const result = await moipCreatePaymentRefund(paymentId, state);

    state.logger.info(
      { result },
      'Payment payment refund created with success',
    );
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error creating payment payment refund');
    throw errors.badGateway('Error creating payment payment refund', { err });
  }
}

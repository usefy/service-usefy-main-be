import { errors } from '../../../errors';
import { State } from '../../../models';
import {
  moipCreateCustomer,
  moipReadCustomer,
} from '../components/moip';
import { customerRepo } from '../repositories/customerRepo';

export async function getCustomerService(state: State) {
  try {
    const customer = await customerRepo.getByUid(state.user.uid, state);

    const result = await moipReadCustomer(customer!.id, state);

    if (!result) {
      state.logger.info('Current user has no customer created');
      throw errors.notFound('Customer not found', {});
    }

    state.logger.info({ result }, 'Payment customer read with success');
    return result;
  } catch (err) {

    // this error is already logged
    if (err.statusCode === 404) {
      throw err;
    }

    state.logger.error({ err }, 'Error reading payment customer');
    throw errors.badGateway('Error reading payment customer', { err });
  }
}

export async function createCustomerService(params, state: State) {
  try {
    const customer = await customerRepo.getByUid(state.user.uid, state);

    if (!customer) {
      state.logger.info('Current user already have a customer created');
      throw errors.forbidden('Current user already have a customer created', {});
    }

    const result = await moipCreateCustomer(params, state);

    state.logger.info({ result }, 'Payment customer created with success');
    return result;
  } catch (err) {

    // this error is already logged
    if (err.statusCode === 403) {
      throw err;
    }

    state.logger.error({ err }, 'Error creating payment customer');
    throw errors.badGateway('Error creating payment customer', { err });
  }
}


import { config } from '../../../config';
import { State } from '../../../models';
import { getSingleListing } from '../../listings/services/listingService';

// Forum do Moip dizendo como calcular os juros
// https://dev.wirecard.com.br/discuss/596e4d8484f7ef001abfb095
export const getInstallmentsAmount = async ({ listingId }: { listingId: string }, installmentsCount: number, state: State) => {
  const listing = await getSingleListing({ id: listingId }, state);

  // TODO: confirmar a taxa de juros
  const credidCartFee = config.moip.creditCard.fee;
  const amount = listing.newPrice ? listing.newPrice : listing.price;

  return Array(installmentsCount).fill(0).map((value, index) => {

    if (index === 0) {
      return amount.toFixed(2);
    }

    const installments = index + 1;
    const interestFactor = credidCartFee / 100;
    const installmentFactor = interestFactor / (1 - (1 / ((interestFactor + 1) ** installments)));

    return (installmentFactor * amount).toFixed(2);
  });
};

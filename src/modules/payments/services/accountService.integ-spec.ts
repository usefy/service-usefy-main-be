import { userRepo as userRepository } from '../../users/repositories/users';
import { accountRepo as accountMoipRepository } from '../../payments/repositories/accountRepo';

import { getApp } from '../../../components/firebase/init';
import * as firebase from '../../../components/firebase/user/index';
import * as Moip from '../../payments/components/moip';

import {
  createUserFaker,
  createStateFaker,
  createMoipFaker,
} from '../../../faker/index';
import { createTables } from '../../../components/dyno/mock/createLocalTables';
import { getUserAccountService, createAccountService } from './accountService';

jest.setTimeout(30000);

describe('Account service', () => {
  beforeAll(async () => {
    await createTables();
    await getApp();
  });

  describe('Get user account', () => {
    const userFaker = createUserFaker();
    const userMoipFaker = createMoipFaker(userFaker);

    const state = createStateFaker({
      user: userFaker,
    });

    it('should be get user account', async () => {
      const user = await userRepository.create(userFaker.uid, userFaker, state);

      await firebase.create(userFaker);

      const userMoip = await Moip.moipCreateAccount(userMoipFaker, state);

      await accountMoipRepository.create(
        {
          uid: user.uid,
          id: userMoip.id,
          accessToken: userMoip.accessToken,
          channelId: userMoip.channelId,
          linksSelf: userMoip._links.self.href,
          login: userMoip.login,
        },
        state,
      );

      const getUserAccountMoip = await getUserAccountService(user.uid, state);

      expect(getUserAccountMoip.id).toEqual(userMoip.id);
    });

    it('should throw an error when invalid id', async () => {
      const state = createStateFaker();
      await expect(getUserAccountService('invalid_id', state))
        .rejects.toThrow();
    });
  });

  describe('Create user account', () => {
    const userFaker = createUserFaker();

    const state = createStateFaker({
      user: userFaker,
    });

    it('should be create user account', async () => {
      const user = await userRepository.create(userFaker.uid, userFaker, state);
      await firebase.create(userFaker);

      const createdAccountMoip = await createAccountService(state);
      const getUserAccountMoip = await getUserAccountService(user.uid, state);

      expect(createdAccountMoip.id).toEqual(getUserAccountMoip.id);
    });

    it('should throw an error when account already exists', async () => {
      await expect(createAccountService(state))
        .rejects.toThrow();
    });
  });
});

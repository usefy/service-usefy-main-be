import { errors } from '../../../errors';
import { State } from '../../../models';
import {
  moipCreateFundinginstrument,
  moipDeleteFundinginstrument,
} from '../components/moip';
import { getCustomerService } from './customerService';
import { customersFundingInstrumentRepo } from '../repositories/customersFundingInstrumentRepo';

export async function createFundinginstrumentService(
  params,
  state: State,
) {
  try {
    const customer = await getCustomerService(state);
    const result = await moipCreateFundinginstrument(customer.id, params, state);

    // save
    await customersFundingInstrumentRepo.create(
      {
        id: result.creditCard.id,
        uid: state.user.uid,
        creditCard: JSON.stringify(result.creditCard),
        card: JSON.stringify(result.card),
      },
      state,
    );

    state.logger.info(
      { result },
      'Payment fundinginstrument created with success',
    );
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error creating payment fundinginstrument');
    throw err;
  }
}

export async function deleteFundinginstrumentService(
  params,
  state: State,
) {
  const { creditcardId } = params;
  try {
    const result = await moipDeleteFundinginstrument(creditcardId, state);

    // remove from table
    await customersFundingInstrumentRepo.delete(
      {
        id: creditcardId,
        uid: state.user.uid,
      },
      state,
    );

    state.logger.info(
      { result },
      'Payment fundinginstrument deleted with success',
    );
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error deleting payment fundinginstrument');
    throw err;
  }
}

export async function getFundinginstrumentService(state: State) {
  try {
    const cards = await customersFundingInstrumentRepo.getByUid(state.user.uid, state);

    return cards!.map((card) => {
      return {
        id: card.id,
        creditCard: JSON.parse(card.creditCard),
        card: JSON.parse(card.card),
      };
    });
  } catch (err) {
    state.logger.error({ err }, 'Error get user\'s payment fundinginstrument');
    throw err;
  }
}

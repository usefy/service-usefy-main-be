import { State } from '../../../models';
import {
  moipCreateBankaccount,
  moipDeleteBankaccount,
  moipReadBankaccount,
  moipReadAllBankaccount,
} from '../components/moip';
import { moipBankAccountRepo } from '../repositories/moipBankAccountRepo';
import { getAccount } from './accountService';

export async function readBankaccountService(params, state: State) {
  const { id } = params;
  try {
    const account = await getAccount(state);
    const result = await moipReadBankaccount(id, account.accessToken, state);

    state.logger.info({ result }, 'Payment bankaccount read with success');
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error deleting payment bankaccount');
    throw err;
  }
}

export async function readAllBankaccountService(state: State) {
  try {
    const account = await getAccount(state);
    const result = await moipReadAllBankaccount(
      account.id,
      account.accessToken,
      state,
    );

    state.logger.info({ result }, 'Payment bankaccount read with success');
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error deleting payment bankaccount');
    throw err;
  }
}

export async function createBankaccountService(body, state: State) {
  try {
    const account = await getAccount(state);
    const result = await moipCreateBankaccount(
      account.id,
      account.accessToken,
      body,
      state,
    );

    // update bank account after success
    await moipBankAccountRepo.create(
      {
        uid: state.user.uid,
        id: result.id,
        agencyNumber: result.agencyNumber,
        agencyCheckNumber: result.agencyCheckNumber,
        accountNumber: result.accountNumber,
        accountCheckNumber: result.accountCheckNumber,
        bankName: result.bankName,
        bankNumber: result.bankNumber,
        type: result.type,
        holder: result.holder,
        status: result.status,
        linksSelf: result._links.self.href,
      },
      state,
    );

    state.logger.info({ result }, 'Payment bankaccount created with success');
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error deleting payment bankaccount');
    throw err;
  }
}

export async function deleteBankaccountService(params, state: State) {
  const { id } = params;
  try {
    const account = await getAccount(state);
    const result = await moipDeleteBankaccount(id, account.accessToken, state);

    state.logger.info({ result }, 'Payment bankaccount deleted with success');
    return result;
  } catch (err) {
    // this error is already logged
    if (err.statusCode === 502) {
      throw err;
    }

    state.logger.error({ err }, 'Error deleting payment bankaccount');
    throw err;
  }
}

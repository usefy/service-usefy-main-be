import { errors } from '../../../errors';
import { State } from '../../../models';
import { getAccount } from './accountService';
import { moipReadBalance } from '../components/moip';

export async function readBalanceService(state: State) {
  try {
    const account = await getAccount(state);

    const result = await moipReadBalance(state, account.accessToken);

    state.logger.info({ result }, 'Payment balance read with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error reading payment balance');
    throw errors.badGateway('Error reading payment balance', { err });
  }
}

import { has, get } from 'lodash';
import { State } from '../../../models';
import { errors } from '../../../errors';
import { orderRepo } from '../repositories/orderRepo';
import { paymentRepo } from '../repositories/paymentRepo';
import { updatePaymentService } from './paymentService';
import { PaymentDocument } from '../models/Payment';
import { updateOrder } from '../../checkout/services/checkoutService';
import { OrderStatus as MoipOrderStatus } from '../models/Orders';
import { OrderStatus } from '../../checkout/models/order';

export enum OrderEvents {
  all = 'ORDER.*',
  created = 'ORDER.CREATED',
  waiting = 'ORDER.WAITING',
  paid = 'ORDER.PAID',
  notPaid = 'ORDER.NOT_PAID',
  reverted = 'ORDER.REVERTED',
}

export enum PaymentEvents {
  all = 'PAYMENT.*',
  created = 'PAYMENT.CREATED',
  waiting = 'PAYMENT.WAITING',
  analysis = 'PAYMENT.IN_ANALYSIS',
  preAuthorized = 'PAYMENT.PRE_AUTHORIZED',
  authorized = 'PAYMENT.AUTHORIZED',
  cancelled = 'PAYMENT.CANCELLED',
  refunded = 'PAYMENT.REFUNDED',
  reversed = 'PAYMENT.REVERSED',
  settled = 'PAYMENT.SETTLED',
}

export enum RefundEvents {
  all = 'REFUND.*',
  requested = 'REFUND.REQUESTED',
  completed = 'REFUND.COMPLETED',
  failed = 'REFUND.FAILED',
}

export enum MultiOrderEvents {
  all = 'MULTIORDER.*',
  created = 'MULTIORDER.CREATED',
  paid = 'MULTIORDER.PAID',
  notPaid = 'MULTIORDER.NOT_PAID',
}

export enum MultiPaymentEvents {
  all = 'MULTIPAYMENT.*',
  waiting = 'MULTIPAYMENT.WAITING',
  analysis = 'MULTIPAYMENT.IN_ANALYSIS',
  authorized = 'MULTIPAYMENT.AUTHORIZED',
  cancelled = 'MULTIPAYMENT.CANCELLED',
}

export enum TransfersEvents {
  all = 'TRANSFER.*',
  requested = 'TRANSFER.REQUESTED',
  completed = 'TRANSFER.COMPLETED',
  failed = 'TRANSFER.FAILED',
}

export enum EscrowEvents {
  all = 'ESCROW.*',
  pending = 'ESCROW.HOLD_PENDING',
  held = 'ESCROW.HELD',
  released = 'ESCROW.RELEASED',
}

const eventMatch = (regexp: string, e: string) => RegExp(regexp).test(e);

const logAndSuccess = (e, resource, state) => {
  state.logger.info({ webhookEvent: e, webhookResource: resource }, 'Unprocessed resource');
  return true;
};

const paymentProcessor = async (resource, state: State): Promise<PaymentDocument> => {
  state.logger.info({ webhookResource: resource }, 'Webhook payment processor started');

  if (!has(resource, 'payment')) {
    state.logger.warn({ webhookResource: resource }, 'No payment in payload');
    throw errors.badRequest('No payment in payload', {});
  }

  const { payment } = resource;
  const order = await orderRepo.getById(get(payment, '_links.order.title'), state);

  if (!order) {
    state.logger.warn({ webhookResource: resource }, 'Order not found');
    throw errors.notFound('Order not found', {});
  }

  const myPayment = await paymentRepo.getById(get(payment, 'id'), state);

  if (!myPayment) {
    state.logger.warn({ webhookResource: resource }, 'Payment not found');
    throw errors.notFound('Payment not found', {});
  }

  const document = {
    amount: JSON.stringify(payment.amount),
    paymentStatus: payment.status,
  };

  const updatedPayment = await updatePaymentService(
    {
      id: myPayment.id,
      uid: myPayment.uid,
    },
    document,
    state,
  );

  return updatedPayment;
};

const orderProcessor = async (resource, state: State) => {
  state.logger.info({ webhookResource: resource }, 'Webhook order processor started');

  if (!has(resource, 'order')) {
    state.logger.warn({ webhookResource: resource }, 'No order in payload');
    throw errors.badRequest('No order in payload', {});
  }

  const id = get(resource, 'order.ownId');
  const order = await orderRepo.getByOrderId(id, state);

  if (!order) {
    state.logger.warn({ webhookResource: resource }, 'Order not found');
    throw errors.notFound('Order not found', {});
  }

  let status = null;

  switch (get(resource, 'order.status')) {
    case MoipOrderStatus.waiting:
      status = OrderStatus.paymentIssue;
      break;
    case MoipOrderStatus.paid:
      status = OrderStatus.awaitShipment;
      break;
    case MoipOrderStatus.notPaid:
      status = OrderStatus.canceled;
      break;
    case MoipOrderStatus.reverted:
      status = OrderStatus.revertedPayment;
      break;
    default:
      break;
  }

  if (status) {
    const updatedOrder = await updateOrder({ id }, { status, summary: 'Atualização automática pelo gateway de pagamento' }, state);
  }
  return true;
};

export const notificationReceived = async (body, state: State): Promise<boolean> => {
  const {
    data,
    env,
    event,
    resource,
  } = body;

  state.logger.info({ notificationBody: body }, 'New notification received.');

  try {
    if (eventMatch(PaymentEvents.all, event)) {
      const payment = await paymentProcessor(resource, state);
      state.logger.info({ payment }, 'Payment updated by webhook');
    } else if (eventMatch(OrderEvents.all, event)) {
      const order = await orderProcessor(resource, state);
      state.logger.info({ order }, 'Order updated by webhook');
    } else {
      await logAndSuccess(event, resource, state);
    }

    return true;
  } catch (err) {

    state.logger.warn({ err, notificationBody: body }, 'Fail to process webhook.');
    throw errors.teapot('Something went wrong.');
  }

};

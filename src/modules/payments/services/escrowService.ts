import { errors } from '../../../errors';
import { State } from '../../../models';
import { moipCreateEscrow } from '../components/moip';

export async function createEscrowService({ escrowId }, state: State) {
  try {
    const result = await moipCreateEscrow(escrowId, state);

    state.logger.info({ result }, 'Payment escrow created with success');
    return result;
  } catch (err) {
    state.logger.error({ err }, 'Error creating payment escrow');
    throw errors.badGateway('Error creating payment escrow', { err });
  }
}

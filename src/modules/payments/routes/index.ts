import { Router } from 'express';

import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';
import * as validations from './validations';

import * as accountController from '../controllers/accountController';
import * as customerController from '../controllers/customerController';
import * as fundinginstrumentController from '../controllers/fundinginstrumentController';
import * as paymentController from '../controllers/paymentController';
import * as bankaccountController from '../controllers/bankaccountController';
import * as statementsController from '../controllers/statementsController';
import * as transfersController from '../controllers/transfersController';
import * as notificationController from '../controllers/notificationController';
import * as balanceController from '../controllers/balanceController';
import * as escrowController from '../controllers/escrowController';

export const router = Router();

router
  .route('/payments/account')
  .post(
    validateFirebaseIdToken,
    routerDefaultHandler(accountController.accountPost),
  )
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(accountController.accountGet),
  );

router
  .route('/payments/customers')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(customerController.customerGet),
  )
  .post(
    validateFirebaseIdToken,
    validateReqParams(validations.customerPost),
    routerDefaultHandler(customerController.customerPost),
  );

router
  .route('/payments/methods/:listingId')
  .get(
    validateFirebaseIdToken,
    validateReqParams(validations.methodsGet),
    routerDefaultHandler(paymentController.paymentGetMethods),
  );

router
  .route('/payments/customers/fundinginstruments')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(fundinginstrumentController.fundinginstrumentGet),
  )
  .post(
    validateFirebaseIdToken,
    validateReqParams(validations.fundinginstrumentPost),
    routerDefaultHandler(fundinginstrumentController.fundinginstrumentPost),
  )
  .delete(
    validateFirebaseIdToken,
    validateReqParams(validations.fundinginstrumentDelete),
    routerDefaultHandler(fundinginstrumentController.fundinginstrumentDelete),
  );

// router
//   .route('/payments/orders/:orderId/payment')
//   .post(
//     validateFirebaseIdToken,
//     validateReqParams(validations.paymentPost),
//     routerDefaultHandler(paymentController.paymentPost),
//   );

router
  .route('/payments/bankaccounts')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(bankaccountController.allBankaccountsGet),
  )
  .post(
    validateFirebaseIdToken,
    validateReqParams(validations.bankaccountPost),
    routerDefaultHandler(bankaccountController.bankaccountPost),
  );

router
  .route('/payments/bankaccounts/:id')
  .get(
    validateFirebaseIdToken,
    validateReqParams(validations.bankaccountGet),
    routerDefaultHandler(bankaccountController.bankaccountGet),
  )
  .delete(
    validateFirebaseIdToken,
    validateReqParams(validations.bankaccountGet),
    routerDefaultHandler(bankaccountController.bankaccountDelete),
  );

router
  .route('/payments/statements')
  .get(
    validateFirebaseIdToken,
    validateReqParams(validations.statementsGet),
    routerDefaultHandler(statementsController.statementsDetailGet),
  );

router
  .route('/payments/futurestatements')
  .get(
    validateFirebaseIdToken,
    validateReqParams(validations.statementsGet),
    routerDefaultHandler(statementsController.futureStatementsGet),
  );

router
  .route('/payments/statements/detail')
  .get(
    validateFirebaseIdToken,
    validateReqParams(validations.statementsDetailGet),
    routerDefaultHandler(statementsController.statementsDetailGet),
  );

router
  .route('/payments/transfers/:id')
  .get(
    validateFirebaseIdToken,
    validateReqParams(validations.transfersGet),
    routerDefaultHandler(transfersController.transfersDetailGet),
  );

router
  .route('/payments/transfers')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(transfersController.transfersGetAll),
  )
  .post(
    validateFirebaseIdToken,
    validateReqParams(validations.transfersPost),
    routerDefaultHandler(transfersController.transfersPost),
  );

router
  .route('/payments/notifications')
  .post(
    notificationController.validateNotificationsToken,
    routerDefaultHandler(notificationController.notificationPost),
  );

router
  .route('/payments/balance')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(balanceController.balanceGet),
  );

router
  .route('/payments/escrow/:escrowId')
  .post(
    validateFirebaseIdToken,
    routerDefaultHandler(escrowController.escrowPost),
  );

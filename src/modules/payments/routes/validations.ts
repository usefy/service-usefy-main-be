import * as joi from '@hapi/joi';

const taxDocumentSchema = joi.object().keys({
  type: joi
    .string()
    .valid('CPF')
    .required(),
  number: joi
    .string()
    .regex(/^[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}$/)
    .required(),
});

const addressSchema = joi.object().keys({
  street: joi.string().required(),
  streetNumber: joi.string().required(),
  complement: joi.string(),
  district: joi.string().required(),
  zipcode: joi
    .string()
    .regex(/^[0-9]{5}$/)
    .required(),
  city: joi.string().required(),
  state: joi
    .string()
    .regex(/^[A-Z]{2}$/)
    .required(),
  country: joi
    .string()
    .valid('BRA')
    .required(),
});

const phoneSchema = joi.object().keys({
  countryCode: joi
    .string()
    .valid('55')
    .required(),
  areaCode: joi
    .string()
    .regex(/^[0-9]{2}$/)
    .required(),
  number: joi
    .string()
    .regex(/^[0-9]{8,16}$/)
    .required(),
});

const holderSchema = joi.object().keys({
  fullname: joi
    .string()
    .max(65)
    .required(),
  birthDate: joi
    .date()
    .max('now')
    .required(),
  taxDocument: taxDocumentSchema,
  phone: phoneSchema,
});

export const accountsPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      email: {
        address: joi
          .string()
          .email()
          .required(),
      },
      person: {
        name: joi.string().required(),
        lastName: joi.string().required(),
        birthDate: joi
          .date()
          .max('now')
          .required(),
        taxDocument: taxDocumentSchema,
        address: addressSchema,
        phone: phoneSchema,
        identityDocument: {
          number: joi.string().required(),
          issuer: joi.string().required(),
          issueDate: joi
            .date()
            .max('now')
            .required(),
          type: joi
            .string()
            .valid('RG')
            .required(),
        },
      },
      type: joi
        .string()
        .valid('MERCHANT')
        .required(),
      transparentAccount: joi
        .boolean()
        .valid('true')
        .required(),
    })
    .required(),
});

export const customerPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      ownId: joi
        .string()
        .max(65)
        .required(),
      fullname: joi
        .string()
        .max(90)
        .required(),
      email: joi
        .string()
        .email()
        .max(90)
        .required(),
      birthDate: joi
        .date()
        .max('now')
        .required(),
      taxDocument: taxDocumentSchema,
      phone: phoneSchema,
      shippingAddress: addressSchema,
    })
    .required(),
});

export const fundinginstrumentPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      method: joi
        .string()
        .valid('CREDIT_CARD,')
        .required(),
      creditCard: {
        expirationMonth: joi
          .string()
          .regex(/^[0-9]{1.2}$/)
          .required(),
        expirationYear: joi
          .string()
          .regex(/^[0-9]{2}$/)
          .required(),
        number: joi.string().creditCard(),
        cvc: joi
          .string()
          .regex(/^[0-9]{3,4}$/)
          .required(),
        holder: holderSchema,
      },
    })
    .required(),
});

export const fundinginstrumentDelete: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      creditcardId: joi.string().required(),
    })
    .required(),
});

export const orderPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      ownId: joi
        .string()
        .max(65)
        .required(),
      amount: {
        currency: joi
          .string()
          .valid('BRL')
          .required(),
        subtotals: {
          shipping: joi
            .string()
            .regex(/^[0-9]{1,9}$/)
            .required(),
        },
      },
      items: [
        {
          product: joi
            .string()
            .max(250)
            .required(),
          category: joi
            .string()
            .max(256)
            .required(),
          quantity: joi
            .number()
            .max(9)
            .required(),
          detail: joi
            .string()
            .max(250)
            .required(),
          price: joi
            .string()
            .regex(/^[0-9]{1,8}$/)
            .required(),
        },
      ],
      customer: {
        id: joi.string().required(),
      },
      receivers: [
        {
          type: joi
            .string()
            .valid('SECONDARY')
            .required(),
          feePayor: joi
            .boolean()
            .valid('false')
            .required(),
          moipAccount: {
            id: joi
              .string()
              .max(16)
              .required(),
          },
          amount: {
            fixed: joi
              .string()
              .regex(/^[0-9]{1,12}$/)
              .required(),
          },
        },
      ],
    })
    .required(),
});

export const paymentPost: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      orderId: joi.string().required(),
    }).required(),
  body: joi
    .object()
    .keys({
      method: joi.string().valid('BOLETO').valid('CREDIT_CARD').required(),
      fundingInstrument: {
        id: joi.string().required(),
      },
      installmentCount: joi.number().min(1).max(12),
    }).required(),
});

export const bankaccountPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      bankNumber: joi
        .string()
        .regex(/^[0-9]{3}$/)
        .required(),
      agencyNumber: joi.string().required(),
      agencyCheckNumber: joi.string().required(),
      accountNumber: joi.string().required(),
      accountCheckNumber: joi.string().required(),
      type: joi
        .string()
        .valid('CHECKING')
        .required(),
      holder: {
        taxDocument: taxDocumentSchema,
        fullname: joi.string().required(),
      },
    })
    .required(),
});

export const bankaccountPut: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      bankNumber: joi
        .string()
        .regex(/^[0-9]{3}$/)
        .required(),
      agencyNumber: joi.string().required(),
      agencyCheckNumber: joi.string().required(),
      accountNumber: joi.string().required(),
      accountCheckNumber: joi.string().required(),
      type: joi
        .string()
        .valid('CHECKING')
        .required(),
      holder: {
        taxDocument: taxDocumentSchema,
        fullname: joi.string().required(),
      },
    })
    .required(),
});

export const bankaccountGet: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      id: joi.string().required(),
    })
    .required(),
});

export const methodsGet: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      listingId: joi.string().required(),
    }),
});

export const statementsGet: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      begin: joi.string().required(),
      end: joi.string().required(),
    }),
});

export const statementsDetailGet: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      type: joi.string().required(),
      date: joi.string().required(),
    }),
});

export const transfersPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      amount: joi.number().min(1).required(),
      bankAccount: joi.object().keys({
        id: joi.string().required(),
      }).required(),
    }),
});

export const transfersGet: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      id: joi.string().required(),
    })
    .required(),
});

import { Request, Response } from 'express';
import { logoutService, loginService } from '../services/authService';

export async function login(req: Request, res: Response) {
  return loginService({ user: req.state.user }, req.state);
}

export async function logout(req: Request, res: Response) {
  return logoutService(req.state);
}

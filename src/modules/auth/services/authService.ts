import * as firebase from '../../../components/firebase';

export async function loginService({ user }, state) {
  state.logger.info('Starts login service.');
  const token = await firebase.auth.createToken(user.uid);
  return token;
}

export async function logoutService(state) {
  state.logger.info('Starts logout service.');

}

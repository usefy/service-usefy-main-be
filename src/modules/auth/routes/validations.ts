import * as joi from '@hapi/joi';

export const loginPost: joi.SchemaLike = joi.object().keys({
  headers: joi
    .object()
    .keys({
      Authorization: joi.string().required(),
    }),
  body: joi
    .object()
    .keys({
      email: joi.string().email().required(),
    })
    .required(),
});

export const logoutPost: joi.SchemaLike = joi.object().keys({
  headers: joi
    .object()
    .keys({
      Authorization: joi.string().required(),
    }),
});

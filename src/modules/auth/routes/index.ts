import { NextFunction, Request, Response, Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';
import { loginPost, logoutPost } from './validations';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import * as authController from '../controllers/authController';

export const router = Router();

router
  .route('/login')
  .post(
    validateFirebaseIdToken,
    validateReqParams(loginPost),
    routerDefaultHandler(authController.login),
  );

router
  .route('/logout')
  .post(
    validateFirebaseIdToken,
    validateReqParams(logoutPost),
    routerDefaultHandler(authController.logout),
  );

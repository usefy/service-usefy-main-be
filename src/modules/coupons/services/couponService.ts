import v4 from 'uuid/v4';
import moment from 'moment';
import { State } from '../../../models';
import { getSingleListing } from '../../listings/services/listingService';
import { errors } from '../../../errors';
import { couponsRepo } from '../repositories/couponsRepo';
import { randomString } from '../../../helpers/randomString';
import { config } from '../../../config';
import { profileService } from '../../users/services/profileService';

export interface CouponBody {
  title: string;
  value: number;
  listingItem: string;
}

export interface CouponParam {
  id: string;
}

export const createCoupon = async (body: CouponBody, state: State) => {
  const listing = await getSingleListing({ id: body.listingItem }, state);

  if (listing.firebaseUid !== state.user.uid) {
    state.logger.warn({ listingItem: body.listingItem, firebaseUid: listing.firebaseUid, uid: state.user.uid }, '');
    throw errors.forbidden('User can\'t create a coupon for given listing.', { listingItem: body.listingItem });
  }

  try {
    let key = '';
    let keyIsUnique = false;

    while (!keyIsUnique) {
      key = randomString(9);

      const keyCount = await couponsRepo.countKey(key, state);
      keyIsUnique = (keyCount! < 1);
    }

    const coupon = await couponsRepo.create(
      {
        key,
        id: v4(),
        firebaseUid: state.user.uid,
        title: body.title,
        value: body.value,
        validUntil: moment().add(config.coupons.validInHours, 'hours'),
        listingItem: body.listingItem,
      },
      state,
    );

    state.logger.info({ coupon }, 'New coupon created.');

    return coupon;
  } catch (err) {
    state.logger.error({ err, ...body }, 'Error on create a new coupon');
    throw err;
  }
};

export const getCoupon = async (params, state: State) => {

  if (! params.id) {
    return {};
  }

  try {
    let coupon = await couponsRepo.getOneById(params.id, state);

    if (!coupon) {
      coupon = await couponsRepo.getOneByKey(params.id, state);
    }

    if (!coupon) {
      state.logger.warn({ params }, 'Coupon not found.');
      throw errors.notFound('Coupon not found', { ...params });
    }

    // parallel promises
    const [owner, usedBy, listingItem] = await Promise.all([
      profileService({ username: null, uid: coupon.firebaseUid }, state),
      (coupon.usedBy ? profileService({ username: null, uid: coupon.usedBy }, state) : async () => null),
      getSingleListing({ id: coupon.listingItem }, state),
    ]);

    return {
      owner,
      usedBy,
      listingItem,
      id: coupon.id,
      key: coupon.key,
      title: coupon.title,
      value: coupon.value,
      validUntil: coupon.validUntil,
      usedAt: coupon.usedAt,
      createdAt: coupon.createdAt,
      purchaseId: coupon.purchaseId,
    };

  } catch (err) {
    state.logger.error({ err, ...params }, 'Error getting a coupon');
    throw err;
  }
};

// TODO: arrumar a utilização de cupom quando tivermos o fluxo de compra
export const updateCoupon = (params, body: { listingId: string }, state: State) => {
  throw errors.forbidden('This method is not implemented', {});
};

import { Request } from 'express';
import * as couponService from '../services/couponService';

export async function couponGet(req: Request) {
  return couponService.getCoupon(req.params, req.state);
}
export async function couponPost(req: Request) {
  return couponService.createCoupon(req.body, req.state);
}
export async function couponPut(req: Request) {
  return couponService.updateCoupon(req.params, req.body, req.state);
}

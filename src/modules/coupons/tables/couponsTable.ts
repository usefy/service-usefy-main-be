import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';

const tableName = `usefy-${config.server.configEnv}-coupons`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  key: joi.string().required(),
  firebaseUid: joi.string().required(),
  title: joi.string().required(),
  value: joi.number().required(),
  validUntil: joi.date().required(),
  createdAt: joi.date().required(),
  listingItem: joi.string(),
  usedBy: joi.string(),
  usedAt: joi.date(),
  destroyedAt: joi.date(),
});

export class CouponsModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getOneById (id: string, state: State) {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#id = :id',
      ExpressionAttributeNames: {
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoQuery(params, state);
    return data![0];
  }

  async getOneByKey (key: string, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'key_index',
      KeyConditionExpression: '#key = :key',
      ExpressionAttributeNames: {
        '#key': 'key',
      },
      ExpressionAttributeValues: {
        ':key': key,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoQuery(params, state);
    return data![0];
  }

  async countKey (key: string, state: State) {
    const params = {
      TableName: this.tableName,
      Select: 'COUNT',
      IndexName: 'key_index',
      FilterExpression: 'key = :key',
      ExpressionAttributeValues: {
        ':key': key,
      },
      ConsistentRead: false,
    };
    const { Count } = await this.dynamoScan(params, state);
    return Count;
  }
}

export const couponsTable = new CouponsModel(tableName, validationScheme);

import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';
import { couponPost, couponPut, couponGet } from './validations';
import * as couponController from '../controllers/couponController';

export const router = Router();

router
  .route('/coupon')
  .post(
    validateFirebaseIdToken,
    validateReqParams(couponPost),
    routerDefaultHandler(couponController.couponPost),
  );

router
  .route('/coupon/:id')
  .get(
    validateFirebaseIdToken,
    validateReqParams(couponGet),
    routerDefaultHandler(couponController.couponGet),
  )
  .put(
    validateFirebaseIdToken,
    validateReqParams(couponPut),
    routerDefaultHandler(couponController.couponPut),
  );

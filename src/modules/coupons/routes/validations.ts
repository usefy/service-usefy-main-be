import * as joi from '@hapi/joi';

export const couponPost: joi.SchemaLike = joi.object().keys({
  query: joi
    .object()
    .keys({
      title: joi.string().required(),
      value: joi.number().min(1).required(),
      listingItem: joi.string().required(),
    })
    .required(),
});

export const couponGet: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      id: joi.string().required(),
    })
    .required(),
});

export const couponPut: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      id: joi.string().required(),
    })
    .required(),
  query: joi
    .object()
    .keys({
      purchaseId: joi.string().required(),
    })
    .required(),
});

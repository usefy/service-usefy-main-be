import * as _ from 'lodash';
import moment from 'moment';
import { State } from '../../../models';
import { couponsTable } from '../tables/couponsTable';

export const couponsRepo = {
  create: async (document, state: State) => {
    const documentCopy = _.cloneDeep(document);
    return couponsTable.create(
      _.merge({}, documentCopy, {
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },
  getOneById: async (id: string, state: State) => couponsTable.getOneById(id, state),
  getOneByKey: (key: string, state: State) => couponsTable.getOneByKey(key, state),
  countKey: (key: string, state: State) => couponsTable.countKey(key, state),
};

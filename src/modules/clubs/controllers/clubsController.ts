import { Request } from 'express';
import { getSingleClubPartner } from '../services/clubsPartnerService';
import { searchClubs, newClub } from '../services/clubsService';

export async function getSearch(req: Request) {
  return searchClubs(req.query, req.state);
}

export async function postClub(req: Request) {
  return newClub(req.body, req.state);
}

export async function getClubPartner(req: Request) {
  return getSingleClubPartner(req.params, req.state);
}

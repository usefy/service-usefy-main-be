import supertest from 'supertest';
import { app } from '../../../app';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';

jest.mock('../../../helpers/authHandler');

const mockedValidate = validateFirebaseIdToken as jest.MockedFunction<typeof validateFirebaseIdToken>;

describe('Clubs Module', () => {
  describe('post club endpoint validations', () => {
    beforeAll(() => {
      mockedValidate.mockImplementation(async (req, res, next) => { next(); });
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    it('should not create club without state', async () => {
      await supertest(app)
        .post('/v1/clubs')
        .send({ clubName: 'clube da lulu', city: 'cidade' })
        .expect(400);
    });

    it('should not create club without city', async () => {
      await supertest(app)
        .post('/v1/clubs')
        .send({ clubName: 'clube da lulu', state: 'estado' })
        .expect(400);
    });

    it('should not create club without clubName', async () => {
      await supertest(app)
        .post('/v1/clubs')
        .send({ city: 'city', state: 'state' })
        .expect(400);
    });
  });
});

import { State } from '../../../models';
import { clubsPartnerTable } from '../tables/clubsPartnerTable';

export const clubsPartnerRepo = {
  getById: async (id: string, state: State) =>
    clubsPartnerTable.getById(id, state),
  countAll: (state: State) => clubsPartnerTable.countAll(state),
};

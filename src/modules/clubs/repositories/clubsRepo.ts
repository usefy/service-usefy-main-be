import moment from 'moment';
import { State } from '../../../models';
import { clubsTable } from '../tables/clubsTable';
import { Club } from '../models/Club';
import { NewClubDocument } from '../services/clubsService';

export const clubsRepo = {
  searchByClubName: async (params, state: State) =>
    clubsTable.searchByClubName(params, state),
  getByKeys: async (keys, state: State) => clubsTable.getByKeys(keys, state),
  countAll: (state: State) => clubsTable.countAll(state),
  create: async (document: NewClubDocument, state: State): Promise<Club> => {
    return clubsTable.create(
      {
        ...document,
        createdAt: moment().toISOString(),
      },
      state,
    );
  },
};

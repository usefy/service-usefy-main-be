import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';

const tableName = `usefy-${config.server.configEnv}-clubs-partner`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  clubName: joi.string().required(),
  createdAt: joi.date().iso(),
  updatedAt: joi.date().iso(),
});

export class ClubsPartnerModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getById(id: string, state: State) {
    const params = {
      TableName: this.tableName,
      IndexName: 'id_index',
      KeyConditionExpression: '#id = :id',
      ExpressionAttributeNames: {
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
      },
      ConsistentRead: false,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return Items![0];
  }
}

export const clubsPartnerTable = new ClubsPartnerModel(
  tableName,
  validationScheme,
);

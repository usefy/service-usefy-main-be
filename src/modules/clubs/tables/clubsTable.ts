import * as joi from '@hapi/joi';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';

const tableName = `usefy-${config.server.configEnv}-clubs`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  clubName: joi.string().required(),
  state: joi.string().required(),
  city: joi.string().required(),
  createdAt: joi.date().iso(),
  updatedAt: joi.date().iso(),
});

export class ClubsModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async searchByClubName(clubName: string, state: State) {
    const params = {
      TableName: this.tableName,
      FilterExpression: 'begins_with( #clubName, :clubName ) ',
      ExpressionAttributeNames: {
        '#clubName': 'clubName',
      },
      ExpressionAttributeValues: {
        ':clubName': clubName,
      },
    };
    const { Items: data } = await this.dynamoScan(params, state);
    return data;
  }

  async getByKeys(keys, state: State) {
    const params = { RequestItems: {} };

    params.RequestItems[this.tableName] = {
      Keys: keys,
      ConsistentRead: false,
      ProjectionExpression: 'clubName',
    };

    state.logger.info(
      { dynamodbCall: params },
      'batchGet call to get items from ids list',
    );
    const { Responses } = await this.client.batchGet(params).promise();
    return Responses ? Responses![this.tableName] : [];
  }
}

export const clubsTable = new ClubsModel(tableName, validationScheme);

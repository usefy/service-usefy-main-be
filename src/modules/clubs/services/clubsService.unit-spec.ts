import { stateFactory } from '../../../../test/utils';
import { newClub } from './clubsService';
import { clubsTable } from '../tables/clubsTable';

jest.mock('../tables/clubsTable');

const fakeTimestamp = 1584498992320;
const mockedClubsTable = clubsTable.create as jest.MockedFunction<typeof clubsTable.create>;
const mockedDate = jest.spyOn(Date, 'now');

describe('clubsService', () => {
  describe('newClub', () => {
    describe('when giving a club name', () => {
      beforeEach(() => {
        mockedDate.mockImplementation(() => fakeTimestamp);
        mockedClubsTable.mockImplementationOnce((body: any) => ({ ...body }));
      });

      afterEach(() => {
        mockedDate.mockClear();
        mockedClubsTable.mockClear();
      });

      afterAll(() => {
        jest.clearAllMocks();
      });

      it('should create a new club with state and city', async () => {
        const body = {
          clubName: 'clube da luluzinha com endereço',
          state: 'Acre',
          city: 'Rio Branco',
        };
        const createdClub = await newClub({ ...body }, stateFactory());

        expect(mockedClubsTable).toHaveBeenCalledTimes(1);
        expect(createdClub).toBeTruthy();
        expect(createdClub.id).toBeTruthy();
        expect(createdClub.clubName).toEqual(body.clubName);
        expect(createdClub.state).toEqual(body.state);
        expect(createdClub.city).toEqual(body.city);
        expect(createdClub.createdAt).toEqual('2020-03-18T02:36:32.320Z');
      });
    });
  });
});

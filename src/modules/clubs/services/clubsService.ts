import v4 from 'uuid/v4';
import { State } from '../../../models';
import { errors } from '../../../errors';
import { clubsRepo } from '../repositories/clubsRepo';

interface SearchParams {
  search: string;
}

interface ClubPayload {
  clubName: string;
  state: string;
  city: string;
}

export interface NewClubDocument extends ClubPayload {
  id: string;
}

export const searchClubs = async (params: SearchParams, state: State) => {
  state.logger.info('Clubs Service');

  try {
    const item = await clubsRepo.searchByClubName(params.search, state);

    if (!item) {
      state.logger.warn({ item, params }, 'Club not found');
      throw errors.notFound('Club not found', { params });
    }

    return item;
  } catch (err) {
    state.logger.error({ err }, 'Error getting Club');
    throw err;
  }
};

export async function newClub(body: ClubPayload, state: State) {
  state.logger.info({ body }, 'New club');
  const document: NewClubDocument = {
    id: v4(),
    clubName: body.clubName,
    state: body.state,
    city: body.city,
  };

  try {
    const response = await clubsRepo.create(document, state);
    state.logger.info({ newClub: response }, 'New club created');
    return response;
  } catch (err) {
    state.logger.error({ err }, 'Error on creating new club.');
    throw err;
  }
}

import { State } from '../../../models';
import { errors } from '../../../errors';
import { clubsPartnerRepo } from '../repositories/clubsPartnerRepo';

export async function getSingleClubPartner(params, state: State) {
  state.logger.info('Clubs Partner service');
  const { id } = params;

  try {
    const item = await clubsPartnerRepo.getById(id, state);

    if (!item) {
      state.logger.warn({ item, id }, 'Club Partner not found');
      throw errors.notFound('Club Partner not found', { id });
    }

    return item;
  } catch (err) {
    state.logger.error({ err }, 'Error getting Club Partner');
    throw err;
  }
}

import * as joi from '@hapi/joi';

export const searchGet: joi.SchemaLike = joi.object().keys({
  query: joi
    .object()
    .keys({
      keyword: joi.string(),
    }),
});

export const clubsSingle: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      id: joi.string().required(),
    })
    .required(),
});

export const clubsPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      clubName: joi.string().required(),
      state: joi.string().required(),
      city: joi.string().required(),
    })
    .required(),
});

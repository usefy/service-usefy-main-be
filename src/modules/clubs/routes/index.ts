import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';
import * as clubsController from '../controllers/clubsController';
import { searchGet, clubsSingle, clubsPost } from './validations';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';

export const router = Router();

router
  .route('/clubs')
  .get(
    validateReqParams(searchGet),
    routerDefaultHandler(clubsController.getSearch),
  ).post(
    validateFirebaseIdToken,
    validateReqParams(clubsPost),
    routerDefaultHandler(clubsController.postClub),
  );

router
  .route('/clubs/:clubId')
  .get(
    validateReqParams(clubsSingle),
    routerDefaultHandler(clubsController.getClubPartner),
  );

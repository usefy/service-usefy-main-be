export interface ClubPartner {
  id: string;
  clubName: string;
  createdAt: Date;
  updatedAt: Date;
}

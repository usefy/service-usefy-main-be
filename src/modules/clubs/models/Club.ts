export interface Club {
  id: string;
  clubName: string;
  state?: string;
  city?: string;
  createdAt: Date;
  updatedAt: Date;
}

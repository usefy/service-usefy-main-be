import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { healthController } from '../controller/healthController';

export const router = Router();

router
  .route('/health')
  .get(
    routerDefaultHandler(healthController),
  );

router
  .route('/version')
  .get(
    routerDefaultHandler(healthController),
  );

import { Request, Response } from 'express';
import { config } from '../../../config';

export async function healthController(req: Request, res: Response) {
  return {
    name: config.log.name,
    version: process.env.npm_package_version,
    dateTime: (new Date()).toISOString(),
    timestamp: (new Date()).valueOf(),
  };
}

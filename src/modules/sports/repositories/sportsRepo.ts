import * as _ from 'lodash';
import moment from 'moment';
import { State } from '../../../models';
import { sportsTable } from '../tables/sportsTable';

export const sportsRepo = {
  all: async (state: State) => sportsTable.all(state),
  getOneById: async (id: string, state: State) => sportsTable.getOneById(id, state),
  countAll: (state: State) => sportsTable.countAll(state),
  create: async (document, state: State) => {
    return sportsTable.create(
      {
        ...document,
        createdAt: moment().toISOString(),
      },
      state,
    );
  },
};

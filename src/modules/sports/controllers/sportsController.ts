import { Request, Response } from 'express';
import { sportsService, createSport } from '../services/sportsService';

export async function sportsGet(req: Request, res: Response) {
  return sportsService(req.state);
}

export async function sportsPost(req: Request) {
  return createSport(req.body, req.state);
}

import v4 from 'uuid/v4';

import { errors } from '../../../errors';
import { sportsRepo } from '../repositories/sportsRepo';
import { Sport, SportPost } from '../models/Sport';
import { State } from '../../../models';
import { config } from '../../../config';

export async function sportsService(state: State) {
  state.logger.info('Sports service');
  const allSports = await sportsRepo.all(state);
  return allSports;
}

export async function singleSportService(id: string, state: State) {
  return sportsRepo.getOneById(id, state);
}

export async function createSport(body: SportPost, state: State) {
  state.logger.info({ body }, 'New sport');
  const { uid } = state.user;
  const isAdmin = config.adminsUid.indexOf(uid) >= 0;

  if (!isAdmin) {
    state.logger.warn({ uid }, 'Creating sport not authorized');
    throw errors.badRequest('User not allowed to create sport', {});
  }

  const document: SportPost = {
    id: v4(),
    title: body.title,
    description: body.description,
    parentId: body.parentId,
    canFilter: body.canFilter,
    icon: body.icon,
  };

  try {
    const response = await sportsRepo.create(document, state);
    state.logger.info({ newSport: response }, 'New sport created');
    return response;
  } catch (err) {
    state.logger.error({ err }, 'Error on creating new sport.');
    throw err;
  }
}

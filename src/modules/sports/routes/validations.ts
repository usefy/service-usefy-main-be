import * as joi from '@hapi/joi';

export const sportsPost: joi.SchemaLike = joi.object().keys({
  body: joi
    .object()
    .keys({
      title: joi.string().required(),
      description: joi.string(),
      parentId: joi.string(),
      canFilter: joi.boolean(),
      icon: joi.string(),
    })
    .required(),
});

import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import { validateReqParams } from '../../../helpers/validateReqParams';

import { sportsPost } from './validations';
import * as sportsController from '../controllers/sportsController';

export const router = Router();

router
  .route('/sports')
  .get(
    routerDefaultHandler(sportsController.sportsGet),
  ).post(
    validateFirebaseIdToken,
    validateReqParams(sportsPost),
    routerDefaultHandler(sportsController.sportsPost),
  );

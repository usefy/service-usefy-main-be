import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';

const tableName = `usefy-${config.server.configEnv}-sports`;

const validationScheme = joi.object().keys({
  id: joi.string().required(),
  parentId: joi.string(),
  title: joi.string().required(),
  description: joi.string(),
  canFilter: joi.boolean(),
  icon: joi.string(),
  createdAt: joi.date().required(),
  updatedAt: joi.date(),
});

export class SportModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async all(state: State) {
    const params = {
      TableName: this.tableName,
      ConsistentRead: true,
    };
    const { Items } = await this.dynamoScan(params, state);
    return Items;
  }

  async getOneById(id: string, state: State) {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#id = :id',
      ExpressionAttributeNames: {
        '#id': 'id',
      },
      ExpressionAttributeValues: {
        ':id': id,
      },
      ConsistentRead: true,
    };
    const { Items: data } = await this.dynamoQuery(params, state);
    return data![0];
  }

}

export const sportsTable = new SportModel(tableName, validationScheme);

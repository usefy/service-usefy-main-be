export interface Sport {
  id: string;
  title: string;
  description?: string;
  parent?: Sport | string;
  canFilter?: boolean;
  createdAt?: Date | string;
  updatedAt?: Date | string;
}

export interface SportPost {
  id: string;
  title: string;
  description?: string;
  parentId?: Sport | string;
  canFilter?: boolean;
  icon?: string;
}

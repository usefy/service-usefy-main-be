import * as _ from 'lodash';
import moment from 'moment';
import v4 from 'uuid/v4';
import { State } from '../../../models';
import { NotificationDocument } from '../models';
import { notificationsTable } from '../tables/notificationsTable';
import { string } from 'joi';

export const notificationsRepo = {

  create: async (document: Partial<NotificationDocument>, state: State): Promise<NotificationDocument> => {
    const documentCopy = _.cloneDeep(document);
    return notificationsTable.create(
      _.merge({}, documentCopy, {
        id: v4(),
        createdAt: moment().toISOString(),
      }),
      state,
    );
  },

  update: async (
    { uid, id }: { uid: string, id: string },
    document: Partial<NotificationDocument>,
    state: State,
  ): Promise<NotificationDocument> => notificationsTable.update({ id, uid }, document, state),

  getOne: async (key, state: State) => notificationsTable.get(key, state),

  getByUserUid: async (uid: string, state: State) => notificationsTable.getByUserUid(uid, state),

  deleteAll: state => notificationsTable.deleteAll(['uid', 'id'], state),

  delete: (key, state: State) => notificationsTable.delete(key, state),

  validate: document => notificationsTable.validate(document),

  countAll: (state: State) => notificationsTable.countAll(state),
};

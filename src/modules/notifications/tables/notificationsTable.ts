import * as joi from '@hapi/joi';
import * as _ from 'lodash';
import { config } from '../../../config';
import { DefaultModel } from '../../../components/dyno';
import { State } from '../../../models';
import { NotificationDocument } from '../models';

const tableName = `usefy-${config.server.configEnv}-notifications`;

const validationScheme = joi.object().keys({
  uid: joi.string().required(),
  id: joi.string().required(),
  subject: joi.string().required(),
  body: joi.string().required(),
  href: joi.string(),
  channel: joi.string(),
  template: joi.string(),
  readAt: joi.date(),
  createdAt: joi.date().required(),
});

export class NotificationsModel extends DefaultModel {
  constructor(tableName, validationScheme) {
    super(tableName, validationScheme);
  }

  async getByUserUid(uid: string, state: State): Promise<NotificationDocument[]> {
    const params = {
      TableName: this.tableName,
      KeyConditionExpression: '#uid = :uid',
      ExpressionAttributeNames: {
        '#uid': 'uid',
      },
      ExpressionAttributeValues: {
        ':uid': uid,
      },
      ConsistentRead: true,
    };
    const { Items } = await this.dynamoQuery(params, state);
    return <NotificationDocument[]>Items;
  }

  async update(
    { id, uid }: { uid: string, id: string },
    document: Partial<NotificationDocument>,
    state: State,
  ): Promise<NotificationDocument> {

    // tslint:disable-next-line:variable-name
    const UpdateExpression: string[] = [];
    // tslint:disable-next-line:variable-name
    const ExpressionAttributeValues = {};

    for (const key in document) {
      if (document.hasOwnProperty(key) && document[key]) {
        UpdateExpression.push(`${key} = :${key}`);
        ExpressionAttributeValues[`:${key}`] = _.get(document, key);
      }
    }

    const params = {
      ExpressionAttributeValues,
      UpdateExpression: `set ${UpdateExpression.join(' ,')}`,
      TableName: this.tableName,
      Key: {
        uid,
        id,
      },
      ReturnValues: 'ALL_NEW',
    };

    const { Attributes: data } = await this.dynamoUpdate(params, state);
    return <NotificationDocument>data;
  }
}

export const notificationsTable = new NotificationsModel(tableName, validationScheme);

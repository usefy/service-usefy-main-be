import { Request } from 'express';
import { getUsersNotifications, usersReadNotification } from '../services/notificationsService';

export async function getNotifications(req: Request) {
  return getUsersNotifications(req.state);
}

export async function putNotifications(req: Request) {
  return usersReadNotification(req.params, req.state);
}

import { State } from '../../../models';
import { notificationsRepo } from '../repos/notificationsRepo';
import moment = require('moment');

export const getUsersNotifications = async (state: State) => {
  const { user: { uid } } = state;

  const notifications = await notificationsRepo.getByUserUid(uid, state);

  return notifications;
};

export const usersReadNotification = async (params, state: State) => {
  const { user: { uid } } = state;
  const { id } = params;

  const notification = await notificationsRepo.update({ uid, id }, { readAt: moment().toISOString() }, state);

  return notification;
};

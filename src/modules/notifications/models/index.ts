export interface NotificationDocument {
  id: string;
  uid: string;
  subject: string;
  body: string;
  href?: string;
  channel?: string;
  template?: string;
  createdAt?: Date | string;
  readAt?: Date | string;
}

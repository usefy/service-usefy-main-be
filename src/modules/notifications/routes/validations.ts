import * as joi from '@hapi/joi';

export const notificationPut: joi.SchemaLike = joi.object().keys({
  params: joi
    .object()
    .keys({
      id: joi.string().required(),
    })
    .required(),
});

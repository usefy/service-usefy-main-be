import { Router } from 'express';

import { routerDefaultHandler } from '../../../helpers/routerDefaultHandler';
import { validateFirebaseIdToken } from '../../../helpers/authHandler';
import * as notificationsController from '../controllers/notificationsController';
import { validateReqParams } from '../../../helpers/validateReqParams';
import { notificationPut } from './validations';

export const router = Router();

router
  .route('/notifications')
  .get(
    validateFirebaseIdToken,
    routerDefaultHandler(notificationsController.getNotifications),
  );

router
  .route('/notifications/:id')
  .put(
    validateFirebaseIdToken,
    validateReqParams(notificationPut),
    routerDefaultHandler(notificationsController.putNotifications),
  );

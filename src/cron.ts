import { cronTask, ScheduleSetting } from './components/cronos';
import { logger } from './logger';
import { State } from './models';
import {
  processingDeliveredOrders,
  processingOrdersNotCheckReceivedAfter3Days,
  processingOrdersNotCheckReceivedAfter5Days,
  processingOrdersNotCheckReceivedAfter7Days,
  processingOrdersNotCheckDeliveredAfter3Days,
  processingOrdersNotCheckDeliveredAfter5Days,
  processingOrdersNotCheckDeliveredAfter7Days,
  processingOrdersWaitingPayment,
  processingShippedOrders,
} from './modules/checkout/services/ordersService';
import { processingUsersNotCreatedEquipoAfter5Days } from './modules/users/services/userService';

const childLogger = logger.child({ name: 'crontaks' });
const state: State = {
  logger: childLogger,
};

export const setupCron = () => {
  state.logger.info('Crontask: Setting up tasks');

  // CRON: 1 processingDeliveredOrders
  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing delivered orders started.');

    try {
      await processingDeliveredOrders(state);
    } catch (err) {
      state.logger.warn({ err }, 'Crontask: Error on running.');
    }
  });

  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing shipped orders started.');

    try {
      await processingShippedOrders(state);
    } catch (err) {
      state.logger.warn({ err }, 'Crontask: Error on running.');
    }
  });

  cronTask(ScheduleSetting.everyThirtyMinutes, async () => {
    state.logger.info('Crontask: Processing orders waiting for payment');
    try {
      await processingOrdersWaitingPayment(state);
    } catch (err) {
      state.logger.warn({ err }, 'Crontask: Error on running.');
    }
  });

  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing orders not check received after 3 days');

    await processingOrdersNotCheckReceivedAfter3Days(state);
  });

  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing orders not check received after 5 days');

    await processingOrdersNotCheckReceivedAfter5Days(state);
  });

  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing orders not check received after 7 days');

    await processingOrdersNotCheckReceivedAfter7Days(state);
  });

  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing orders not check delivery after 3 days');

    await processingOrdersNotCheckDeliveredAfter3Days(state);
  });

  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing orders not check delivery after 5 days');

    await processingOrdersNotCheckDeliveredAfter5Days(state);
  });

  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing orders not check delivery after 7 days');

    await processingOrdersNotCheckDeliveredAfter7Days(state);
  });

  cronTask(ScheduleSetting.everyDay5am, async () => {
    state.logger.info('Crontask: Processing users not created equipo after 5 days');

    await processingUsersNotCreatedEquipoAfter5Days(state);
  });
};

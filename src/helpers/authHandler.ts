import { errors } from '../errors';
import * as firebase from '../components/firebase';
import { has, merge } from 'lodash';
import { State } from '../models/index';
import { Roles } from '../models/roles';
import { config } from '../config';

const handleAdminClaims = async (uid: string, decodedIdToken, state: State) => {
  const isAdminInConfig = config.adminsUid.indexOf(uid) >= 0;

  if (has(decodedIdToken, 'roles') && Array.isArray(decodedIdToken.roles)) {
    const roles = decodedIdToken.roles;
    const isAdmin = decodedIdToken.roles.indexOf(Roles.superAdmin) >= 0;

    if (!isAdmin && isAdminInConfig) {
      firebase.user.setClaim(uid, { roles: merge(roles, [Roles.superAdmin]) });
    } else if (isAdmin && !isAdminInConfig) {
      const newRoles = [].filter((value: string) => value !== Roles.superAdmin);
      firebase.user.setClaim(uid, { roles: newRoles });
    }
  } else if (isAdminInConfig) {
    firebase.user.setClaim(uid, { roles: [Roles.superAdmin] });
  }
};

// Express middleware that validates Firebase ID Tokens passed in the
// Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the
// Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
export const validateFirebaseIdToken = async (req, res, next) => {
  req.state.logger.info(
    'Check if request is authorized with Firebase ID token',
  );

  if (
    (!req.headers.authorization ||
      !req.headers.authorization.startsWith('Bearer ')) &&
    !(req.cookies && req.cookies.__session)
  ) {
    // 'Make sure you authorize your request by providing the following HTTP header:',
    // 'Authorization: Bearer <Firebase ID Token>',
    // 'or by passing a "__session" cookie.'
    req.state.logger.error(
      'No Firebase ID token was passed as a Bearer token in the Authorization header.',
    );

    return next(errors.unauthorized('Unauthorized', {}));
  }

  let idToken;

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer ')
  ) {
    req.state.logger.info('Found "Authorization" header');
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else if (req.cookies) {
    req.state.logger.info('Found "__session" cookie');
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  } else {
    // No cookie
    return next(errors.unauthorized('Unauthorized', {}));
  }

  try {
    const decodedIdToken = await firebase.verifyIdToken(idToken, req.state);

    await handleAdminClaims(decodedIdToken.uid, decodedIdToken, req.state);

    req.state.user = decodedIdToken;
    next();
  } catch (error) {
    req.state.logger.error(
      { error },
      'Unable to verify authentication token on firebase.',
    );
    next(errors.unauthorized('Unauthorized', {}));
  }
};

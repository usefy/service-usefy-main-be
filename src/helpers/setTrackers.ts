const uuidv4 = require('uuid/v4');

export function setCorrelationAndSessionId(options = {}) {
  return (req, res, next) => {
    const correlationId =
      req.header('X-Correlation-Id') ||
      req.header('X-Amz-Cf-Id') ||
      req.header('X-Amz-Trace-Id') ||
      uuidv4();

    req.state = req.state || {};

    req.state.correlationId = correlationId;
    res.set('X-Correlation-Id', correlationId);

    if (req.header('X-Session-Id')) {
      req.state.sessionId = req.header('X-Session-Id');
      res.set('X-Session-Id', req.header('X-Session-Id'));
    }

    next();
  };
}

import * as joi from '@hapi/joi';
import * as _ from 'lodash';

import { errors } from '../errors';

export const validateReqParams = (schema: joi.SchemaLike, allowUnknown = true) => async (req, res, next) => {
  const result = joi.validate(req, schema, { allowUnknown });
  if (_.isObject(result.error)) {
    return next(errors.badRequest(_.get(result, 'error.message'), {}));
  }

  return next();
};

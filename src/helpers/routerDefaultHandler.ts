export const routerDefaultHandler = (fn: any) => async (req, res, next) => {
  try {
    req.state.out = await fn(req, res);
    return next();
  } catch (ex) {
    return next(ex);
  }
};

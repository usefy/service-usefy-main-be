import random from 'lodash/random';

export interface Options {
  letters?: boolean;
  numbers?: boolean;
  specials?: boolean;
}

export const randomString = (length: number, options: Options = { letters: true, numbers: true, specials: false }): string => {
  if (!options.letters && !options.numbers && !options.specials) {
    throw new Error('Invalid arguments, at least one char type should be true.');
  }

  const lettersList = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const numberList = '0123456789';
  const specialsList = '?!@#$%&*-=+';
  const response: string[] = Array(length);
  response.fill('');

  return response.map((value: string) => {
    let opt = 0;

    while (opt < 1) {
      opt = random(1, 3);

      switch (opt) {
        case 1:
          if (options.letters) {
            return lettersList[random(0, lettersList.length - 1)];
          }
          opt = 0;
        case 2:
          if (options.numbers) {
            return numberList[random(0, numberList.length - 1)];
          }
          opt = 0;
        case 3:
          if (options.specials) {
            return specialsList[random(0, specialsList.length - 1)];
          }
          opt = 0;
        default:
          opt = 0;
      }
    }

  }).join('');
};

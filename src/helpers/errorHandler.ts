import { logger } from '../logger';
import * as _ from 'lodash';
import { config } from '../config';

export const errorHandler = (err, req, res, next) => {
  if (!_.isNumber(err.statusCode)) {
    err.statusCode = 500;
  }

  res.status(err.statusCode);
  req.state.logger.error({ err, req, res }, 'Error handler at the end of app');

  const errOutput: any = _.pick(err, ['errorCode', 'statusCode', 'message', 'name', 'info']);
  if (config.params.sendErrorStackTrace) {
    errOutput.stack = _.get(err, 'stack');
  }

  // for any response that is application/json
  res.json(errOutput);
};

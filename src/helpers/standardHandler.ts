import * as _ from 'lodash';
import { errors } from '../errors';

export const standardHandler = (req, res, next) => {
  if (_.has(req, 'state.out')) {
    req.state.logger.info({ req, res }, 'Standard output');
    return res.json(req.state.out);
  }

  res.status(404);
  req.state.logger.info({ req, res }, 'Standard output 404');
  return res.json(errors.notFound('Not Found', {}));
};

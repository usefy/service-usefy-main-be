import { v4 as uuidv4 } from 'uuid';

export const createCustomerFaker = (args = {}) => {
  return  {
    id: uuidv4(),
    uid: uuidv4(),
    moipAccountId: uuidv4(),
    linksSelf: uuidv4(),
    createdAt: new Date(),
    ...args,
  };
};

import { v4 as uuidv4 } from 'uuid';
import * as faker from 'faker';

export const createUserFaker = (args = {}) => {
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();

  return  {
    firstName,
    lastName,
    uid: uuidv4(),
    displayName: `${firstName} ${lastName}`,
    email: faker.internet.email(),
    photoURL: faker.internet.avatar(),
    taxNumber: '052.215.770-03',
    birthDate: '28/06/1988',
    idNumber: `${faker.datatype.number()}`,
    idIssuer: 'qip',
    idIssuerDate: new Date().toISOString(),
    phone: {
      countryCode: '55',
      areaCode: '27',
      phone: '965213244',
    },
    address: {
      zipcode: '99999999',
      address: faker.address.streetName(),
      address2: 'Casa',
      district: 'Centro',
      country: 'BRA',
      currentCity: faker.address.city(),
      currentState: 'AA',
      number: `${faker.datatype.number()}`,
    },
    ...args,
  };
};

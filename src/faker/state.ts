import { v4 as uuidv4 } from 'uuid';
import { logger } from '../logger';

export const createStateFaker = (args = {}) => {
  return  {
    logger,
    correlationId: uuidv4(),
    sessionId: uuidv4(),
    ...args,
  };
};

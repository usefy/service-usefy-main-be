import { v4 as uuidv4 } from 'uuid';
import { logger } from '../logger';

import { createUserFaker } from './user';
import { createEquipoFaker } from './equipo';
import { createShipmentFaker } from './shipment';
import { createOrderFaker } from './order' ;
import { createAccountFaker } from './account';
import { createCustomerFaker } from './customer';
import { createStateFaker } from './state';
import { createMoipFaker } from './moip';

const state = {
  logger,
  correlationId: uuidv4(),
  sessionId: uuidv4(),
};

export {
  uuidv4,
  createUserFaker,
  createEquipoFaker,
  createShipmentFaker,
  createOrderFaker,
  createAccountFaker,
  createCustomerFaker,
  createStateFaker,
  createMoipFaker,
  state,
};

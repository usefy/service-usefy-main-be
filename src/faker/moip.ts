
import { v4 as uuidv4 } from 'uuid';
import * as faker from 'faker';
import moment from 'moment';

export const createMoipFaker = (args) => {
  return  {
    email: {
      address: args.email,
    },
    person: {
      name: args.displayName,
      lastName: args.lastName || faker.name.lastName(),
      taxDocument: {
        type: 'CPF',
        number: args.taxNumber,
      },
      identityDocument: {
        type: 'RG',
        number: args.idNumber!,
        issuer: args.idIssuer!,
        issueDate: moment(args.idIssuerDate!, 'DD-MM-YYYY').format('YYYY-MM-DD').toString(),
      },
      birthDate: moment(args.birthDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      phone: {
        countryCode: args.phone!.countryCode,
        areaCode: args.phone!.areaCode,
        number: args.phone!.phone,
      },
      address: {
        street: args.address!.address,
        streetNumber: args.address!.number,
        district: args.address!.district,
        zipCode: args.address!.zipcode,
        city: args.address!.currentCity,
        state: args.address!.currentState,
        country: args.address!.country,
      },
    },
    type: 'MERCHANT', // Marketplaces devem utilizar o valor MERCHANT para criar contas para os seus usuários.
    transparentAccount: true,
  };
};

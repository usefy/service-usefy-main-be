
import { v4 as uuidv4 } from 'uuid';
import * as faker from 'faker';

import { createShipmentFaker } from './shipment';
import { createCouponFaker } from './coupon';

export const createOrderFaker = (args = {}) => {
  const shipmentFaker = createShipmentFaker();
  const couponFaker = createCouponFaker();

  return  {
    id: uuidv4(),
    listingId: uuidv4(),
    uid: uuidv4(),
    shipment: shipmentFaker,
    coupon: 'coupon',
    orderStatus: 'new',
    history: [
      {
        uid: uuidv4(),
        summary: 'Order Created',
        status: 'new',
        createdAt: new Date().toISOString(),
      },
    ],
    ...args,
  };
};

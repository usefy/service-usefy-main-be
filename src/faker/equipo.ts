import { v4 as uuidv4 } from 'uuid';
import * as faker from 'faker';

export const createEquipoFaker = (args = {}) => {
  const productName = faker.commerce.productName();

  return  {
    cover: faker.image.imageUrl(),
    createdAt: new Date().toISOString(),
    firebaseUid: uuidv4(),
    hasInvoice: faker.datatype.boolean(),
    hasWarranty: faker.datatype.boolean(),
    id: uuidv4(),
    itemCondition: 'as-new',
    itemStatus: 'enabled',
    manufacturer: faker.company.companyName() ,
    photos: [
      faker.image.imageUrl(),
    ],
    price: 100000,
    publishedAt: new Date().toISOString() ,
    size: `${faker.datatype.number()}`,
    sports: [
      uuidv4(),
    ],
    story: productName,
    storySearch: productName,
    title: productName,
    titleSearch: productName,
    updatedAt: new Date().toISOString(),
    ...args,
  };
};

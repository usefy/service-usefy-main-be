import { v4 as uuidv4 } from 'uuid';
import * as faker from 'faker';

export const createCouponFaker = (args = {}) => {
  return  {
    id: uuidv4(),
    key: uuidv4(),
    firebaseUid: uuidv4(),
    title: 'cuponfaker',
    value: 100,
    validUntil: new Date().toISOString(),
    createdAt: new Date().toISOString(),
    ...args,
  };
};

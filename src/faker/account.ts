import { v4 as uuidv4 } from 'uuid';

export const createAccountFaker = (args = {}) => {
  return  {
    uid: uuidv4(),
    id: uuidv4(),
    accessToken: uuidv4(),
    channelId: uuidv4(),
    linksSelf: uuidv4(),
    login: uuidv4(),
    createdAt: new Date(),
    ...args,
  };
};

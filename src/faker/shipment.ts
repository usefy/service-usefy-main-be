import * as faker from 'faker';

export const createShipmentFaker = (args = {}) => {
  return  {
    method: 'Correios',
    cost: faker.datatype.number(),
    zipcode: faker.address.zipCode(),
    address: faker.address.streetName(),
    address2: 'Casa',
    district: 'Centro',
    country: faker.address.country(),
    city: faker.address.city(),
    state: faker.address.state(),
    ...args,
  };
};

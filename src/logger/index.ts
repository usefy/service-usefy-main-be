// tslint:disable-next-line
import pino from 'pino';
import * as _ from 'lodash';
import { config } from '../config';

const headerWhitelist = ['cache-control', 'content-type', 'accept', 'host',
  'accept-encoding', 'content-length', 'connection', 'user-agent', 'x-real-ip',
  'x-forwarded-for'];

const options = {};

const serializers = {
  axiosError(error) {
    return _.pick(error, [
      'message', 'stack',
      'config.data', 'config.url', 'config.method', 'config.headers',
      'response.status', 'response.statusText', 'response.headers', 'response.data',
      'host', 'hostname', 'port', 'code', 'errno']);
  },
  axiosRequest(params) {
    return _.pick(params, [
      'method',
      'connection.servername',
      'connection.remoteAddress',
      'connection.remotePort',
      'path', 'headers',
    ]);
  },
  err(params) {
    const notSerializedByDefault = _.pick(params, ['message', 'stack']);
    return _.merge({}, params, notSerializedByDefault);
  },
  params(params) {
    return _.pick(params, ['port', 'name', 'params']);
  },
  processEnv(params) {
    return _.pick(params, [
      'NODE_ENV',
      'CONFIG_ENV',
      'PORT',
      'deploymentVersion',
      'logLevel',
      'CUSTOM_MAX_OLD_SPACE_SIZE',
    ]);
  },
  req(params) {
    const allHeaders = _.get(params, 'headers');
    const headers = _.pick(allHeaders, headerWhitelist);
    return _.merge({}, headers, _.pick(params, ['body', 'method', 'originalUrl', 'query', 'url']));
  },
  res(params) {
    const allHeaders = _.get(params, 'headers');
    const headers = _.pick(allHeaders, headerWhitelist);
    return _.merge({}, headers, _.pick(params, 'statusCode'));
  },
  trackers(params) {
    return _.pick(params, ['correlationId', 'sessionId']);
  },
};

const pinoConfig: any = {
  serializers,
  name: config.log.name,
  level: config.log.level,
};

if (config.log.pretty) {
  pinoConfig.prettyPrint = {
    levelFirst: config.log.pretty || false,
  };
  pinoConfig.prettifier = require('pino-pretty');
}

export const logger = pino(pinoConfig);

process.on('unhandledRejection', (reason, p) => {
  logger.error('Possibly Unhandled Rejection at: Promise ', p, ' reason: ', reason);
  logger.error(p['_trace'] ? p['_trace']['stack'] : p);
});

process.on('uncaughtException', (err) => {
  logger.error('An uncaught exception occurred!');
  logger.error(err.stack ? [err.stack] : err);
});

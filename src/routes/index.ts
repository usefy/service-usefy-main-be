import * as express from 'express';
import { logger } from '../logger';
import { errorHandler } from '../helpers/errorHandler';
import { standardHandler } from '../helpers/standardHandler';

import { router as authRouter } from '../modules/auth/routes';
import { router as sportsRouter } from '../modules/sports/routes';
import { router as checkoutRouter } from '../modules/checkout/routes';
import { router as citiesByStateRouter } from '../modules/citiesByState/routes';
import { router as clubsRouter } from '../modules/clubs/routes';
import { router as couponsRouter } from '../modules/coupons/routes';
import { router as listingsRouter } from '../modules/listings/routes';
import { router as messagesRouter } from '../modules/messages/routes';
import { router as shipmentRouter } from '../modules/shipment/routes';
import { router as userRouter } from '../modules/users/routes';
import { router as notificationsRouter } from '../modules/notifications/routes';
import { router as healthRouter } from '../modules/health/routes';
import { router as paymentsRouter } from '../modules/payments/routes';
import { router as availableBanksRouter } from '../modules/availableBanks/routes';

logger.info('Declaring Express routes.');

// API
export const router: express.Router = express.Router();

router.use('', [
  healthRouter,
]);

router.use('/v1', [
  authRouter,
  sportsRouter,
  checkoutRouter,
  citiesByStateRouter,
  clubsRouter,
  couponsRouter,
  listingsRouter,
  messagesRouter,
  shipmentRouter,
  userRouter,
  notificationsRouter,
  paymentsRouter,
  availableBanksRouter,
]);

router.use(errorHandler);
router.use(standardHandler);

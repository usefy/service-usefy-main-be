import * as firebase from 'firebase-admin';

export type Config = {
  log: LogConfig;
  params: ParamsConfig;
  root: string;
  server: ServerConfig;
  autoRetry: AutoRetryConfig;
  version: VersionConfig;
  firebase: firebase.AppOptions;
  dyno: DynoConfig;
  garmin: GarminConfig;
  moip: MoipConfig;
  notification: NotificationConfig;
  adminsUid: string[],
  coupons: CouponsConfig;
  correios: CorreiosConfig;
  customerio: CustomerioConfig;
  zendesk: ZendeskProps;
  frontend: FrontendProps;
}

interface FrontendProps {
  url: string;
}

export interface ZendeskProps {
  username: string;
  token: string;
  remoteUri: string;
}

export interface CustomerioConfig {
  siteID: string;
  trackingApiKey: string;
  appApiKey: string;
  enabled: boolean;
  track: {
    welcome: string;
    message: {
      received: string;
    },
    payment: {
      canceled: string;
    },
    order: {
      buyer: {
        notCheckReceivedAfter3Days: string;
        notCheckReceivedAfter5Days: string;
        notCheckReceivedAfter7Days: string;
      };
      seller: {
        notCheckDeliveredAfter3Days: string;
        notCheckDeliveredAfter5Days: string;
        notCheckDeliveredAfter7Days: string;
        notCreatedEquipoAfter5Days: string;
      };
    };
    checkout: {
      seller: string;
      buyer: string;
      error: {
        wirecard: string;
      };
    };
    transfer: {
      seller: string;
      error: {
        wirecard: string;
      };
    };
    equipo: {
      new: string;
      enabled: string;
      review: string;
      delivered: string;
      received: string;
    };
  };
}
export interface CorreiosConfig {
  SRO: {
    baseUrl: string,
    limit: number,
    username: string,
    password: string,
    type: string,
    result: string,
    language: string,
    filter: boolean,
  }
}

export interface CouponsConfig {
  validInHours: number;
}

export interface NotificationConfig {
  queueUrl: string;
}

export interface GarminConfig {
  key: string;
  secret: string;
}

export interface MoipConfig {
  accessToken: string;
  production: boolean;
  escrowDescription: string;
  baseUrl: string;
  webhookToken: string;
  sellerSplit: number;
  boleto: {
    daysToExpire: number,
    instructionLines: {
      first: string;
      second: string;
      third: string;
    }
    logoUri: string;
  },
  creditCard: {
    fee: number;
  }
}

export type DynoConfig = {
  endpoint: string;
};

export type LogConfig = {
  name: string;
  level?:
    | 'trace'
    | 'debug'
    | 'info'
    | 'warn'
    | 'error'
    | 'fatal'
  pretty: boolean;
};

export type ParamsConfig = {
  sendErrorStackTrace: boolean;
  mockFirebase: boolean;
  mockDynamodb: boolean;
};

export type ServerConfig = {
  port: number;
  nodeEnv: string;
  configEnv: string;
  region: string;
};

export type SubscriptionVerificationRokuConfig = {
  ag: number;
};

export type AutoRetryConfig = {
  numberOfRetries: number;
};

export type VersionConfig = {
  apiVersion: string;
  deploymentVersion: string;
};

export default Config;

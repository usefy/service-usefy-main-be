import { Config } from './index.d';
import { defaults } from './defaults';
import * as _ from 'lodash';
import { envs } from './envs';

if (!_.has(process.env, 'CONFIG_ENV')) {
  throw Error('You have to set up your environment');
}

let envPath;
// set name of desired config file as environment variable
const configEnv = process.env.CONFIG_ENV;

try {
  envPath = require.resolve(`./env/${configEnv}`);
} catch (err) {
  throw Error(`Couldn't find environment configuration for ${configEnv}`);
}

// combine config files to one main config
const config: Config =
  _.merge(
    {},
    defaults,
    require(envPath).default,
    envs,
  );

export { config };

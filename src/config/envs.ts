import * as firebase from 'firebase-admin';
const env = process.env;

export const envs = {
  log: {
    level: env.logLevel,
  },
  params: {
    mockDynamodb: env.MOCK_DYNAMODB,
  },
  server: {
    nodeEnv: env.NODE_ENV,
    configEnv: env.CONFIG_ENV,
    port: env.PORT,
  },
  version: {
    apiVersion: env.apiVersion,
    deploymentVersion: env.deploymentVersion,
  },
  dyno: {
    endpoint: env.MOCK_DYNAMODB_ENDPOINT,
  },
  garmin: {
    key: env.GARMIN_KEY,
    secret: env.GARMIN_SECRET,
  },
  moip: {
    accessToken: env.MOIP_ACCESSTOKEN,
    webhookToken: env.MOIP_WEBHOOK_TOKEN,
    creditCard: {
      fee: env.MOIP_CREDIT_CARD_FEE,
    },
  },
  firebase: {
    credential: env.FIREBASE_PRIVATE_KEY ? firebase.credential.cert({
      projectId: env.FIREBASE_PROJECT_ID,
      clientEmail: env.FIREBASE_CLIENT_EMAIL,
      privateKey: env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
    }) : null,
  },
  adminsUid: env.ADMIN_UIDS ? env.ADMIN_UIDS.split(',') : [],
  customerio: {
    siteID: env.CUSTOMERIO_ID,
    appApiKey: env.CUSTOMERIO_APP_API_KEY,
    trackingApiKey: env.CUSTOMERIO_TRACKING_API_KEY,
    enabled: env.CUSTOMERIO_ENABLED === 'true',
    track: {
      welcome: env.CUSTOMERIO_TRACKING_WELCOME,
      message: {
        received: env.CUSTOMERIO_TRACKING_RECEIVED_MESSAGE,
      },
      payment: {
        canceled: env.CUSTOMERIO_TRACKING_PAYMENT_CANCELED,
      },
      order: {
        buyer: {
          notCheckReceivedAfter3Days: env.CUSTOMERIO_TRACKING_ORDER_BUYER_NOT_CHECK_RECEIVED_AFTER3DAYS,
          notCheckReceivedAfter5Days: env.CUSTOMERIO_TRACKING_ORDER_BUYER_NOT_CHECK_RECEIVED_AFTER5DAYS,
          notCheckReceivedAfter7Days: env.CUSTOMERIO_TRACKING_ORDER_BUYER_NOT_CHECK_RECEIVED_AFTER7DAYS,
        },
        seller: {
          notCheckDeliveredAfter3Days: env.CUSTOMERIO_TRACKING_ORDER_SELLER_NOT_CHECK_DELIVERED_AFTER3DAYS,
          notCheckDeliveredAfter5Days: env.CUSTOMERIO_TRACKING_ORDER_SELLER_NOT_CHECK_DELIVERED_AFTER5DAYS,
          notCheckDeliveredAfter7Days: env.CUSTOMERIO_TRACKING_ORDER_SELLER_NOT_CHECK_DELIVERED_AFTER7DAYS,
          notCreatedEquipoAfter5Days: env.CUSTOMERIO_TRACKING_ORDER_SELLER_NOT_CREATED_EQUIPO_AFTER5DAYS,
        },
      },
      checkout: {
        seller: env.CUSTOMERIO_TRACKING_CHECKOUT_SELLER,
        buyer: env.CUSTOMERIO_TRACKING_CHECKOUT_BUYER,
        error: {
          wirecard: env.CUSTOMERIO_TRACKING_CHECKOUT_WIRECARD_ERROR,
        },
      },
      transfer: {
        seller: env.CUSTOMERIO_TRACKING_TRANSFER_SELLER,
        error: {
          wirecard: env.CUSTOMERIO_TRACKING_TRANSFER_WIRECARD_ERROR,
        },
      },
      equipo: {
        new: env.CUSTOMERIO_TRACKING_EQUIPO_NEW,
        enabled: env.CUSTOMERIO_TRACKING_EQUIPO_ENABLED,
        review: env.CUSTOMERIO_TRACKING_EQUIPO_REVIEW,
        delivered: env.CUSTOMERIO_TRACKING_EQUIPO_DELIVERED,
        received: env.CUSTOMERIO_TRACKING_EQUIPO_RECEIVED,
      },
    },
  },
  zendesk: {
    username: env.ZENDESK_USERNAME,
    token: env.ZENDESK_TOKEN,
    remoteUri: env.ZENDESK_REMOTE_URI,
  },
  frontend: {
    url: env.FRONTEND_URL,
  },
};


export default {
  log: {
    level: 'trace',
    pretty: true,
  },
  params: {
    sendErrorStackTrace: true,
    mockFirebase: true,
    mockDynamodb: true,
  },
};

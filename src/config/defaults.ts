import * as path from 'path';

import { Config } from './index.d';

export const defaults: Config = {
  log: {
    name: 'service-usefy-main',
    level: 'debug',
    pretty: false,
  },
  params: {
    // true, if you want to output error into response output (recommended false on production)
    sendErrorStackTrace: false,
    mockFirebase: false,
    mockDynamodb: false,
  },
  root: path.normalize(`${__dirname}/../..`),
  server: {
    port: 3000,
    nodeEnv: 'notSet', // NODE_ENV value, should be development or production
    configEnv: 'notSet', // CONFIG_ENV choose the env/ file to be merged
    region: 'us-east-1',
  },
  autoRetry: {
    numberOfRetries: 3,
  },
  version: {
    apiVersion: 'API version was not set up',
    deploymentVersion: 'Deployment version was not set up',
  },
  firebase: {
    credential: null,
    databaseURL: null,
  },
  dyno: {
    endpoint: 'http://localhost:8000',
  },
  garmin: {
    key: 'notSet',
    secret: 'notSet',
  },
  moip: {
    accessToken: 'notSet',
    production: false,
    baseUrl: 'https://sandbox.moip.com.br/v2',
    escrowDescription: 'Aguardando a confirmação de recebimento',
    webhookToken: 'notset',
    boleto: {
      daysToExpire: 2,
      instructionLines: {
        first: 'Atenção,',
        second: 'fique atento à data de vencimento do boleto.',
        third: 'Pague em qualquer casa lotérica.',
      },
      logoUri: '',
    },
    creditCard: {
      fee: 1.99,
    },
    sellerSplit: 0.8,
  },
  notification: {
    queueUrl: 'http://localhost:1234',
  },
  coupons: {
    validInHours: 48,
  },
  adminsUid: [],
  customerio: {
    siteID: null,
    appApiKey: null,
    trackingApiKey: null,
    enabled: false,
    track: {
      welcome: null,
      message: {
        received: null,
      },
      payment: {
        canceled: null,
      },
      order: {
        buyer: {
          notCheckReceivedAfter3Days: null,
          notCheckReceivedAfter5Days: null,
          notCheckReceivedAfter7Days: null,
        },
        seller: {
          notCheckDeliveredAfter3Days: null,
          notCheckDeliveredAfter5Days: null,
          notCheckDeliveredAfter7Days: null,
          notCreatedEquipoAfter5Days: null,
        },
      },
      checkout: {
        seller: null,
        buyer: null,
        error: {
          wirecard: null,
        },
      },
      transfer: {
        seller: null,
        error: {
          wirecard: null,
        },
      },
      equipo: {
        new: null,
        enabled: null,
        review: null,
        received: null,
        delivered: null,
      },
    },
  },
  zendesk: {
    username: '',
    token: '',
    remoteUri: '',
  },
  frontend: {
    url: '',
  },
  correios: {
    SRO: {
      baseUrl: 'https://webservice.correios.com.br',
      username: 'ECT',
      password: 'SRO',
      type: 'L',
      result: 'T',
      language: '101',
      limit: 5000,
      filter: true,
    },
  },
};

import { logger } from './logger';
import { config } from './config';
import { app } from './app';
import { setupCron } from './cron';

const { port } = config.server;

logger.info('Initializing app');

async function startServer() {
  if (config.params.mockDynamodb) {
    logger.info('Initializing Local DynamoDB Tables');

    const { createTables } = require('./components/dyno/mock/createLocalTables');
    await createTables();
  }

  setupCron();

  app.listen(port, () => {
    logger.info(
      {
        port,
        params: config.params,
        processEnv: process.env,
      },
      `App has started on port ${port}`,
    );
  });

}

startServer()
  .catch((err) => {
    logger.error({ err }, 'Critical error, cannot start server');
    process.exit(1);
  });

// To run: CONFIG_ENV=localhost ts-node adhoc/add-search-props-to-enabled-listings.ts

import * as AWS from 'aws-sdk';

import { config } from '../src/config';
import { logger } from '../src/logger';

// localhost/tests config
const opt = {
  region: 'us-east-1',
  accessKeyId: 'xxxx',
  secretAccessKey: 'xxxx',
  endpoint: config.dyno.endpoint,
};

AWS.config.update(opt);
const docClient = new AWS.DynamoDB.DocumentClient();

const updateItem = (item) => {
  const updateParams = {
    TableName: 'usefy-localhost-listings',
    Key: {
      id: item.id,
      firebaseUid: item.firebaseUid,
    },
    UpdateExpression: 'set titleSearch = :titleSearch, storySearch=:storySearch',
    ExpressionAttributeValues: {
      ':titleSearch': item.title.toLowerCase(),
      ':storySearch': item.story.toLowerCase(),
    },
    ReturnValues: 'UPDATED_NEW',
  };

  docClient.update(updateParams, (err, data) => {
    if (err) {
      logger.error('Unable to update item. Error JSON:', JSON.stringify(err, null, 2));
    } else {
      logger.info('UpdateItem succeeded:', JSON.stringify(data, null, 2));
    }
  });
};

const getParams = {
  TableName: 'usefy-localhost-listings',
  FilterExpression: '#itemStatus = :itemStatus',
  ExpressionAttributeNames: {
    '#itemStatus': 'itemStatus',
  },
  ExpressionAttributeValues: { ':itemStatus': 'enabled' },
  ExclusiveStartKey: undefined,
};

let count = 0;
function onScan(err, data) {
  if (err) {
    logger.error('Unable to scan the table. Error JSON:', JSON.stringify(err, null, 2));
  } else {
    logger.info('Scan succeeded.');
    data.Items.forEach((itemdata) => {
      count += 1;
      logger.info('Item :', count, itemdata.title);
      updateItem(itemdata);
    });

    // continue scanning if we have more items
    if (typeof data.LastEvaluatedKey !== 'undefined') {
      logger.info('Scanning for more...');
      getParams.ExclusiveStartKey = data.LastEvaluatedKey;
      docClient.scan(getParams, onScan);
    }
  }
}

docClient.scan(getParams, onScan);

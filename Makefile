api-test-integration:
	@docker-compose up --build --exit-code-from api-test-integration --abort-on-container-exit api-test-integration
.PHONY: test-integration

prepare:
	@yarn install
	@ops/tools/install-docker-compose.sh
	@mkdir data
	@cd data
	@touch shared-local-instance.db